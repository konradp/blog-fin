function compound(price, perc) {
  return (perc/100)*price;
}

function main() {
  // Get params
  var iPrice = document.getElementById('price');
  var iCount = document.getElementById('count');
  var iPerc = document.getElementById('perc');
  var price = parseInt(iPrice.value);
  var count = parseInt(iCount.value);
  var perc = parseInt(iPerc.value);

  // Changing price value updates the table
  iPrice.addEventListener('change', () => {
    main();
  });

  // Populate table
  var t = document.getElementById('table');
  t.innerHTML = "";
  var tr = document.createElement('tr');
  tr.innerHTML = "<td>0</td><td>" + price.toFixed(2) + "</td><td>0.00</td>";
  t.appendChild(tr);
  for (var i = 1; i <= count; i++) {
    gain = compound(price, perc);
    price = price + gain
    console.log(price.toFixed(2))
    var tr = document.createElement('tr');
    var tdDay = document.createElement('td');
    var tdPrice = document.createElement('td');
    var tdGain = document.createElement('td');
    tdDay.innerHTML = i;
    tdPrice.innerHTML = price.toFixed(2);
    tdGain.innerHTML = gain.toFixed(2);
    tr.appendChild(tdDay);
    tr.appendChild(tdPrice);
    tr.appendChild(tdGain);
    t.appendChild(tr);
  }
}
