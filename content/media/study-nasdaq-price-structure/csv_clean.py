#!/usr/bin/env python3
# Convert CSV file to JSON file
import csv
import json
import sys

if len(sys.argv) != 3:
  print(f'Usage: {sys.argv[0]} FILE_IN FILE_OUT')
  exit(1)
f_in = sys.argv[1]
f_out = sys.argv[2]

with open(f_in, mode='r') as f:
  reader = csv.DictReader(f)
  data = [row for row in reader]

for r in data:
  #print(row['Market Cap'])
  if r['Market Cap'] == '0.00':
    print(r['Symbol'], r['Name'], r['Last Sale'], r['Market Cap'])

data = [r['Market Cap'] for r in data if r['Market Cap'] != '0.00']
with open(f_out, mode='w') as f:
  f.write('\n'.join(data))
