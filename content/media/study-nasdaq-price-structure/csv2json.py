#!/usr/bin/env python3
# Convert CSV file to JSON file
import csv
import json
import sys
f_in = sys.argv[1]
f_out = sys.argv[2]

with open(f_in, mode='r') as f:
  reader = csv.DictReader(f)
  data = [row for row in reader]

with open(f_out, mode='w') as f:
  json.dump(data, f, indent=4)
