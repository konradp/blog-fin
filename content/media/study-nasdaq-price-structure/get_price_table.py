#!/usr/bin/env python3
import numpy as np
import sys

if len(sys.argv) < 2:
  print(f'Usage: {sys.argv[0]} FILE')
  print(f'Example: {sys.argv[0]} price.txt')
  exit(1)


def process_price_file(filename, bin_size=100):
  with open(FILE, mode='r') as f:
    prices = [float(line.strip()) for line in f]

  print(prices)

  # Define bins
  max_price = int(max(prices) // bin_size + 1) * bin_size  # Ensure bins cover the range
  bins = np.arange(0, max_price + bin_size, bin_size)      # Define bin edges
  bin_labels = [f"{int(bins[i])}-{int(bins[i+1])}" for i in range(len(bins)-1)]

  # Calculate histogram
  counts, _ = np.histogram(prices, bins=bins)

  # Calculate percentages
  total_count = sum(counts)
  percentages = [(count / total_count) * 100 for count in counts]

  # Print results
  print(f"{'RANGE':<10} | {'COUNT':<10} | {'PERC':<10}")
  print(f"{'--:':<10} | {'--:':<10} | {'--:':<10}")
  for label, count, percent in zip(bin_labels, counts, percentages):
    print(f"{label:<10} | {count:<10} | {percent:.2f}%")


FILE = sys.argv[1]
process_price_file(FILE, bin_size=100)
