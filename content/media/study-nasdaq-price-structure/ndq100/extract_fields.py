#!/usr/bin/env python3
# Used only once, to get the csv lines from FILE_CSV, for the NDQ100 tickers listed in symbols.json
# Outputs out.csv
import csv
import json

FILE_CSV = "../nasdaq_screener_1736274256746_nasdaq.csv"
OUTPUT_CSV = "out.csv"

# Get symbols
FILE_SYMBOLS = "symbols.json"
symbols = []
with open(FILE_SYMBOLS, 'r') as f:
  data = json.load(f)
  data = data['data']['data']['rows']
  for s in data:
    symbols.append(s['symbol'])

print(f"Got {len(symbols)} symbols")
print(symbols)

# Get csv lines
data_out = []
with open(FILE_CSV, mode='r') as f:
  reader = csv.DictReader(f)
  data = [row for row in reader]
  for i in data:
    if i['Symbol'] in symbols:
      data_out.append(i)
      #print(i)
print(f"Got {len(data_out)} datapoints")

# Save data
if data_out:
  with open(OUTPUT_CSV, mode='w', newline='') as f:
    writer = csv.DictWriter(f, fieldnames=data_out[0].keys())
    writer.writeheader() 
    writer.writerows(data_out)
