console.log('test');
let DATA = null;


window.addEventListener('load', () => {
  document.getElementById('price-submit').addEventListener('click', drawPrices);
  document.getElementById('show-hide').addEventListener('click', () => {
    let el = document.getElementById('price-stocks-div');
    el.style.display = el.style.display === 'none' ? 'block' : 'none';
  });

  fetch('/blog-fin/media/study-nasdaq-price-structure/prices-20240621.json')
    .then(response => response.json())
    .then(data => {
      DATA = data;
      drawPrices();
    });
});


function drawPrices() {
  let min = document.getElementById('price-min').value;
  min = parseInt(min);
  let max = document.getElementById('price-max').value;
  max = parseInt(max);

  console.log(min, max)

  let stocks = {};
  let data = DATA['data'];
  for (s in data) {
    let p = data[s];
    if (p >= min && p <= max) {
      stocks[s] = p;
    }
  }

  console.log('stocks', stocks);
  let count = Object.keys(stocks).length;
  document.getElementById('price-count').innerText = count;
  //console.log('count', Object.keys(stocks).length);
  document.getElementById('price-stocks').innerText = JSON.stringify(stocks, null, 2);
}
