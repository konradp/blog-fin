#!/usr/bin/env python3
import json
import sys
from datetime import datetime


if len(sys.argv) != 2:
  print(f"Usage: {sys.argv[0]} FILE")
  exit(1)

FILE = sys.argv[1]
with open(FILE, 'r') as f:
  data = json.load(f)
  timestamps = data['chart']['result'][0]['timestamp']
  prices = data['chart']['result'][0]['indicators']['adjclose'][0]['adjclose']

data_new = {
  't': [],
  'p': [],
}
for i, t in enumerate(timestamps):
  t_new = datetime.utcfromtimestamp(t).strftime('%Y-%m-%d')
  data_new['t'].append(t_new)
  p = round(prices[i], 2)
  data_new['p'].append(p)
  #print(f"t {t} p {p:.2f}")

print(json.dumps(data_new))
