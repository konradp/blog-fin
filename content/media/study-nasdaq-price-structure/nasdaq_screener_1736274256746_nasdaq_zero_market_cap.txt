AFJK Aimei Health Technology Co. Ltd Ordinary Share $10.66 0.00
AIMAU Aimfinity Investment Corp. I Unit $12.99 0.00
AIMAW Aimfinity Investment Corp. I Warrant $0.0215 0.00
AIMBU Aimfinity Investment Corp. I Subunit $12.13 0.00
AITR AI TRANSPORTATION ACQUISITION CORP Ordinary shares $10.75 0.00
AITRR AI TRANSPORTATION ACQUISITION CORP Right $0.12 0.00
AITRU AI TRANSPORTATION ACQUISITION CORP Unit $10.86 0.00
ALCY Alchemy Investments Acquisition Corp 1 Class A Ordinary Shares $10.94 0.00
ALCYW Alchemy Investments Acquisition Corp 1 Warrants $0.09 0.00
ALDF Aldel Financial II Inc. Class A Ordinary Shares $9.93 0.00
ALDFU Aldel Financial II Inc. Units $10.0455 0.00
ALDFW Aldel Financial II Inc. Warrants $0.295 0.00
ALF Centurion Acquisition Corp. Class A Ordinary Shares $10.10 0.00
ALFUU Centurion Acquisition Corp. Unit $10.1701 0.00
ALFUW Centurion Acquisition Corp. Warrant $0.125 0.00
ANSC Agriculture & Natural Solutions Acquisition Corporation Class A Ordinary Shares $10.48 0.00
ANSCW Agriculture & Natural Solutions Acquisition Corporation Warrant $0.27 0.00
AQU Aquaron Acquisition Corp. Common Stock $11.80 0.00
AQUNR Aquaron Acquisition Corp. Rights $0.2114 0.00
AQUNU Aquaron Acquisition Corp. Units $11.46 0.00
ASPC A SPAC III Acquisition Corp. Class A Ordinary Shares $9.97 0.00
ASPCR A SPAC III Acquisition Corp. Right $0.12 0.00
ASPCU A SPAC III Acquisition Corp. Unit $10.0822 0.00
ATMC AlphaTime Acquisition Corp Ordinary Shares $11.341 0.00
ATMCR AlphaTime Acquisition Corp Right $0.2075 0.00
ATMCW AlphaTime Acquisition Corp Warrant $0.0176 0.00
ATMV AlphaVest Acquisition Corp Ordinary Shares $11.40 0.00
ATMVR AlphaVest Acquisition Corp Right. $0.1352 0.00
BACQ Bleichroeder Acquisition Corp. I Class A Ordinary Shares $9.87 0.00
BACQR Bleichroeder Acquisition Corp. I Right $0.18 0.00
BACQU Bleichroeder Acquisition Corp. I Unit $10.03 0.00
BAYA Bayview Acquisition Corp Ordinary Shares $10.72 0.00
BAYAR Bayview Acquisition Corp Right $0.17 0.00
BEAG Bold Eagle Acquisition Corp. Class A Ordinary Shares $9.87 0.00
BEAGR Bold Eagle Acquisition Corp. Right $0.225 0.00
BEAGU Bold Eagle Acquisition Corp. Units $10.22 0.00
BKHA Black Hawk Acquisition Corporation Class A Ordinary Shares $10.38 0.00
BKHAR Black Hawk Acquisition Corporation Rights $1.00 0.00
BLAC Bellevue Life Sciences Acquisition Corp. Common Stock $11.35 0.00
BLACU Bellevue Life Sciences Acquisition Corp. Unit $11.89 0.00
BLACW Bellevue Life Sciences Acquisition Corp. Warrant $0.04 0.00
BNIX Bannix Acquisition Corp. Common Stock $11.33 0.00
BNIXR Bannix Acquisition Corp. Right $0.0932 0.00
BNIXW Bannix Acquisition Corp. Warrant $0.0276 0.00
BOWN Bowen Acquisition Corp Ordinary Shares $10.93 0.00
BOWNR Bowen Acquisition Corp Rights $0.208 0.00
BOWNU Bowen Acquisition Corp Unit $11.50 0.00
BRAC Broad Capital Acquisition Corp Common Stock $11.75 0.00
BRACR Broad Capital Acquisition Corp Rights $0.141 0.00
BSII Black Spade Acquisition II Co Class A Ordinary Share $9.94 0.00
BSIIW Black Spade Acquisition II Co Warrant $0.4801 0.00
BUJA Bukit Jalil Global Acquisition 1 Ltd. Ordinary Shares $11.18 0.00
BUJAR Bukit Jalil Global Acquisition 1 Ltd. Rights $0.1001 0.00
BUJAW Bukit Jalil Global Acquisition 1 Ltd. Warrants $0.0254 0.00
BYNO byNordic Acquisition Corporation Class A Common Stock $11.39 0.00
BYNOU byNordic Acquisition Corporation Units $11.35 0.00
BYNOW byNordic Acquisition Corporation Warrant $0.0353 0.00
CAMP CAMP4 Therapeutics Corporation Common Stock $4.61 0.00
CAPN Cayson Acquisition Corp Ordinary shares $10.0305 0.00
CCD Calamos Dynamic Convertible & Income Fund Common Stock $24.50 0.00
CCIR Cohen Circle Acquisition Corp. I Class A Ordinary Shares $10.01 0.00
CCIRU Cohen Circle Acquisition Corp. I Unit $10.13 0.00
CCIRW Cohen Circle Acquisition Corp. I Warrant $0.32 0.00
CCIX Churchill Capital Corp IX Ordinary Shares $10.37 0.00
CCIXU Churchill Capital Corp IX Unit $10.54 0.00
CCIXW Churchill Capital Corp IX Warrant $0.76 0.00
CEPO Cantor Equity Partners I Inc. Class A Ordinary Shares $10.05 0.00
CGO Calamos Global Total Return Fund Common Stock $11.315 0.00
CHAR Charlton Aria Acquisition Corporation Class A Ordinary Shares $9.94 0.00
CHI Calamos Convertible Opportunities and Income Fund Common Stock $11.64 0.00
CHW Calamos Global Dynamic Income Fund Common Stock $6.765 0.00
CHY Calamos Convertible and High Income Fund Common Stock $11.9908 0.00
CITE Cartica Acquisition Corp Class A Ordinary Shares $11.70 0.00
CITEW Cartica Acquisition Corp Warrant $0.27 0.00
CLRC ClimateRock Class A Ordinary Shares $11.79 0.00
CLRCW ClimateRock Warrant $0.0218 0.00
CNCKW Coincheck Group N.V. Warrants $1.88 0.00
CSLM CSLM Acquisition Corp. Class A Ordinary Share $11.82 0.00
CSLMR CSLM Acquisition Corp. Right $0.156 0.00
CSLMU CSLM Acquisition Corp. Unit $12.11 0.00
CSLMW CSLM Acquisition Corp. Warrant $0.08 0.00
CUBA Herzfeld Caribbean Basin Fund Inc. (The) Common Stock $2.34 0.00
CUBWU Lionheart Holdings Unit $10.02 0.00
CUBWW Lionheart Holdings Warrant $0.13 0.00
DECA Denali Capital Acquisition Corp. Class A Ordinary Shares $11.99 0.00
DECAW Denali Capital Acquisition Corp. Warrant $0.0372 0.00
DIST Distoken Acquisition Corporation Ordinary Shares $11.20 0.00
DISTR Distoken Acquisition Corporation Right $0.1202 0.00
DRDBU Roman DBDR Acquisition Corp. II Unit $9.96 0.00
DTSQ DT Cloud Star Acquisition Corporation Ordinary Shares $10.09 0.00
DYCQ DT Cloud Acquisition Corporation Ordinary Shares $10.4541 0.00
DYCQU DT Cloud Acquisition Corporation Unit $10.52 0.00
DYNX Dynamix Corporation Class A Ordinary Shares $9.78 0.00
DYNXU Dynamix Corporation Unit $9.92 0.00
DYNXW Dynamix Corporation Warrant $0.27 0.00
ELPW Elong Power Holding Limited Class A Ordinary Shares $1.37 0.00
EMCGR Embrace Change Acquisition Corp Rights $0.0753 0.00
ESHAR ESH Acquisition Corp. Right $0.0751 0.00
EURK Eureka Acquisition Corp Class A Ordinary Share $10.16 0.00
EURKR Eureka Acquisition Corp Right $0.20 0.00
EURKU Eureka Acquisition Corp Unit $10.32 0.00
EVGR Evergreen Corporation Class A Ordinary Share $11.90 0.00
EVGRW Evergreen Corporation Warrant $0.0301 0.00
FACT FACT II Acquisition Corp. Class A Ordinary Shares $9.89 0.00
FACTU FACT II Acquisition Corp. Unit $9.9775 0.00
FORL Four Leaf Acquisition Corporation Class A Common Stock $11.07 0.00
FORLW Four Leaf Acquisition Corporation Warrants $0.0594 0.00
FSHP Flag Ship Acquisition Corp. Ordinary Shares $10.16 0.00
FSHPR Flag Ship Acquisition Corp. Right $0.1101 0.00
FTII FutureTech II Acquisition Corp. Class A Common Stock $11.95 0.00
FVN Future Vision II Acquisition Corporation Ordinary shares $10.04 0.00
FVNNU Future Vision II Acquisition Corporation Units $10.515 0.00
GAINI Gladstone Investment Corporation 7.875% Notes due 2030 $25.36 0.00
GATE Marblegate Acquisition Corp. Class A Common Stock $10.9001 0.00
GATEW Marblegate Acquisition Corp. Warrant $0.03 0.00
GBBK Global Blockchain Acquisition Corp. Common Stock $11.06 0.00
GBBKR Global Blockchain Acquisition Corp. Right $0.1055 0.00
GDST Goldenstone Acquisition Limited Common Stock $11.29 0.00
GDSTW Goldenstone Acquisition Limited Warrants $0.0192 0.00
GECCH Great Elm Capital Corp. 8.125% Notes Due 2029 $24.48 0.00
GECCI Great Elm Capital Corp. 8.50% NOTES DUE 2029 $25.06 0.00
GECCO Great Elm Capital Corp. 5.875% Notes due 2026 $24.78 0.00
GECCZ Great Elm Capital Corp. 8.75% Notes due 2028 $25.20 0.00
GIG GigCapital7 Corp. Class A Ordinary Share $10.02 0.00
GIGGW GigCapital7 Corp. Warrant $0.07 0.00
GITS Global Interactive Technologies Inc. Common Stock $0.2266 0.00
GLAC Global Lights Acquisition Corp Ordinary Shares $10.66 0.00
GLST Global Star Acquisition Inc. Class A Common Stock $11.99 0.00
GLSTR Global Star Acquisition Inc. Right $0.1505 0.00
GLSTW Global Star Acquisition Inc. Warrants $0.03 0.00
GODN Golden Star Acquisition Corporation Ordinary Shares $11.23 0.00
GODNR Golden Star Acquisition Corporation Rights $0.31 0.00
GODNU Golden Star Acquisition Corporation Unit $11.47 0.00
GPAT GP-Act III Acquisition Corp. Class A Ordinary Share $10.15 0.00
GPATW GP-Act III Acquisition Corp. Warrants $0.14 0.00
GSRT GSR III Acquisition Corp. Ordinary Shares $9.905 0.00
GSRTR GSR III Acquisition Corp. Right $1.19 0.00
GSRTU GSR III Acquisition Corp. Unit $10.06 0.00
GUTS Fractyl Health Inc. Common Stock $2.01 0.00
HCVI Hennessy Capital Investment Corp. VI Class A Common Stock $10.64 0.00
HCVIU Hennessy Capital Investment Corp. VI Unit $10.57 0.00
HCVIW Hennessy Capital Investment Corp. VI Warrant $0.131 0.00
HLXB Helix Acquisition Corp. II Class A Ordinary Shares $10.99 0.00
HOND HCM II Acquisition Corp. Class A Ordinary Shares $10.03 0.00
HONDU HCM II Acquisition Corp. Unit $10.10 0.00
HONDW HCM II Acquisition Corp. Warrant $0.12 0.00
HSPO Horizon Space Acquisition I Corp. Ordinary Shares $11.34 0.00
HSPOR Horizon Space Acquisition I Corp. Right $0.1378 0.00
HSPOW Horizon Space Acquisition I Corp. Warrant $0.027 0.00
HSPTU Horizon Space Acquisition II Corp. Units $10.04 0.00
IBAC IB Acquisition Corp. Common Stock $10.165 0.00
IBACR IB Acquisition Corp. Right $0.06 0.00
INLF INLIF LIMITED Ordinary Shares $4.93 0.00
IPXX Inflection Point Acquisition Corp. II Class A Ordinary Share $13.10 0.00
IPXXU Inflection Point Acquisition Corp. II Unit $15.48 0.00
IPXXW Inflection Point Acquisition Corp. II Warrant $0.6199 0.00
IROH Iron Horse Acquisitions Corp. Common Stock $10.29 0.00
IROHR Iron Horse Acquisitions Corp. Right $0.392 0.00
IROHU Iron Horse Acquisitions Corp. Unit $10.62 0.00
IROHW Iron Horse Acquisitions Corp. Warrant $0.05 0.00
ISRL Israel Acquisitions Corp Class A Ordinary Shares $11.3008 0.00
ISRLU Israel Acquisitions Corp Unit $11.31 0.00
ISRLW Israel Acquisitions Corp Warrant $0.0425 0.00
IVCA Investcorp AI Acquisition Corp. Class A Ordinary Share $11.8355 0.00
IVCAW Investcorp AI Acquisition Corp. Warrant $0.03 0.00
JVSA JV SPAC Acquisition Corp. Class A Ordinary Share $10.43 0.00
JVSAR JV SPAC Acquisition Corp. Right $0.2662 0.00
KVAC Keen Vision Acquisition Corporation Ordinary Shares $11.00 0.00
KVACW Keen Vision Acquisition Corporation Warrant $0.0725 0.00
LATGU Chenghe Acquisition I Co. Unit $13.95 0.00
LOT Lotus Technology Inc. American Depositary Shares $3.7329 0.00
LPAA Launch One Acquisition Corp. Class A Ordinary shares $10.045 0.00
LPAAU Launch One Acquisition Corp. Unit $10.09 0.00
LPAAW Launch One Acquisition Corp. Warrant $0.15 0.00
LPBB Launch Two Acquisition Corp. Class A Ordinary Shares $9.96 0.00
LPBBU Launch Two Acquisition Corp. Unit $10.05 0.00
LPBBW Launch Two Acquisition Corp. Warrant $0.22 0.00
MACI Melar Acquisition Corp. I Class A Ordinary Shares $10.09 0.00
MACIW Melar Acquisition Corp. I Warrant $0.13 0.00
MBAV M3-Brigade Acquisition V Corp. Class A Ordinary shares $10.06 0.00
MBAVU M3-Brigade Acquisition V Corp. Units $10.14 0.00
MBAVW M3-Brigade Acquisition V Corp. Warrant $0.18 0.00
MBINL Merchants Bancorp Depositary Shares Each Representing a 1/40thInterest in a Share of 7.25% Fixed Rate Series E Non-CumulativePerpetual Preferred Stock without par value $25.41 0.00
MDCX Medicus Pharma Ltd. Common Stock   $2.85 0.00
METCZ Ramaco Resources Inc. 8.375% Senior Notes due 2029 $25.22 0.00
MKZR MacKenzie Realty Capital Inc. Common Stock $2.975 0.00
MLACU Mountain Lake Acquisition Corp. Units $10.02 0.00
MLEC Moolec Science SA Ordinary Shares $0.9199 0.00
MLECW Moolec Science SA Warrant $0.027 0.00
MSSA Metal Sky Star Acquisition Corporation Ordinary shares $12.67 0.00
MSSAW Metal Sky Star Acquisition Corporation Warrant $0.025 0.00
NETD Nabors Energy Transition Corp. II Class A Ordinary Shares $10.83 0.00
NETDU Nabors Energy Transition Corp. II Unit $10.84 0.00
NETDW Nabors Energy Transition Corp. II Warrant $0.18 0.00
NEWTG NewtekOne Inc. 8.50% Fixed Rate Senior Notes due 2029 $25.35 0.00
NEWTH NewtekOne Inc. 8.625% Fixed Rate Senior Notes due 2029 $25.685 0.00
NOEMU CO2 Energy Transition Corp. Unit $10.03 0.00
NTWO Newbury Street II Acquisition Corp Class A Ordinary Shares $9.93 0.00
NTWOU Newbury Street II Acquisition Corp Unit $9.9814 0.00
NTWOW Newbury Street II Acquisition Corp Warrant $0.14 0.00
NVAWW Nova Minerals Limited Warrant $9.25 0.00
OACC Oaktree Acquisition Corp. III Life Sciences Class A Ordinary Share $9.97 0.00
OACCU Oaktree Acquisition Corp. III Life Sciences Unit $10.20 0.00
OACCW Oaktree Acquisition Corp. III Life Sciences Warrant $1.00 0.00
OAKU Oak Woods Acquisition Corporation Class A Ordinary Shares $11.37 0.00
OAKUR Oak Woods Acquisition Corporation Right $0.2253 0.00
OAKUW Oak Woods Acquisition Corporation Warrant $0.05 0.00
OCCIM OFS Credit Company Inc. 7.875% Series F Term Preferred Stock $25.07 0.00
OXLCI Oxford Lane Capital Corp. 8.75% Notes due 2030 $25.6725 0.00
OXSQZ Oxford Square Capital Corp. 6.25% Notes due 2026 $24.88 0.00
PCSC Perceptive Capital Solutions Corp Class A Ordinary Shares $10.17 0.00
PLAO Patria Latin American Opportunity Acquisition Corp. Class A Ordinary Shares $11.64 0.00
PLMJ Plum Acquisition Corp. III Class A Ordinary Shares $11.15 0.00
POLE Andretti Acquisition Corp. II Class A Ordinary Shares $10.02 0.00
POLEU Andretti Acquisition Corp. II Unit $10.08 0.00
POLEW Andretti Acquisition Corp. II Warrant $0.15 0.00
PPYA Papaya Growth Opportunity Corp. I Class A Common Stock $11.25 0.00
PPYAU Papaya Growth Opportunity Corp. I Unit $11.19 0.00
PPYAW Papaya Growth Opportunity Corp. I Warrant $0.0211 0.00
PWUP PowerUp Acquisition Corp. Class A Ordinary Shares $11.465 0.00
PWUPW PowerUp Acquisition Corp. Warrant $0.04 0.00
QETA Quetta Acquisition Corporation Common Stock $10.55 0.00
QETAR Quetta Acquisition Corporation Right $1.80 0.00
QQQX Nuveen NASDAQ 100 Dynamic Overwrite Fund Shares of Beneficial Interest $27.10 0.00
RADX Radiopharm Theranostics Limited American Depositary Shares $4.27 0.00
RAIN Rain Enhancement Technologies Holdco Inc. Class A Common Stock $4.97 0.00
RAINW Rain Enhancement Technologies Holdco Inc. Warrants $0.179 0.00
RANGU Range Capital Acquisition Corp. Units $10.0112 0.00
RDAC Rising Dragon Acquisition Corp. Ordinary Shares $10.02 0.00
RENE Cartesian Growth Corporation II Class A Ordinary Shares $11.75 0.00
RFAC RF Acquisition Corp. Class A Common Stock $11.35 0.00
RFACR RF Acquisition Corp. Rights $0.1807 0.00
RFACW RF Acquisition Corp. Warrants $0.057 0.00
RFAI RF Acquisition Corp II Ordinary Shares $10.2427 0.00
RFAIR RF Acquisition Corp II Right $0.0601 0.00
RFAIU RF Acquisition Corp II Unit $10.26 0.00
SIMA SIM Acquisition Corp. I Class A Ordinary Shares $10.0651 0.00
SIMAU SIM Acquisition Corp. I Unit $10.08 0.00
SIMAW SIM Acquisition Corp. I Warrant $0.1108 0.00
SKGR SK Growth Opportunities Corporation Class A Common Stock $11.6201 0.00
SKGRW SK Growth Opportunities Corporation Warrant $0.51 0.00
SPHAU Shepherd Ave Capital Acquisition Corporation Unit $10.02 0.00
SPKL Spark I Acquisition Corp. Class A Ordinary Share $10.61 0.00
SPKLW Spark I Acquisition Corp. Warrant $0.11 0.00
STAI ScanTech AI Systems Inc. Common stock $2.5499 0.00
SVII Spring Valley Acquisition Corp. II Class A Ordinary Shares $11.26 0.00
SVIIR Spring Valley Acquisition Corp. II Rights $0.09 0.00
SVIIU Spring Valley Acquisition Corp. II Unit $11.1914 0.00
SVIIW Spring Valley Acquisition Corp. II Warrant $0.0551 0.00
TAVI Tavia Acquisition Corp. Ordinary Shares $9.92 0.00
TAVIR Tavia Acquisition Corp. Right $0.12 0.00
TAVIU Tavia Acquisition Corp. Unit $10.02 0.00
TBMC Trailblazer Merger Corporation I Class A Common Stock $11.13 0.00
TBMCR Trailblazer Merger Corporation I Rights $0.2399 0.00
TDACU Translational Development Acquisition Corp. Units $10.0206 0.00
TETE Technology & Telecommunication Acquisition Corporation Class A Ordinary Shares $12.39 0.00
TETEW Technology & Telecommunication Acquisition Corporation Warrant $0.0137 0.00
TNMG TNL Mediagene Ordinary Shares $6.40 0.00
VACH Voyager Acquisition Corp Class A Ordinary Shares $10.12 0.00
VACHU Voyager Acquisition Corp Unit $10.15 0.00
VACHW Voyager Acquisition Corp Warrants $0.12 0.00
VCIC Vine Hill Capital Investment Corp. Class A Ordinary Shares $10.04 0.00
VCICU Vine Hill Capital Investment Corp. Unit $10.09 0.00
VCICW Vine Hill Capital Investment Corp. Warrant $0.15 0.00
VLYPN Valley National Bancorp 8.250% Fixed-Rate Reset Non-Cumulative Perpetual Preferred Stock Series C $25.58 0.00
VMCA Valuence Merger Corp. I Class A Ordinary Shares $11.54 0.00
VMCAW Valuence Merger Corp. I Warrant $0.0705 0.00
WAVS Western Acquisition Ventures Corp. Common Stock $12.20 0.00
WAVSU Western Acquisition Ventures Corp. Unit $13.75 0.00
WAVSW Western Acquisition Ventures Corp. Warrant $0.04 0.00
WINV WinVest Acquisition Corp. Common Stock $12.06 0.00
WINVW WinVest Acquisition Corp. Warrant $0.0334 0.00
WLAC Willow Lane Acquisition Corp. Class A Ordinary Shares $9.87 0.00
WLACU Willow Lane Acquisition Corp. Unit $10.0201 0.00
WLACW Willow Lane Acquisition Corp. Warrants $0.20 0.00
YHNA YHN Acquisition I Limited Ordinary Shares $10.06 0.00
YOTA Yotta Acquisition Corporation Common Stock $11.27 0.00
YOTAW Yotta Acquisition Corporation Warrant $0.0526 0.00
ZOOZ ZOOZ Power Ltd. Ordinary Shares $2.38 0.00
ZYBT Zhengye Biotechnology Holding Limited Ordinary Shares $4.81 0.00
