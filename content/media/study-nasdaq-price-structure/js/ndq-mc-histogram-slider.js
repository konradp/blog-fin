window.addEventListener('load', Run);

function Run() {
  console.log('test');
  const predefinedValues = ['e05', 'e06', 'e07', 'e08', 'e09', 'e10', 'max'];

  const slider = document.getElementById('range-mc');
  const displayValue = document.getElementById('display-mc');

  slider.addEventListener('input', () => {
    const value = predefinedValues[slider.value];
    displayValue.textContent = value;
    const charts = Array.from(document.getElementById('ndq-mc-histograms').children);
    charts.forEach(i => {
      i.style.display = 'none';
      if (i.getAttribute('chartId') == value) {
        i.style.display = 'block';
      }
    });
  });

  const len = predefinedValues.length-1;
  slider.value = len;
  displayValue.textContent = predefinedValues[len];
}
