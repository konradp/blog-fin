let DATA = null;
let CHART = null;
//window.addEventListener('load', Draw);

fetch('/blog-fin/media/study-nasdaq-price-structure/src1_ndq_all.json')
  .then(res => res.json())
  .then(data => {
    DATA = data;
    Draw();
  });

function Draw() {
  console.log('data', DATA[0]);
  let type = 'Sector';

  let typeMap = new Map();
  DATA.forEach(i => {
    typeMap.set(i[type], (typeMap.get(i[type]) || 0) + 1);
  });
  //console.log('typeMap', typeMap);
  const sorted = [...typeMap.entries()].sort((a, b) => b[1] - a[1]);
  const labels = sorted.map(entry => entry[0]);
  const counts = sorted.map(entry => entry[1]);
  //const percs = sorted.map
  //const labels = Array.from(typeMap.keys());
  //const counts = Array.from(typeMap.values());
  //console.log('labels', labels);


  const ctx = document.getElementById('chart');
  CHART = new Chart(ctx, {
    type: 'doughnut',
    data: {
      //labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
      labels: labels,
      datasets: [{
        label: 'count',
        //data: [12, 19, 3, 5, 2, 3],
        data: counts,
        borderWidth: 1
      }]
    },
    options: {
      //scales: {
      //  y: {
      //    beginAtZero: true
      //  }
      //}
      plugins: {
        tooltip: {
          callbacks: {
            label: function(tooltipItem) {
              const dataset = tooltipItem.dataset.data;
              const total = dataset.reduce((sum, value) => sum + value, 0);
              const value = dataset[tooltipItem.dataIndex];
              const percentage = ((value / total) * 100).toFixed(2); // Calculate percentage
              return `${labels[tooltipItem.dataIndex]}: ${value} (${percentage}%)`;
            }
          }
        }
      }




    }
  });
}
