window.addEventListener('load', Run);

function Run() {
  console.log('test');
  const predefinedValues = [1, 10, 100, 500, 5000];

  const slider = document.getElementById('boxplot-slider');
  const displayValue = document.getElementById('boxplot-value');

  slider.addEventListener('input', () => {
    const value = predefinedValues[slider.value];
    displayValue.textContent = value;
    const charts = Array.from(document.getElementById('ndq-price-boxplots').children);
    charts.forEach(i => {
      i.style.display = 'none';
      if (i.getAttribute('chartId') == value) {
        i.style.display = 'block';
      }
    });
  });

  const len = predefinedValues.length-1;
  slider.value = len;
  displayValue.textContent = predefinedValues[len];
}
