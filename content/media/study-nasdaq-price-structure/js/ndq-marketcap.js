let DATA = null;
let DATA_CHART = {
  values: null,
};
let CHART = null;
//window.addEventListener('load', Draw);

fetch('/blog-fin/media/study-nasdaq-price-structure/src1_ndq_all.json')
  .then(res => res.json())
  .then(data => {
    DATA = data;
    Calculate();
    Draw();
  });

function range(start, end, step = 1) {
  let result = [];
  for (let i = start; step > 0 ? i < end : i > end; i += step) {
    //console.log(i);
    result.push(i);
  }
  return result;
}


function Calculate() {
  let data = [];
  //console.log('data', DATA[0]);
  //et result =
  //str.replace(/^\$/, "")
  //.replace(/%$/, "");
  for (let i of DATA) {
    //console.log(i);
    //if (j >= 4) {
    //  break;
    //}
    let j = {
      symbol: i['Symbol'],
      perc_change: parseFloat(i['% Change'].replace(/%$/, "")),
      country: i['Country'],
      ipo_year: i['IPO Year'],
      industry: i['Industry'],
      price: parseFloat(i['Last Sale'].replace(/^\$/, "")),
      market_cap: i['Market Cap']/1000000,
      name: i['Name'],
      net_change: i['Net Change'],
      sector: i['Sector'],
      volume: i['Volume'],
    };
    //data.push(j);
    if (j['market_cap'] <= 100) {
      data.push(j);
    }
  }
  DATA = data;
  // DEBUG
  for (let k of DATA) {
    //console.log(k);
    if (k['price'] >= 500) {
      console.log(k['symbol'], k['price']);
    }
  }
  console.log('data_new', DATA[0]);
  //let d = data.map(i => i.market_cap);
  let max = Math.max(...data.map(i => i.market_cap));
  let min = Math.min(...data.map(i => i.market_cap));
  console.log('max', max, 'min', min);
  let binSize = 10;
  let bins = range(binSize, max+100, binSize);
  console.log('bins.length', bins.length);
  let counts = Array(bins.length).fill(0);
  console.log('bins', bins, counts);
  //data[1].price = 399;
  
  for (let i in data) {
    //if (i == 10) {
    //  break;
    //}
    let idx = parseInt((data[i].market_cap)/binSize);
    counts[idx] += 1;
  }
  console.log('counts', counts);
  DATA_CHART.values = counts;
  DATA_CHART.labels = bins;
}

function Draw() {
  const ctx = document.getElementById('chart');
  CHART = new Chart(ctx, {
    type: 'bar',
    data: {
      //labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
      labels: DATA_CHART.labels,
      datasets: [{
        label: 'count',
        //data: [12, 19, 3, 5, 2, 3],
        data: DATA_CHART.values,
        borderWidth: 1
      }]
    },
    options: {
      //scales: {
      //  y: {
      //    beginAtZero: true
      //  }
      //}
      plugins: {
        tooltip: {
          callbacks: {
            label: function(tooltipItem) {
              const dataset = tooltipItem.dataset.data;
              const total = dataset.reduce((sum, value) => sum + value, 0);
              const value = dataset[tooltipItem.dataIndex];
              const percentage = ((value / total) * 100).toFixed(2);
              return `${DATA_CHART.labels[tooltipItem.dataIndex]}: ${value} (${percentage}%)`;
            }
          }
        }
      }
    }
  });
}
