window.addEventListener('load', Run);

function Run() {
  console.log('test');
  const predefinedValues = ['e04', 'e05', 'e06', 'e07', 'e08', 'e09', 'e10', 'e11', 'e12', 'max'];

  const slider = document.getElementById('mc-boxplot-slider');
  const displayValue = document.getElementById('mc-boxplot-value');

  slider.addEventListener('input', () => {
    const value = predefinedValues[slider.value];
    displayValue.textContent = value;
    const charts = Array.from(document.getElementById('ndq-mc-boxplots').children);
    charts.forEach(i => {
      i.style.display = 'none';
      if (i.getAttribute('chartId') == value) {
        i.style.display = 'block';
      }
    });
  });

  const len = predefinedValues.length-1;
  slider.value = len;
  displayValue.textContent = predefinedValues[len];
}
