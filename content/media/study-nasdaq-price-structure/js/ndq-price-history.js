let DATA = null;
let CHART = null;


fetch('/blog-fin/media/study-nasdaq-price-structure/ndq-price-history/IXIC_cleaned.json')
  .then(res => res.json())
  .then(data => {
    DATA = data;
    Draw();
  });


function range(start, end, step = 1) {
  const result = [];
  for (let i = start; step > 0 ? i < end : i > end; i += step) {
    result.push(i);
  }
  return result;
}


function Draw() {
  var slider = document.getElementById('slider');
  noUiSlider.create(slider, {
    start: [20, 80],
    connect: true,
    range: {
      'min': 0,
      'max': 100
    }
  });


  const ctx = document.getElementById('chart');
  const data = {
    labels: DATA.t,
    datasets: [{
      //label: 'count',
      //data: [12, 19, 3, 5, 2, 3],
      data: DATA.p,
      borderWidth: 1,
      pointRadius: 0,
    }]
  };
  CHART = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
      plugins: {
        legend: {
          display: false,
        },
        tooltip: {
          mode: 'index',
          intersect: false,
        }, // tooltip
      }, // plugins
    }, // options
  });
}
