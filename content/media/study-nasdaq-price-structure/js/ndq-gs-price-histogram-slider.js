window.addEventListener('load', Run);

function Run() {
  console.log('test');
  const predefinedValues = [1, 10, 100, 500, 1000, 'max'];

  const slider = document.getElementById('ndq-gs-price-histogram-slider');
  const displayValue = document.getElementById('display-value');

  slider.addEventListener('input', () => {
    const value = predefinedValues[slider.value];
    displayValue.textContent = value;
    const charts = Array.from(document.getElementById('ndq-gs-price-histograms').children);
    charts.forEach(i => {
      i.style.display = 'none';
      if (i.getAttribute('chartId') == value) {
        i.style.display = 'block';
      }
    });
  });

  const len = predefinedValues.length-1;
  slider.value = len;
  displayValue.textContent = predefinedValues[len];
}
