window.addEventListener('load', Run);

function Run() {
  console.log('test');
  const predefinedValues = [100, 500, 1000, '1000_2'];

  const slider = document.getElementById('ndq100-price-histogram-slider');
  const displayValue = document.getElementById('ndq100-price-histogram-value');

  slider.addEventListener('input', () => {
    const value = predefinedValues[slider.value];
    displayValue.textContent = value;
    const charts = Array.from(document.getElementById('ndq100-price-histograms').children);
    charts.forEach(i => {
      i.style.display = 'none';
      if (i.getAttribute('chartId') == value) {
        i.style.display = 'block';
      }
    });
  });

  const len = predefinedValues.length-1;
  slider.value = len;
  displayValue.textContent = predefinedValues[len];
}
