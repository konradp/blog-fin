let DATA = null;
let DATA_CHART = {
  values: null,
};
let CHART = null;
//window.addEventListener('load', Draw);

fetch('/blog-fin/media/study-nasdaq-price-structure/src1_ndq_all.json')
  .then(res => res.json())
  .then(data => {
    DATA = data;
    Calculate();
    Draw();
  });

function range(start, end, step = 1) {
  const result = [];
  for (let i = start; step > 0 ? i < end : i > end; i += step) {
    result.push(i);
  }
  return result;
}


function Calculate() {
  let data = [];
  for (let i of DATA) {
    let j = {
      symbol: i['Symbol'],
      perc_change: parseFloat(i['% Change'].replace(/%$/, "")),
      country: i['Country'],
      ipo_year: i['IPO Year'],
      industry: i['Industry'],
      price: parseFloat(i['Last Sale'].replace(/^\$/, "")),
      market_cap: i['Market Cap'],
      name: i['Name'],
      net_change: i['Net Change'],
      sector: i['Sector'],
      volume: i['Volume'],
    };
    if (j['price'] <= 100) {
      data.push(j);
    }
  }
  DATA = data;
  // DEBUG
  //for (let k of DATA) {
  //  if (k['price'] >= 500) {
  //    console.log(k['symbol'], k['price']);
  //  }
  //}
  console.log('data_new', DATA[0]);
  let d = data.map(i => i.price);
  let max = Math.max(...data.map(i => i.price));
  console.log('max', max);
  let binSize = 10;
  let bins = range(binSize, max+binSize, binSize);
  let counts = Array(bins.length).fill(0);
  console.log('bins', bins, counts);
  //data[1].price = 399;
  
  for (let i in data) {
    //if (i == 10) {
    //  break;
    //}
    let idx = parseInt((data[i].price)/binSize);
    counts[idx] += 1;
  }
  console.log('counts', counts);
  DATA_CHART.values = counts;
  DATA_CHART.labels = bins;
}

function Draw() {
  const ctx = document.getElementById('chart');
  CHART = new Chart(ctx, {
    type: 'bar',
    data: {
      //labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
      labels: DATA_CHART.labels,
      datasets: [{
        label: 'count',
        //data: [12, 19, 3, 5, 2, 3],
        data: DATA_CHART.values,
        borderWidth: 1
      }]
    },
    options: {
      //scales: {
      //  y: {
      //    beginAtZero: true
      //  }
      //}
      plugins: {
        tooltip: {
          callbacks: {
            label: function(tooltipItem) {
              const dataset = tooltipItem.dataset.data;
              const total = dataset.reduce((sum, value) => sum + value, 0);
              const value = dataset[tooltipItem.dataIndex];
              const percentage = ((value / total) * 100).toFixed(2); // Calculate percentage
              return `${DATA_CHART.labels[tooltipItem.dataIndex]}: ${value} (${percentage}%)`;
            }
          }
        }, //tooltip
      } // plugins
    }
  });
}
