#!/usr/bin/env python3
# Prints stocks for which the specified field is empty
import csv
import sys

if len(sys.argv) < 3:
  print(f'Usage: {sys.argv[0]} FILE FIELD')
  print(f'Example: {sys.argv[0]} data.csv "Last Sale"')
  exit(1)

FILE = sys.argv[1]
FIELD = sys.argv[2]

with open(FILE, mode='r') as f:
  reader = csv.DictReader(f)
  data = [row for row in reader]

for i in data:
  value = i[FIELD]
  if i[FIELD] == "":
    print(i['Symbol'], i['Name'], i['Market Cap'])
