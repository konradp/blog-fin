#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np

# Example data
FILES = [
  "ndq/00mc.txt",
  "ndq-gs/00mc.txt",
  "ndq100/00mc.txt",
]
LABELS = [
  "NASDAQ",
  "NASDAQ GS",
  "NASDAQ 100",
]
data = []

for F_NAME in FILES:
  with open(F_NAME,'r') as f:
    d = [float(line.strip()) for line in f]
    data.append(d)

# Print stats
print(f"{'NAME':<10} | {'COUNT':<10} | {'MIN':<10} | {'Q1':<10} | {'MEDIAN':<10} | {'Q3':<10} | {'MAX':<10}")
print(f"{'---':<10} | {'--:':<10} | {'--:':<10} | {'--:':<10} | {'--:':<10} | {'--:':<10} | {'--:':<10}")
for name, dataset in zip(LABELS, data):
  dataset = np.sort(dataset)
  min_val = dataset[0]
  q1 = np.percentile(dataset, 25)
  median = np.percentile(dataset, 50)
  q3 = np.percentile(dataset, 75)
  max_val = dataset[-1]
  count = len(dataset)
  print(f"{name:<10} | {count:<10} | {min_val:<10.2f} | {q1:<10.2f} | {median:<10.2f} | {q3:<10.2f} | {max_val:<10.2f}")

# Plot
plt.boxplot(data, patch_artist=True, boxprops=dict(facecolor='lightblue'), vert=False)
plt.yticks([1, 2, 3], LABELS)
plt.title('Price')
plt.tight_layout()
plt.savefig('bplot_mc.png', bbox_inches='tight')
plt.show()
