# This R script prints graphs of the 36 losers
# blog: https://konradp.gitlab.io
# title: Trading Friday losers on Monday
library("ggplot2")
library("jsonlite")
library("scales")

options(width=120)
palette <- c("#56B4E9", "#E69F00")
th <- theme() +
  theme(
    axis.line=element_line(colour="black"),
    axis.text=element_text(colour="black"),
    legend.position="top",
    panel.grid.major=element_blank(),
    panel.grid.minor=element_blank(),
    panel.background=element_blank(),
    text=element_text(size=20)
)
savep <- function(name) { ggsave(name, width=10, height=4) }

ch <- fromJSON("chart.json", flatten=TRUE)
dates <- seq(as.Date("2017-08-17"), by="day", length.out=386)
dates <- data.frame(date = dates)
for(s in names(ch)) {
  ch[[s]]$chart$date <- as.Date(ch[[s]]$chart$date)
  d <- data.frame(
    date=ch[[s]]$chart$date,
    value=ch[[s]]$chart$close
  )
  data <- merge(dates, d, by="date", all.x=TRUE)
  
  ggplot(data, aes(x=date, y=value, group=0)) +
    scale_x_date(date_breaks="1 month", labels = date_format("%m-%y")) +
    geom_line() +
    labs(x=paste(s, "Month (MM-YY)")) +
    th
  savep(paste("new/", s, ".png", sep=""))
}
