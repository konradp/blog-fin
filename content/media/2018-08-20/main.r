# This R script performs the calculations and prepares the graphs for the
# blog: https://konradp.gitlab.io
# title: Trading Friday losers on Monday
# desc: We study symbols with low price and negative day change, and their subsequent performance.

library(ggplot2)
library(plyr)
# Style and display
options(width=120)
palette <- c("#56B4E9", "#E69F00")
th <- theme() +
  theme(
    axis.line=element_line(colour="black"),
    axis.text=element_text(colour="black"),
    legend.position="top",
    panel.grid.major=element_blank(),
    panel.grid.minor=element_blank(),
    panel.background=element_blank(),
    text=element_text(size=20)
)

# A helper function for uniform plot images
savep <- function(name) { ggsave(name, width=10, height=4) }

# DATA
day0 <- read.csv("day0.csv", na.strings=c("NA", "NaN"), sep=",")
day1 <- read.csv("day1.csv", na.strings=c("NA", "NaN"), sep=",")

# Print missing data
cat("Missing data day0\n")
x <- day0$symbol[is.na(day0$changePerc)]
paste(x, collapse=" ")
cat("Missing data day1\n")
x <- day1$symbol[is.na(day1$changePerc)]
paste(x, collapse=" ")
cat("\n\n")

# Extrema
cat("day | min (%) | mean (%) | max (%)\n")
cat("--- | --- | --- | ---\n")
cat(
  "day 0 |",
  min(day0$changePerc), "|",
  mean(day0$changePerc[!is.na(day0$changePerc)]), "|",
  max(day0$changePerc), "\n"
)

cat(
  "day 1 |",
  min(day1$changePerc[!is.na(day1$changePerc)]), "|",
  mean(day1$changePerc[!is.na(day1$changePerc)]), "|",
  max(day1$changePerc[!is.na(day1$changePerc)]), "\n\n\n"
)

# Fig.1: Day 0 percent change distribution of all symbols, grouped by share price
day0allLo <- subset(
    day0,
    day0$price <= 10,
    select=c(symbol, changePerc))
day0allLo$type <- "less than $10"
day0allHi <- subset(
    day0,
    day0$price > 10,
    select=c(symbol, changePerc))
day0allHi$type <- "greater than $10"
day0allByPrice <- rbind(day0allHi, day0allLo)
# Plot
ggplot(
    day0allByPrice,
    aes(changePerc, fill = type)) +
    scale_fill_manual(
        "Price per share",
        values=palette) +
    geom_histogram(
        aes(y=..density..),
        binwidth=2,
        colour="grey30",
        position="dodge") +
    th +
    xlab("Day change (%)")
savep("fig1.png")

# Fig.2: Day 0 percent change distribution of all symbols, grouped by share price (detail)
ggplot(day0allByPrice, aes(changePerc, fill = type)) +
  scale_fill_manual("Price per share", values=palette) +
  geom_histogram(
    aes(y=..density..),
    binwidth=2,
    colour="black",
    position="dodge") +
  th +
  xlim(c(-15,15)) +
  xlab("Day change (%)")
savep("fig2.png")
#readline(prompt="Presss Return to continue")

# Fig.3: Symbols with percent change less than -10% on day 0, grouped by price per share
day0losersHi <- subset(
    day0,
    day0$changePerc <= -10 & day0$price > 10,
    select=c(symbol,changePerc))
day0losersHi$type <- "greater than 10$"
day0losersLo <- subset(
    day0,
    day0$changePerc <= -10 & day0$price <= 10,
    select=c(symbol,changePerc))
day0losersLo$type <- "less than 10$"
day0losersByPrice <- rbind(day0losersLo, day0losersHi)
# Plot
ggplot(day0losersByPrice, aes(changePerc, fill = type)) +
  geom_histogram(
      binwidth=1,
      colour="black",
      position="dodge"
  ) +
  scale_fill_manual("Price per share", values=palette) +
  th +
  xlab("Day change (%)")
savep("fig3.png")

# Prepare day0/day1 combined table
TlosersDay0 <- subset(day0losersLo, select=c(symbol,changePerc))
TlosersDay1 <- subset(day1, select=c(symbol,changePerc))
TlosersCombined <- merge(TlosersDay0, TlosersDay1, by="symbol")
TlosersCombined <- TlosersCombined[order(TlosersCombined$changePerc.x),,]
TlosersCombined$number <- paste(c(1:nrow(TlosersCombined)))

# Fig.4: Day 1 percent change distribution of all symbols, grouped by share price (detail).
day1allHi <- subset(day1, day1$price > 10, select=c(symbol,changePerc))
day1allHi$type <- "greater than $10"
day1allLo <- subset(day1, day1$price <= 10, select=c(symbol,changePerc))
day1allLo$type <- "less than $10"
day1allByPrice <- rbind(day1allHi, day1allLo)
# Plot
ggplot(day1allByPrice, aes(changePerc, fill = type)) +
  scale_fill_manual("Price per share", values=palette) +
  geom_histogram(
    aes(y=..density..),
    binwidth=2,
    colour="black",
    position="dodge") +
  th +
  xlim(c(-15,15)) +
  xlab("Day change (%)")
savep("fig4.png")

# Fig.5: Change pecent of all symbols on day 0 and day 1
day0all <- subset(day0, select=c(changePerc))
names(day0all) <- c("value")
day0all$type="day 0"
day1all <- subset(day1, select=c(changePerc))
names(day1all) <- c("value")
day1all$type="day 1"
day1d2 <- rbind(day0all, day1all)
day1d2$type <- factor(
  day1d2$type,
  levels=c("day 1", "day 0"),
  ordered=TRUE
)
# Plot
ggplot(
  data = day1d2,
  aes(x=type, y=value),
  horizontal=TRUE
) +
  coord_flip() +
  geom_boxplot() +
  th
savep("fig5.png")

# Fig.6: Change pecent of all symbols on day 0 and day 1
ggplot(
  data = day1d2,
  aes(x=type, y=value),
  horizontal=TRUE
) +
  coord_flip() +
  geom_boxplot() +
  th +
  ylim(c(-15,15))
savep("fig6.png")
#readline(prompt="Presss Return to continue")

# Fig.7: Boxplot, day0/day1: losers only
day0losers <- subset(TlosersCombined, select=c(changePerc.x))
names(day0losers) <- c("value")
day0losers$type="day 0"
day1losers <- subset(TlosersCombined, select=c(changePerc.y))
names(day1losers) <- c("value")
day1losers$type="day 1"
day1d2 <- rbind(day0losers, day1losers)
day1d2$type <- factor(
  day1d2$type,
  levels=c("day 1", "day 0"),
  ordered=TRUE
)
# Plot
ggplot(
  data = day1d2,
  aes(x=type, y=value),
  horizontal=TRUE
) +
  coord_flip() +
  geom_boxplot() +
  geom_hline(yintercept=0, linetype="dashed") +
  th
savep("fig7.png")
#readline(prompt="Presss Return to continue")

# Table: day 0 losers vs day 1
total <- 2
cat("id | symbol | day 0 change (%) | day 1 change (%)\n")
cat("--- | --- | --- | ---\n")
write.table(
  col.names=FALSE,
  TlosersCombined[,c("number", "symbol", "changePerc.x", "changePerc.y")],
  quote=FALSE,
  row.names=FALSE,
  sep=" | ")
cat(
  "Total |  | ",
  sum(TlosersCombined$changePerc.x),
  "|",
  sum(TlosersCombined$changePerc.y),
  "\n")
cat("... | ... | ... | ...\n\n")
#readline(prompt="Presss Return to continue")

# Table: day 0 winners
winners <- head(arrange(day1, desc(changePerc)), n=10)
winners <- data.frame(
  symbol=winners$symbol,
  changePerc=winners$changePerc,
  isLoser=""
)
winners <- merge(
  x=winners,
  y=day0[,c("symbol","close")],
  by="symbol"
)
winners <- head(arrange(winners, desc(changePerc)), n=10)
cat("day 1 winner | day 1 changePerc | day 0 price | is day 0 loser\n")
cat("--- | --- | --- | ---\n")
write.table(
  col.names=FALSE,
  winners[,c("symbol", "changePerc", "close", "isLoser")],
  quote=FALSE,
  row.names=FALSE,
  sep=" | ")

