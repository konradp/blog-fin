Install prerequisites and R packages.
```
apt install r-base-core
sudo R
install.packages("ggplot2")
install.packages("dplyr")
install.packages("magrittr")
```

Run the R scripts as follows.
```
R
source("main.r")
source("main.r", print.eval=TRUE)
```

## Data
Data was captured using the https://gitlab.com/konradp/fintools.git program, specifically:
```
cd fintools
examples/bin/GetDaySnapshotR 20180817
examples/bin/GetDaySnapshotR 20180820
```

Missing data day1
```
ABP ABPRO CORP
status: delisted on 2018-05-16

ALZH Alzheon
status: withdrawn on 2018-05-01 after one day

BNGO Bionano Genomics
status: IPO on 2018-08-24

CMCTP Commercial Trust Corporation
status: glitch, legit

DDOC DERMAdoctor
status: unclear

EHR Energy Hunter Resources, Inc.
status: unclear

GBLK GoodBulk Ltd.
status: unclear
```
