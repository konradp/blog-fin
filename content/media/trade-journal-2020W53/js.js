function Load() {
  UpdateTable(10)
}

function UpdateTable(e) {
  perc = e
  b = document.getElementById('label')
  b.innerHTML = perc
  t = document.getElementById('table')
  //tbody = t.childNodes[2].childNodes[3].childNodes
  t.childNodes.forEach((t1) => {
    if (t1.nodeName == 'TABLE') {
      table = t1
    }
  })
  table.childNodes.forEach((t1) => {
    if (t1.nodeName == 'TBODY') {
      tbody = t1
    }
  })
  
  tbody.childNodes.forEach((el) => {
    if (el.nodeName != 'TR') {
      return
    }

    children = el.childNodes;
    [3, 5, 7, 9, 11].forEach((i) => {
      val = children[i].innerHTML
      if (parseFloat(val) >= perc) {
        children[i].className = 'discard'
      } else {
        children[i].className = ''
      }
    })
  })
}
