let results = [];
let initialInvest = null;
let takeProfit = null;
let stopLoss = null;
let days = null;
let successRate = null;
// results = [
// {
//   balanceIn: 100.23,
//   gain: 4/-1,
//   isWin: true/false,
//   invest: 100,
//   balanceOut: 104,
// }
// ]


// Helper functions
function main() {
  _get('run').addEventListener('click', onRun);
}
function _get(id) {
  return document.getElementById(id);
}
function _mk(id) {
  return document.createElement(id);
}


// Main functions
function onRun() {
  results = [];
  // Get params
  initialInvest = parseInt(_get('investment').value);
  takeProfit = parseInt(_get('take-profit').value);
  stopLoss = parseInt(_get('stop-loss').value);
  days = parseInt(_get('days').value);
  successRate = parseInt(_get('success-rate').value);
  let countSuccess = parseInt((successRate/100)*days);
  let countFail = days - countSuccess;
  let successFailArr = createSuccessFailArr(countSuccess, countFail);
  calculateDays(successFailArr);
  drawTable();
}


function createSuccessFailArr(countSuccess, countFail) {
  // returns [ true, false, true, true, false, ... ], where
  // true: success
  // false: fail
  // This array represents days on which there was a successful trade,
  // and days on which there was an unsuccessful trade
  const array = new Array(countSuccess).fill(true)
  .concat(new Array(countFail).fill(false));
  // Shuffle the array
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]]; // Swap elements
  }
  return array;
}


function calculateDays(successFailArr) {
  for (let i = 0; i < days; i++) {
    let isWin = successFailArr[i];
    let percGain = (isWin)? takeProfit : -stopLoss;
    if (i == 0) {
      // First day
      prevDay = {
        balanceIn: null,
        gain: null,
        action: null,
        invest: null,
        balanceOut: initialInvest,
      }
    } else {
      prevDay = results[results.length-1];
    }
    // Amount invested is a multiple of 10$. The remainder is carried over
    let inv = Math.floor(prevDay.balanceOut/10)*10;
    let remainder = prevDay.balanceOut - inv;
    let soldPrice = inv + inv*percGain/100;
    res = {
      balanceIn: prevDay.balanceOut,
      gain: percGain,
      isWin: isWin,
      invest: inv,
      soldPrice: soldPrice,
      balanceOut: soldPrice + remainder,
    };
    results.push(res);
  }
}


function drawTable() {
  let tbl = _get('table');
  tbl.innerHTML = '';
  for (let i in results) {
    let res = results[i];
    let tr = _mk('tr');
    // day
    let tDay = _mk('td');
    tDay.innerHTML = i;
    tr.appendChild(tDay);
    // balanceIn
    let tIn = _mk('td');
    tIn.innerHTML = res.balanceIn.toFixed(4);
    tr.appendChild(tIn);
    // invest
    let tInvest = _mk('td');
    tInvest.innerHTML = res.invest;
    tr.appendChild(tInvest);
    // perc
    let tGain = _mk('td');
    tGain.className = (res.isWin)? 'green' : 'red';
    tGain.innerHTML = res.gain;
    tr.appendChild(tGain);
    // soldPrice
    let tSoldPrice = _mk('td');
    tSoldPrice.innerHTML = res.soldPrice.toFixed(4);
    tr.appendChild(tSoldPrice);
    // won
    let tWon = _mk('td');
    tWon.innerHTML = (res.soldPrice - res.invest).toFixed(2);
    tr.appendChild(tWon);
    // balanceOut
    let tOut = _mk('td');
    tOut.innerHTML = res.balanceOut.toFixed(4);
    tr.appendChild(tOut);
    // other
    tbl.appendChild(tr);
  }
  // Set final balance
  _get('final-balance').innerHTML = results[results.length-1].balanceOut.toFixed(4);
}
