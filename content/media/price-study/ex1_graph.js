class Graph {
  dates = [];
  data = null;
  canvas = null;
  c = null;
  order = null; // [ 'AAPL', 'AMZN', ... ]
  size = null; // how many symbols
  points = [];
  innerWidth = null; // canvas width without margins
  margin = 10;
  max = null; // max price
  paths = {}; // rectangles for prices, Path2d
  selected = []; // highlighted symbols
  hover = null; // symbol which is currently highlighted/hovered
  colors = [ 'red', 'blue', 'purple', 'cyan', 'magenta', 'yellow', 'brown', 'green' ];
  sectors = [];

  constructor(canvasId, data) {
    // Canvas
    const canvas = document.getElementById(canvasId);
    const c = canvas.getContext('2d');
    this.canvas = canvas;
    this.c = c;
    this.canvas.width = 800;
    this.canvas.height = 600;
    this.innerWidth = this.canvas.width - 2*this.margin;

    // Data
    this.data = data;
    // Get all available dates, sectors, and all prices
    this.size = Object.keys(data).length;
    let allPrices = [];
    let allSectors = [];
    for (let symbol in data) {
      if (!allSectors.includes(data[symbol]['sector'])) {
        allSectors.push(data[symbol]['sector']);
      }
      for (let point of data[symbol]['prices']) {
        let date = point[0];
        let price = point[1];
        allPrices.push(price);
        if (!this.dates.includes(date)) {
          this.dates.push(date);
        }
      }
    }
    this.sectors = allSectors.sort();
    this.max = Math.max(...allPrices);
    this.dates = this.dates.sort();
    this.currentId = this.dates.length-1;
    // DOM elements
    let dateRange = document.getElementById('date-range');
    dateRange.step = 1;
    dateRange.min = 1;
    dateRange.max = this.dates.length-1;
    dateRange.value = this.dates.length-1
    document.getElementById('date-label')
      .innerHTML = this.dates.slice(-1)[0];
    // Others
    this.btnSort();
    this.updateTable();
    dateRange.addEventListener('input', (e) => {
      this.currentId = e.target.value;
      document.getElementById('date-label').innerHTML = this.dates[e.target.value];
      this.drawDateById(e.target.value);
      this.updateTable();
    });
    document.getElementById('btn-sort')
      .addEventListener('click', this.btnSort.bind(this));
    document.getElementById('btn-reset')
      .addEventListener('click', this.btnReset.bind(this));
    // Mouse
    this.canvas.addEventListener('mousemove', this.onMouseOver.bind(this));
    this.canvas.addEventListener('mousedown', this.onMouseDown.bind(this));
  }

  updateTable() {
    let table = document.getElementById('table');
    table.innerHTML = '';
    let id = this.currentId;
    let prices = this.getPricesForDate(this.dates[id]);
    for (let symbol of this.order) {
      let data = prices.filter((e) => e[0] == symbol);
      let price = data[0][1];
      if (price === null) {
        price = '-';
      } else {
        price = price.toFixed(2);
      }
      let tr = document.createElement('tr');
      if (this.selected.includes(symbol)) {
        tr.className = 'select';
      } else {
        tr.className = 'empty';
      }
      tr.id = symbol;
      table.appendChild(tr);
      let tdSymbol = document.createElement('td');
      tdSymbol.innerHTML = symbol;
      tr.appendChild(tdSymbol);
      let tdPrice = document.createElement('td');
      tdPrice.innerHTML = price;
      tr.appendChild(tdPrice);
      let sector = this.data[symbol]['sector'];
      let tdSector = document.createElement('td');
      tdSector.innerHTML = sector;
      tdSector.style.setProperty('background', this.colors[this.sectors.indexOf(sector)]);
      tr.appendChild(tdSector);

      // mouseEvents
      tr.addEventListener('click', this.onTableClick.bind(this));
      tr.addEventListener('mouseover', this.onTableHover.bind(this));
    }
  }

  drawDateById(id) {
    this.drawDate(this.dates[id]);
  }

  btnSort() {
    this.setOrder();
    this.setPoints(this.currentId);
    this.drawDateById(this.currentId);
  };
  btnReset() {
    this.selected = [];
    for (let s in this.paths) {
      document.getElementById(s).className = 'empty';
    }
    this.drawDateById(this.currentId);
  }

  setPoints(id) {
    let prices = this.getPricesForDate(this.dates[id]);
    let sorted = [];
    for (let symbol of this.order) {
      let price = prices.filter((e) => {
        return e[0] == symbol;
      });
      sorted.push(price);
    }
    let points = [];
    // Get width
    for (let i in sorted) {
      let p = sorted[i][0][1];
      if (p === null) continue;
      points.push(p);
    }
    this.points = points;
  };


  drawDate(date) {
    const c = this.c;
    const m = this.margin;
    const h = this.canvas.height;
    c.clearRect(0, 0, this.canvas.width, h);
    c.fillStyle = 'lightgreen';
    // Sort prices by the established order
    let prices = this.getPricesForDate(date);
    let sorted = [];
    for (let symbol of this.order) {
      let price = prices.filter((e) => {
        return e[0] == symbol;
      });
      sorted.push(price);
    }
    // Draw prices
    let priceMax = Math.max(...sorted.map(x => x[0][1]).concat(this.points))
    let w = this.innerWidth / this.size;
    for (let i in sorted) {
      let path = new Path2D();
      let s = sorted[i][0][0]; // symbol
      let p = sorted[i][0][1]; // price
      let sector = this.data[s]['sector'];
      if (p === null) continue;
      let y = (p/priceMax)*(h-m);
      path.rect(this.canvas.width - m*2 -w/2 - i*w, h - y, w, y);
      if (this.selected.includes(s)) {
        c.fillStyle = 'silver';
        c.strokeStyle = 'yellow';
        //c.lineWidth = 4;
      }
      if (this.hover == s) {
        c.lineWidth = 4;
      }
      c.strokeStyle = this.colors[this.sectors.indexOf(sector)];
      c.fill(path);
      c.stroke(path);
      c.fillStyle = 'white';
      c.strokeStyle = 'black';
      c.lineWidth = 4;
      this.paths[s] = path;
    }

    // Draw points
    c.strokeStyle = 'red';
    c.lineWidth = 2;
    c.beginPath();
    for (let i in this.points) {
      let p = this.points[i];
      let y = (p/priceMax)*(h-m);
      c.lineTo(this.canvas.width - m - i*w - w/2, h - y);
    }
    c.stroke();
    c.lineWidth = 1;
    c.strokeStyle = 'black';

    // Draw axis
    c.lineWidth = 2;
    c.beginPath();
    c.moveTo(m-2, m);
    c.lineTo(m-2, h);
    c.stroke();
    c.beginPath();
    c.moveTo(m/2-2, m);
    c.lineTo(3*m/2-2, m);
    c.stroke();
    c.lineWidth = 1;
    let max = Math.max(...sorted.map(x=>x[0][1]));
    c.font = "2em serif";
    let text = c.measureText(max.toFixed(2));
    c.fillStyle = 'black';
    c.fillText(max.toFixed(2), 2*m, 3*m);
    
  };

  setOrder() {
    // Get the last date, and order stocks by their prices
    let lastDate = this.dates[this.currentId];
    let prices = this.getPricesForDate(lastDate);
    prices.sort((a, b) => {
      return b[1] - a[1];
    });
    let order = prices.map(e => e[0]);
    this.order = order;
  };

  getPricesForDate(date) {
    let ret = [];
    let d = this.data;
    let symbols = Object.keys(d);
    for (let symbol of symbols) {
      let price = d[symbol]['prices'].filter((e) => {
        return e[0] == date;
      });
      if (price.length != 0) {
        price = price[0][1];
        ret.push([symbol, price]);
      } else {
        ret.push([symbol, null]);
      }
    }
    return ret;
  }

  getLastDate() {
    return this.dates.slice(-1);
  }

  onMouseOver(e) {
    e.preventDefault();
    const c = this.c;
    const canvas = this.canvas;
    var rect = this.canvas.getBoundingClientRect();
    const mouseX = (e.clientX - rect.left)*(canvas.width/canvas.clientWidth);
    const mouseY = (e.clientY - rect.top)*(canvas.height/canvas.clientHeight);
    let paths = this.paths;
    for (let symbol in paths) {
      let path = paths[symbol];
      if (c.isPointInPath(path, mouseX, mouseY)) {
        // Highlight
        this.hoverSymbol(symbol);
        document.getElementById(symbol)
          .style.setProperty('border', '2px solid black');
      }
    }
  }

  onMouseDown(e) {
    e.preventDefault();
    const c = this.c;
    const canvas = this.canvas;
    var rect = this.canvas.getBoundingClientRect();
    const mouseX = (e.clientX - rect.left)*(canvas.width/canvas.clientWidth);
    const mouseY = (e.clientY - rect.top)*(canvas.height/canvas.clientHeight);
    let paths = this.paths;
    for (let symbol in paths) {
      let path = paths[symbol];
      if (c.isPointInPath(path, mouseX, mouseY)) {
        this.clickSymbol(symbol);
      }
    }
  }

  onTableClick(e) {
    this.clickSymbol(e.target.parentElement.id);
    //this.drawDateById(this.currentId);
  }

  clickSymbol(symbol) {
    let t = document.getElementById(symbol);
    if (this.selected.includes(symbol)) {
      // Already selected, unselect
      this.selected = this.selected.filter(e => e!= symbol);
      t.className = 'empty';
    } else {
      // Select
      this.selected.push(symbol);
      t.className = 'select';
    }
    this.drawDateById(this.currentId);
    //this.drawDateById(this.currentId);
  }

  onTableHover(e) {
    let symbol = e.target.parentElement.id;
    this.hoverSymbol(e.target.parentElement.id);
  }

  hoverSymbol(symbol) {
    this.hover = symbol;
    this.drawDateById(this.currentId);
    let paths = this.paths;
    for (let s in paths) {
      // Clear all
      document.getElementById(s)
        .style.setProperty('border', 'none');
    }
    document.getElementById(symbol).style.setProperty('border', '2px solid black');
  }
}; //class Graph
