window.addEventListener('load', Run);
const dataUrl = '/blog-fin/media/study-sunny-harris-bands/chart1_data.tsv';
const dataUrlFull = '/blog-fin/media/study-sunny-harris-bands/chart1_data_moving_average.tsv';
var data = null;
var dataFull = null;
var dataCount = 0;
var plot = null;
// Events
const eventDataReceived = new Event('dataReceived');

function Run() {
  var canvas = document.getElementById('canvas');
  document.addEventListener('dataReceived', onDataReceived);
  fetch(dataUrl)
    .then(response => response.text())
    .then(text => {
      data = tsvParse(text);
      document.dispatchEvent(eventDataReceived);
    });
  fetch(dataUrlFull)
    .then(response => response.text())
    .then(text => {
      dataFull = tsvParse(text);
      document.dispatchEvent(eventDataReceived);
    });
  let ma1Period = document.querySelector('#range-ma1');
  plot = new Plot('canvas');
  plot.setSettings({ hoverLabels: true });
  ma1Period.addEventListener('input', drawPlot);
  let ma2Period = document.querySelector('#range-ma2');
  ma2Period.addEventListener('input', drawPlot);
}

function onDataReceived() {
  dataCount += 1;
  if (dataCount == 2) {
    // All data received, draw chart and MA
    drawPlot();
  }
};

function drawPlot() {
  plot.clear();
  plot.setDataOhlc(data);
  plot.setVlines([ {"date": "2020-10-01", "label": "lbl1"} ])

  let period1 = document.querySelector('#range-ma1').value;
  let period2 = document.querySelector('#range-ma2').value;
  let ma1 = calculateMa(period1);
  let ma2 = calculateMa(period2);
  let dma1 = calculateDma();
  plot.setData([
    { "label": "MA1", "data": ma1, color: "purple" },
    { "label": "MA2", "data": ma2, color: "gold" },
    { "label": "DMA1", "data": dma1, color: "red" },
  ]);
  document.querySelector('#span-ma1').innerHTML = period1;
  document.querySelector('#span-ma2').innerHTML = period2;
  plot.draw();
}


function calculateMa(period) {
  maPoints = [];
  for (datapoint of data) {
    let index = dataFull.findIndex(x => x.date === datapoint.date);
    let maData = dataFull.slice(index - period + 1, index + 1);
    maData = maData.map(x => x.close);
    const averageFn = array => array.reduce((a, b) => a + b) / array.length;
    maPoints.push({
      'date': datapoint.date,
      'value': averageFn(maData)
    });
  }
  return maPoints;
}


function calculateDma() {
  let period = 10;
  maPoints = [];
  for (p of data) {
    let index = dataFull.findIndex(x => x.date === p.date);
    pPrev = dataFull.slice(index-1, index)[0];
    let perc = Math.abs( (100*(p.close-pPrev.close)/pPrev.close).toFixed(2) );
    let a = 0; // min in (0, 4) %
    let b = 2; // max in (0, 4) %
    let c = 5; // start in (20, 5) MA range periods
    let d = 1; // end in (20, 5) MA range periods
    let newPeriod = c + ( (d-c)/(b-a) )*(perc-a);
    newPeriod = parseInt(newPeriod.toFixed(0));

    //console.log(p.date, 'perc', perc, 'period', period, 'newPeriod', newPeriod,
    //  'period+newPeriod', period+newPeriod);
    let maData = dataFull.slice(index - (period+newPeriod) + 1, index + 1);
    maData = maData.map(x => x.close);
    const averageFn = array => array.reduce((a, b) => a + b) / array.length;
    maPoints.push({
      'date': p.date,
      'value': averageFn(maData)
    });
  }
  return maPoints;
}


/* HELPERS */
function convertDateFormat(inputDate) {
  // Parse the input date string
  const parsedDate = new Date(inputDate);
  if (isNaN(parsedDate.getTime())) {
    console.error('Invalid date format');
    return null;
  }
  // Format the date as 'YYYY-MM-DD'
  const formattedDate = `${parsedDate.getFullYear()}-${(parsedDate.getMonth() + 1).toString().padStart(2, '0')}-${parsedDate.getDate().toString().padStart(2, '0')}`;
  return formattedDate;
}


function parseNumber(str) {
  return parseFloat(str.replace(',', ''));
}


function tsvParse(data) {
  const rows = data.split('\n');
  const headers = rows[0].split('\t');
  const result = [];
  for (let i = 1; i < rows.length; i++) {
    const currentRow = rows[i].split('\t');
    if (currentRow.length === headers.length) {
      //const rowObject = {};
      const rowObject = {
        'date': convertDateFormat(currentRow[0]),
        'open': parseNumber(currentRow[1]),
        'high': parseNumber(currentRow[2]),
        'low': parseNumber(currentRow[3]),
        'close': parseNumber(currentRow[4]),
      };
      result.push(rowObject);
    }
  }
  return result;
}
