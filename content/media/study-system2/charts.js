window.addEventListener('load', Run);
const canvasId = '20240408-arm-daily';
let examples = {
  'arm': { 'url': '/blog-fin/media/study-system2/20240408-daily-ARM.json', },
  'avgo': { 'url': '/blog-fin/media/study-system2/20240411-daily-AVGO.json', },
  'aapl': { 'url': '/blog-fin/media/study-system2/20240411-daily-AAPL.json', },
  'gfs': { 'url': '/blog-fin/media/study-system2/20240411-daily-GFS.json', },
};


function Run() {
  for (let symbol in examples) {
    let url = examples[symbol].url;
    fetch(url)
      .then(res => res.json())
      .then(data => {
        let example = examples[symbol];
        example.data = data;
        example.dataFull = data;
        constructControls(symbol);
        let plot = new Plot('chart-' + symbol);
        plot.setSettings({hoverLabels:true});
        plot.canvas.width = 1024;
        plot.canvas.height = 768;
        examples[symbol].plot = plot;
        drawPlot(symbol);
        calculateMaCrosses(symbol, example.data, example.dataFull);
      });
  }
};


// Helpers
function _get(id) {
  return document.getElementById(id);
}
function _mk(el) {
  return document.createElement(el);
}


// Main functions
function constructControls(symbol) {
  let div = document.querySelector(`.charts[symbol="${symbol}"]`);
  let dataFullLen = examples[symbol].dataFull.length;
  let controls = `
    <!-- CROSSES -->
    Moving average crossovers
    <div class="crosses-tab">
      <div value="price" class="selected">price</div>
      <div value="chart">chart</div>
      <div value="data">data</div>
    </div>
    <div class="crosses-result">
      <canvas id="chart-${symbol}" class="crosses-price"></canvas><br>
      <canvas id="crosses-${symbol}" class="crosses-chart"></canvas>
      <div id="crosses-data-${symbol}" class="crosses-data"></div>
    </div>
    <!-- CONTROLS -->
    zoom:<br>
    <input class="slice" type="range" min="0" max="${dataFullLen-1}" value="0"></input><br>
    MA 1: (purple): <span class="lbl-ma1">10</span><br>
      <input class="ma1" type="range" min="1" max="70" value="16"></input><br>
    MA 2: (gold):   <span class="lbl-ma2">20</span><br>
      <input class="ma2" type="range" min="1" max="70" value="14"></input><br>
  `;
  div.innerHTML = controls;
  div.querySelectorAll('input').forEach((el) => {
    el.addEventListener('input', updatePlot);
  });
  div.querySelector('.crosses-tab')
    .querySelectorAll('div')
    .forEach((el) => {
      el.addEventListener('click', onCrossesTabClick);
    });
  let crossesData = div.querySelector('.crosses-data');
  let w = parseFloat(getComputedStyle(crossesData).width);
  let h = w*(3/4);
  let crossesChart = div.querySelector('.crosses-chart');
  crossesChart.style.display = 'none';
  crossesData.style.height = h + 'px';
  crossesData.style.display = 'none';
}

function onCrossesTabClick(e) {
  let symbol = e.target.parentElement.parentElement.getAttribute('symbol');
  let picked = e.target.getAttribute('value');
  let div = document.querySelector(`.charts[symbol="${symbol}"]`);
  let tabs = div.querySelector('.crosses-tab').children;
  Array.from(tabs).forEach((el) => {
    if (el.getAttribute('value') == picked) {
      el.className = "selected";
    } else {
      el.className = "";
    }
  });
  let results = div.querySelector('.crosses-result').children;
  Array.from(results).forEach((el) => {
    if (el.className == 'crosses-' + picked) {
      el.style.display = null;
    } else {
      el.style.display = "none";
    }
  });
}


function updatePlot(e) {
  //let symbol = e.target.getAttribute('symbol');
  let symbol = e.target.parentElement.getAttribute('symbol');
  drawPlot(symbol);
}


function drawPlot(symbol) {
  let div = document.querySelector(`.charts[symbol="${symbol}"]`);
  // Update control values: slice
  let slice = div.querySelector('.slice').value;
  // Update control values: MA ranges
  let ma1 = div.querySelector('.ma1').value;
  let ma2 = div.querySelector('.ma2').value;
  div.querySelector('.lbl-ma1').innerHTML = ma1;
  div.querySelector('.lbl-ma2').innerHTML = ma2;
  // Calculate MAs
  examples[symbol].data = examples[symbol].dataFull.slice(slice);
  let plot = examples[symbol].plot;
  let data = examples[symbol].data;
  let dataFull = examples[symbol].dataFull;
  // Update plot
  plot.clear();
  plot.setDataOhlc(data);
  plot.setData([
    {
      "label": "MA1",
      "data": calculateMa(data, dataFull, ma1),
      color: "purple"
    },
    {
      "label": "MA2",
      "data": calculateMa(data, dataFull, ma2),
      color: "gold"
    },
  ]);
  plot.draw();
}


function calculateMa(data, dataFull, period) {
  maPoints = [];
  for (datapoint of data) {
    let index = dataFull.findIndex(x => x.date === datapoint.date);
    let maData = dataFull.slice(index - period + 1, index + 1);
    maData = maData.map(x => x.close);
    if (maData.length < period) continue;
    const averageFn = array => array.reduce((a, b) => a + b) / array.length;
    maPoints.push({
      'date': datapoint.date,
      'value': averageFn(maData)
    });
  }
  return maPoints;
}


function calculateMaCrosses(symbol, data, dataFull) {
  let maxLen = dataFull.length-1;
  let pairs = [];
  for (let i = 1; i <= maxLen-1; i++) {
    for (let j = i; j <= maxLen-1; j++) {
      if (j == i) continue;
      let a = [i,j].sort((a,b) => a-b);
      pairs.push(a);
    }
  }
  let crosses = {
    'bull': [],
    'bear': [],
  };
  for (let periods of pairs) {
    let ma1 = calculateMa(data, dataFull, periods[0]);
    ma1.pop();
    ma1 = ma1.slice(-2);
    let ma2 = calculateMa(data, dataFull, periods[1]);
    ma2.pop();
    ma2 = ma2.slice(-2);
    let d = {
      'periods': periods,
      'day1': [ ma1[0].value, ma2[0].value ],
      'day2': [ ma1[1].value, ma2[1].value ],
    };
    if (
      d['day1'][0] < d['day1'][1]
      && d['day2'][0] > d['day2'][1]
    ) {
      // cross: bull
      crosses.bull.push(periods);
    }
    if (
      d['day1'][0] > d['day1'][1]
      && d['day2'][0] < d['day2'][1]
    ) {
      // cross: bear
      crosses.bear.push(periods);
    }
  }
  drawCrosses(symbol, crosses.bear, 'red');
  drawCrosses(symbol, crosses.bull, 'green');
  fillCrossesTable(symbol, crosses);
}


function fillCrossesTable(symbol, data) {
  let div = document.querySelector(`.charts[symbol="${symbol}"]`)
    .querySelector('.crosses-data');
  //div.innerHTML = JSON.stringify(data);
  let win = _mk('div');
  let txtWin = 'BULL<br>';
  for (let i of data.bull) {
    txtWin += i + '<br>';
  }
  win.innerHTML = txtWin;
  div.appendChild(win);
  let lose = _mk('div');
  let txtLose = 'BEAR<br>';
  for (let i of data.bear) {
    txtLose += i + '<br>';
  }
  lose.innerHTML = txtLose;
  div.appendChild(lose);
}


function drawCrosses(symbol, data, color) {
  let canvas = _get('crosses-'+symbol);
  canvas.style.border = '2px solid';
  let c = canvas.getContext('2d');
  if (canvas.width != 1024) {
    canvas.width = 1024;
    canvas.height = 768;
  }
  c.lineWidth = 2;
  c.strokeStyle = color;
  let max = examples[symbol].dataFull.length-20;
  for (let cross of data) {
    c.beginPath();
    c.moveTo(10, canvas.height-(cross[0]/max)*canvas.height);
    c.lineTo(canvas.width, canvas.height-(cross[1]/max)*canvas.height);
    c.stroke();
  }
}
