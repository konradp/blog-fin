window.addEventListener('load', Run);

function Run() {
  var chart = new Chart();

}

class Chart {
  data = []; // List of {x,y} points
  c; // canvas context
  w; // canvas width
  h; // canvas height
  rad = 1; // point radius
  step = 1;
  margin_w = 10;
  //margin_h = this.margin_w*2/3;
  margin_h = this.margin_w;
  inner_w; // canvas width - 2*margin
  inner_h; // canvas height - 2*margin
  max_x = 50;
  highlighted = 20;

  constructor() {
    // Needs id of the div
    const canvas = document.getElementById('chart');
    const c = canvas.getContext('2d');
    this.c = c;
    canvas.width = window.innerWidth*80/100;
    canvas.height = canvas.width*(2/3);
    this.w = canvas.width;
    this.h = canvas.height;
    this.inner_w = this.w - 2*this.margin_w;
    this.inner_h = this.h - 2*this.margin_h;
    let x = 0;
    while (x <= this.max_x) {
      let y = this.Evaluate(x).toFixed(2);
      this.data.push({x: x.toFixed(2), y: y});
      x += this.step;
    }
    this.Draw();
    this.initControls();
  }

  Draw() {
    const c = this.c;
    const rad = this.rad;
    //c.clearRect(10, 10, 10, 10);
    c.clearRect(0, 0, this.w, this.h);
    // Lines

    const data = this.data;
    for (let d of data) {
      let newD = this.Transform(d);
      let rad = this.rad;
      // Draw dots
      c.beginPath();
      c.arc(newD.x, newD.y, rad, 0, 2*Math.PI);
      c.fill();
      // Draw one big dot
      if (d.x == this.highlighted) {
        c.beginPath();
        c.arc(newD.x, newD.y, 4, 0, 2*Math.PI);
        c.fill();
      }
    }
    // Vert axis
    c.beginPath();
    let a = this.Transform({x:0, y:0});
    c.moveTo(a.x, a.y);
    a = this.Transform({x:0, y:100});
    c.lineTo(a.x, a.y);
    c.stroke();
    // Horiz axis
    a = this.Transform({x:0, y:0});
    c.moveTo(a.x, a.y);
    a = this.Transform({x:50, y:0});
    c.lineTo(a.x, a.y);
    c.stroke();
  };


  Evaluate(x) {
    // Evaluate function at x
    return x / (1 - x/100);
  };

  initControls() {
    const slide = document.getElementById('range');
    slide.addEventListener('input', (e) => {
      const x = parseFloat(e.target.value);
      this.highlighted = x;
      this.Draw();
      const xInput = document.getElementById('xInput');
      xInput.value = x.toString();
      const y = document.getElementById('y');
      y.innerHTML = this.Evaluate(x).toFixed(2) + '%';
    });
    const input = document.getElementById('xInput');
    input.addEventListener('input', (e) => {
      const x = parseFloat(e.target.value);
      this.highlighted = x;
      this.Draw();
      const slide = document.getElementById('range');
      slide.value = x.toString();
      const y = document.getElementById('y');
      y.innerHTML = this.Evaluate(x).toFixed(2) + '%';
    });
  };

  Transform(dataPoint) {
    // dataPoint: {x, y}
    return {
      x: this.margin_w + dataPoint.x*this.inner_w/50, // TODO
      y: -this.margin_h + this.h - dataPoint.y*this.inner_h/100,
    };
  }


} // class Chart
