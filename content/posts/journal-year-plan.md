---
title: "Year goals plan"
subtitle: 
date: 2024-02-07
categories: 08 journal
---
Table of contents:
{{<toc>}}

## 2025
Goals:
- {{<x>}} **1. Design and trade a day-trading strategy**
  - doesn't have to be good or working, but well-defined
  - as part of this, skill up in indicators and signals, e.g. bollinger, mean-reversion, RSI, MACD, VWAP, WIX etc
- {{<x>}} **2. Design and trade a swing-trading strategy**
  - as above
  - {{<v>}} blog: [blog: Swing trading plan](/posts/journal-swing-trading-plan/)
- {{<x>}} **3. Make trades**
  - make 50 well-documented trades this year, with clear reason for buy, clear strategy, clear stoploss, and clear execution. Document these in their own article. [here](/posts/journal-trades-2025/)
  - budget: $1000
- {{<x>}} **4. Understand SEC earnings better**
  - balance sheets: which numbers matter
  - do earnings move the price?
- {{<x>}} **5. Understand overall market better**
  - {{<w>}} blog: [NASDAQ price structure](/posts/study-nasdaq-price-structure/)
  - {{<x>}} have an understanding of where the market is at a given day. Are we in downtrend/uptrend etc, NDQ, NDQ100, S&P, indexes, gold
  - also, per-sector/industry. Be able to say e.g. "Biotech is trending up".
- {{<x>}} **6. Backtesting methodology**
  - design a system for backtesting strategies, using all-time daily price of all (or some) NASDAQ stocks
- {{<x>}} **7. Describe my trading style**
  - am I a day trader, swing trader, fundamental trader, technical trader, quant trader?

Notes:
- No nonsense goal like last year (so no 'Make the first million'-type goal).
- TODO: Link the produced articles under each goal item above
- Separated the FIN goals into own blog (this one), and into own Trello board
- In Trello, each task which contributes to a goal has a label (color and 1-7), showing which goal it contributes to


### 01 JAN
{{<highlight python>}}
DONE:
- Schedule fin plan tasks
- Set goals for 2024
- Boxplot and histogram for price and market cap distribution
- PLAN: Backtesting system
- Price and marketcap comparison: boxplot for different NASDAQ tiers
- blog: NASDAQ price structure and market cap
- finplotjs: draw volume
- fintools/nasdaq100: style the numbers in table: align to right and toFixed decimals, and fixed-width
- fintools/nasdaq100: include sectors
- Nasdaq structure: minor fixes
- NDQ 100: Per sector snapshot
{{</highlight>}}


Goals review:

- {{<x>}} 1.Design and trade a day-trading strategy: no progress, goal 5 is a prerequisite
- {{<x>}} 2.Design and trade a swing-trading strategy: no progress, goal 5 is a prerequisite
- {{<x>}} 4.Understand SEC earnings better: no progress
- {{<x>}} 7.Describe my trading style: no progress
- {{<w>}} **3.Make trades**
  - started work on backtesting system
  - made a few $10 trades, not backed by any strategy or a system, just based on guesses from BBC news
  - added volume to finplotjs
- {{<w>}} **5.Understand overall market better**
  - most progress here
  - studied NDQ price structure, added charts/reports for sectors, but no deep study
- {{<w>}} **6.Backtesting methodology**
  - created basic implementation, some boilerplate, the blog article



## 2024
- {{<x>}} study: question of possibility:
  - 4% gain per year: is it possible?
    - pick stocks from etoro, (below certain price). What happens to them after a day? How many increase by 4%?
  - delisted stocks: Stocks as above, how many of these stocks disappear, i.e. get delisted from NASDAQ?
  - initially, study the above e.g. over a week, a small amount of data
- {{<x>}} study: question of possibility: further data:
  - Can I get a list of stocks from the past that satisfy these conditions (etoro+price below $1)? Or do they disappear from historical data?
  - Can I get a list of etoro featured stocks at any point in time in the past?
- {{<x>}} study: if the above is possible (and express these in percentage probability), i.e. there is a 4% gainer each day, study these stocks further. Are there any signals or properties which increase the probability?
- {{<x>}} study: Consider querying etoro internal API:
  - ref: https://www.reddit.com/r/Etoro/comments/leis0m/etoro_api/
  - ref: https://api.etorostatic.com/sapi/instrumentsmetadata/V1.1/instruments/bulk?bulkNumber=1&cv=cbaf02c1af762cde6d7e2b494fb38c87_77e278d7e93fad5d268c4daec194b1aa&totalBulks=1
  - ref: https://api.etorostatic.com/sapi/trade-real/instruments/bulk-slim?InstrumentDataFilters=TradingData&bulkNumber=1&cv=1468216343400a256ee00de7bcbe2d71_34357884669337d186c5252acb1b0401&totalBulks=1
  - but what are the urls? in network tab I see: https://www.etoro.com/sapi/trade-real/instruments?InstrumentDataFilters=Activity,Rates,ActivityInExchange
  - but the instruments have IDs, with no names. So, how to relate instrument IDs to names? Can we get just the names?
