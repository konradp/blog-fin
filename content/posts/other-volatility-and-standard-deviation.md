---
title: "Volatility and standard deviation"
date: 2019-12-23
categories: 03 other
---

## DRAFT notes:

There are two articles here:

- buy the dip strategy
- above not working, so look at standard deviation, and signals based on volatility

### Buy the dip: Background
My most recent idea of a strategy was to pick from the below scanner

- exchange: NASDAQ
- signal: top losers
- price: below $2

I would focus mostly on the stocks which drew down the most, and from looking at their charts, I would (arbitrarily) select and buy those which 'look about to go up', for for example those which appear to be in an overall bullish trend. This is known as 'buying the dip', as featured in the catchy phrase of "buy the dip, sell the rip".

#### DRAFT

- didn't work
- used orders: buy limit, stop loss limit (risk 10%), sell limit (10% gain...)
- discovered that broker Degiro does not allow both a sell stop loss + sell/limit order at the same time. Why? I want to sell when it reaches my target, but I also want to sell when it goes down too much...
- how I chose the entry price? Looked at bid/ask spread. How?
- stock would just trade sideways or go further down, never hitting my sell price
- realised that 10% increase is too eager/greedy
- noted that the top loser list changes over the course of the trading day. For consistency, run the screener to pick stocks after the end of market day, to trade  these the next day
- noticed compounding, and that 3-4% compound is still a huge gain over time (e.g. $100 principal, at 4% daily trade, with roughly 256 work days in a year, compounds to )
- I've read somewhere that 'buy the dip' is more likely to work in a bull market, and can easily result in losses if risk is not properly managed (e.g. the draw down is not a one-off exaggerated 'dip', but a realistic reduction in price which is followed by further draw-downs). How to spot this? indicators/technical data? 52w min/max, news catalyst, consider fundamentals (for longer play)?

### Standard deviation
- Standard deviation formula
- permutations of `[ 1, 2, 3 ]` all have the same standard deviation as there is no time dimension
- but `[ 1, 2, 3 ]` uptrend is clearly 'better' than `[ 3, 2, 1 ]` downtrend
- look at `[ 1, 2, 3, 4 ]` ?
- not really 'better' in case of 1,2,3, but consider `1234`, `4321`, `1324`. Some will be 'better' if we trade with limit orders, if we have more chances for the orders to hit our limits, i.e. if price fluctuates often between two values
- does any stock do that?
- is there an indicator to show when that happens?



