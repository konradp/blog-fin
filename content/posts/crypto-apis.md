---
title: "Crypto APIs"
subtitle: Comparison of the APIs
date: 2022-06-06
categories: 09 wip
---
Winner:

- ?

To consider:

- Binance
- BitPay
- Bitquery
- CoinAPI
- Coincap.io
- CoinGecko
- CoinMarketCap
- CryptoAPIs
- CryptoCompare
- Kraken
- Nomics
- NOWNodes
- Shrimpy
- Wyre

# Comparison
{{<table sortable>}}
name | free | trade | history
--- | --- | --- | ---
Binance | yes | yes | yes
BitPay | yes? | no? | no?
Bitquery |
CoinAPI |
Coincap.io |
CoinGecko |
CoinMarketCap |
CryptoAPIs |
CryptoCompare |
Kraken |
Nomics |
NOWNodes |
Shrimpy |
Wyre |
{{</table>}}

# Binance
Is it free?

- yes: ?

Can you trade?

- yes: https://binance-docs.github.io/apidocs/spot/en/#new-order-trade

Historical data?

- yes: https://binance-docs.github.io/apidocs/spot/en/#kline-candlestick-data

Limits:

- they seem to be quite permissive: https://www.binance.com/en/support/faq/360004492232

# Reference
- https://medium.com/coinmonks/best-crypto-apis-for-developers-5efe3a597a9f
