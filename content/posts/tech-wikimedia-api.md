---
title: "Wikimedia API"
date: 2018-05-09
categories: 02 tech
---

**(In progress)** Write a bot which uses the [MediaWiki action API][api] to track changes to a HTML table on a Wikipedia article, and serve it as JSON REST API.

<!--more-->

## Overview
I was interested in watching for changes to a table which lists the constituents of the FTSE 100 index, available [here][ftse wiki page]. The table on the wiki page looks similar to the below.

Company              | Ticker | FTSE sector        |
| ------------------ | ------ | ------------------ |
| 3i                 | III    | Financial Services |
| Admiral Group      | ADM    | Nonlife Insurance  |
| Anglo American plc | AAL    | Mining             |
| ...                | ...    | ...                |

I wanted to make make this table available for my applications via a RESTful web service serving the table in JSON format. I chose `golang` for this. The [Writing Web Applications][golang web] document seemed like just what I needed. In the very same web search, I also found a [common mistakes][golang mistakes] article, which I would use to tidy up at the end.

### Table parsing
Downloading and printing a web page is very easy in `golang`, so here we will focus on parsing the table (although we do need to watch out for the lack of a deafult timeout for such requests as described [here][default timeout]).

```
page := http.Get("http://example.com")
fmt.Println(page)
```

    


The above table can be represented by HTML code.

```
<table class="wikitable sortable" id="constituents">
  <tr>
    <th>Company</th>
    <th>Ticker</th>
    <th>FTSE sector</th>
  </tr>
  <tr>
    <td><a href="/wiki/3i" title="3i">3i</a></td>
    <td>III</td>
    <td>Financial Services</td>
  </tr>
  <tr>
    <td><a href="/wiki/Admiral_Group">Admiral Group</a></td>
    <td>ADM</td>
    <td>Nonlife Insurance</td>
  </tr>
  <tr>
    <td><a href="/wiki/Anglo_American_plc">Anglo American plc</a></td>
    <td>AAL</td>
    <td>Mining</td>
  </tr>
...
```

An alternative [wikitext][wikitext] representation is below (wikitext can be thought of as Wikipedia's version of Markdown).


```
{| class="wikitable sortable" id="constituents"
|-
!|Company|| Ticker || FTSE sector
|-
|[[3i]]||III||Financial Services
|-
|[[Admiral Group]]|| ADM || Nonlife Insurance
|-
|[[Anglo American plc]]|| AAL || Mining
|-
|...
|}
```

**Example:** https://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&rvsection=4&format=json&titles=FTSE_100_Index

### REST service

<!--Reference-->
[api]: https://www.mediawiki.org/wiki/API:Main_page
[default timeout]: https://medium.com/@nate510/don-t-use-go-s-default-http-client-4804cb19f779
[ftse app]: https://gitlab.com/konradp/ftse100const
[ftse wiki page]: https://en.wikipedia.org/wiki/FTSE_100_Index
[golang web]: https://golang.org/doc/articles/wiki/
[golang mistakes]: http://devs.cloudimmunity.com/gotchas-and-common-mistakes-in-go-golang/index.html
[wikitext]: https://www.mediawiki.org/wiki/Wikitext
