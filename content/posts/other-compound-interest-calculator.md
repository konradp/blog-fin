---
title: 'Compound interest calculator'
subtitle: 
date: 2021-10-18
categories: 03 other
---
<script src='/media/fin-compound-interest-calculator/core.js'></script>
<head>
<style>
input[type='number']{
    width: 2px;
} 
</style>
</head>

<body onload='main();'></body>

<form>
  <label for='number'>Initial price:</label>
  <input id="price" type='number' value="100"></input><br>
  <label for='number'>Percent (%):</label>
  <input id="perc" type='number' value="4"></input><br>
  <label for='number'>Count:</label>
  <input id="count" type='number' value="10"></input><br>
</form>

<table>
  <thead>
    <th>day</th>
    <th>price</th>
    <th>gain</th>
  </thead>
  <tbody id="table"></tbody>
