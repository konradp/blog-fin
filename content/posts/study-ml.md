---
title: "System 4: Trading stocks with machine learning"
subtitle: 
date: 2021-07-18
categories: 00 studies
---
{{<toc>}}

## General idea
Get a list of penny stocks at any given date, and see if we can predict which ones will go up by 4% in the next week. This is a binary classification problem.


## Plan
- get data
- label data
- train, validate, and test model
- evaluate model: design a strategy for how to run it for a bit in realtime (or even actually run it on small amounts)

Other TODO:
- penny stock definition: less than $1 or less than $2?
- pick a better name for the scripts, e.g. `./fetch_test_history.py` is not ideal
- how to design the neural net? data flattening? CNN, RNN or what?
- how to normalise data
- add seciton on how to use this: (add this note to the blog): Here we don’t know what the opening price will be, so we put in a market buy order before the market open. The fill price then determines the sell price (+4%).
- Idea for followup: Predict the fill price. Does the actual fill price change the strategy? e.g. the stop, or whether to sell immediately if it tells us that ‘the trade didn’t work' or ‘is going against us’


## Get and clean data
### Get data: stock histories
Ideally, we want a list of stocks which classified as penny stocks at some point in the past, and the dates at which they classified as such.

Run `./0fetch_year_histories.py`. This does:
- First, we get NASDAQ stocks from eToro (1991 of them) with `fetch_etoro_symbols.py`. It contains stock names.
- Then, `fetch_all_conids.py` gives 4470 symbols for NASDAQ from IB. It doesn't contain stock names but contains tickers.
- Then, pick stocks from eToro which are also on IB. Out of the 1991 stocks from eToro, matching against IB returns 1729 stocks. Note: we can't match on name because on eToro it's called 'Apple', and on IB it's 'APPLE INC'.

This then creates a dir called `0year_histories` which contains 5yr data for each of these 1729 symbols. There are 1720 stocks, and takes ~51mins to run and it generates 221.8MB data which looks like this:
```
$ ls 0histories | head
./
../
AADI.json
AAL.json
AAOI.json
AAON.json
AAPL.json
ABAT.json
ABCB.json
ABCL.json
```

where, for example `AADI.json` looks like:
```
{
  "symbol": "AADI",
  "conid": 511263210,
  "data": [
    {
      "t": 1561469400000,
      "o": 0.9298,
      "c": 0.88,
      "h": 0.9298,
      "l": 0.8617,
      "v": 118850.0
    },
    {
   ...
}
```


### Get data: penny stocks
Then, we run `./1fetch_penny_stocks_per_date.py` to find stocks which were <=$1 at any point in the past, organised by date. The file is called `low_penny_stocks.json`, and it looks like this:
```
{
  "1561383000000": [
    {
      "symbol": "CODX",
      "conid": 280904395,
      "l": 0.7113
    },
    {
      "symbol": "MVIS",
      "conid": 102558681,
      ...
    }, ...
  ],
  "1561469400000": [
    ...
  ],  
  ...
}
```

We plot the count of these penny stocks per date:
<img src='/media/study-machine-learning/penny_stock_count.png'></img>

There are 1083 dates on which some stocks were <=$1. On some days there was a single such stock, and on other days (in particular more recently), this count can be as high as 68. This number is probably saying something about the market, but I don't quite know what, so it will be included as a parameter to the training model. We can treat it as continuous (not discrete) because it's a count, and we can normalise it to between 0 and 1.


### Get data: 5min day chart data for day1 and day2
Now run `./2fetch_data_raw.py`. This will read the `low_penny_stocks.json`, it will download the 5min day chart data for day1 (date when the stock was <=$1), and for the following day (day2). It creates a dir called `1data_raw` (364.3 MB) where the data is organised as below:
```
training_raw/
  1561383000000/
    day1/
      CODX.json
      MVIS.json
    day2/
      CODX.json
      MVIS.json
  1561469400000/
    day1/
      AAPL.json
      MSFT.json
    day2/
    ...
  ...
```

Note: The day1 is the date found in the previous step. The day2 is the following day. The data is a 5min OHLCV for each day. There is one problem with incrementing the date. Suppose day1 is a Friday. We need a way to increment it correctly so that day2 is the following Monday. And what if day2 is a bank holiday? Luckily, we can just increment the date by one day, and the API will fetch the correct date.

Also, I think the day of the week (1-5) should be included in the training data.


### Check data
Run `3check_raw_data.py` to check the data quality. It checks the following.
- If there is data for day1 for symbol, there should be day2 data for symbol, and vice versa
- ~~x for each, make sure that all the data in .json files corresponds to the correct date~~
- ~~x Ideally, there should be 78 (5*13) datapoints for each day. What if it's missing, do we pad it with 'None' or '0', or agevage between two points? We keep all data~~

At this stage we do a minor manual cleanup by removing the few dates for which day1 or day2 is missing.


### Clean and label data
Run `4raw_data_to_pretraining_data.py`. This writes to a 117.7 MB dir called `2pretraining_data`, and gives 15052 JSON files, where 11,195 are labelled 0, and 3857 are labelled 1.

The script does a few things:
- **label the data**: day1 data 0 if there is no 4% gain on day2, mark 1 if there is 4% gain
- include the day of week in the training data: number between 1 and 5 to mean mon, tue, wed, ...
- include count of how many penny stocks there were on that date
- ~~clean data: if either day1 or day2 has less than 50 datapoints, then skip that date/symbol. No, we don't do this in the end~~
- It's important to use the ET timezone because that's what NASDAQ follows (9:30 to 15:55 ET)
- Something happened between 2019.11.01 and 2019.11.04 (1572877800000) where opening time changed from 13:30 to 14:30. So we exclude all dates less than 1572877800000

Resulting pre-training data is still human-friendly and non-normalised. Dir structure is:
```
data/
  20240101_CODX.json
  20240101_MVIS.json
  20240102_CODX.json
  20240102_MVIS.json
  ...
```

where each json file looks like:
```
{
  symbol: ABC,
  label: 0/1,
  day_of_week: 1-5,
  penny_stock_count: 2,
  data: [
    {
      t: 1716557400000,
      p: 0.5,
      v: 0.7133,
    },
    {
      t: 1716557400001,
      p: 0.5,
      v: 0.7133,
    },
    ...
  ]
}
```

where:
- data is day1 data
- p is o+h+l+c/4 <- we take the average, so that we have single price values
- label is calculated from day2 data, according to the labelling algorithm


**Data labelling algorithm**  
Assume we buy at the opening price on day2 (pick the high price here to make it harder). Sell anytime after for 4% gain at least ← is it possible? If yes, this is a good stock (label=1). If no, this is a bad stock (label=0). We do this by first taking the opening price (high price at the first datapoint), then we find the first price which is 4% higher than the opening price. If we can find such price, we label as 1. Otherwise, we label as 0.


### Prepare training data
Run `./5pretraining_data_to_training_data.py`. This creates a dir called `3training_data` (118.4 MB) with 15052 examples. This does some final cleaning and normalising of the data:

- normalise way_of week: instead of a single input node with value 1-5, we will have 5 input nodes, each with value 0 or 1, e.g.:
  - 1: [1, 0, 0, 0, 0] <- monday
  - 2: [0, 1, 0, 0, 0] <- tuesday
  - ...
- normalise prices: the highest price for the day is taken as 1, the min price is 0
- if data points are missing (ideally 78 data points per day), these are filled by a placeholder -1, this way there will always be 78 points: what time of the day is the zero point? Time ranges from 09:30 ET to 15:55 ET, so 78 points
- normalise penny_stock_count: between 0 and 1, where 1 is the highest count in the dataset, and 0 is zero count

In total, this gives input nodes:
- price: 78
- volume: 78
- day_of_week: 5
- penny_stock_count: 1

so, 162 input nodes in total.

A resulting example file `20240621_IDEX.json` looks like this:
```
{
  "symbol": "IDEX",
  "label": 1,
  "day_of_week": [ 0, 0, 0, 0, 1 ],
  "penny_stock_count": 0.9545454545454546,
  "data": [
    {
      "t": "09:30",
      "p": 1.0,
      "v": 0.2909726760513627
    },
    {
      "t": "09:35",
      "p": 1.0,
      "v": 0.01413003161594574
    },
    ...
    ...
  ]
}
```
Note: max_penny_stock_count is 66


## Training the model
We are now ready to train the model, run `./6train_model.py`.


### Install tensorflow
virtual env
```
virtualenv --system-site-packages -p python3 ./tensorflow_env
source ./tensorflow_env/bin/activate
pip install tensorflow
```

With Conv1D layers instead of LSTM, this gives a nice accuracy at model.evaluate:
```
Loss: 1.0951505899429321, Accuracy: 0.9283151626586914
Loss: 1.072116494178772, Accuracy: 0.93137127161026
Loss: 1.2247631549835205, Accuracy: 0.931769847869873
```

Saving the model with:
```
model.save('model.keras')
```

produces a file `model.keras` which is 1.9 MB.


## Using the model
Plan:
- get the list of penny stocks for given day (day1)
- get the 5min data for each penny stock (day1)
- normalise the data
- run the model on each penny stock data to predict if it will go up by 4% the next day

```
# Get data
./10get_predict_data.py /opt/fintools-ib/data 20240627
# Use model to predict the labels
cd data
source ./tensorflow_env/bin/activate
cd ../fintools_ib/bin/machine_learning
./11predict.py ~/konradp/fintools-ib/data/predict_data/20240627
```

For 20240627 I get <a href='/media/study-machine-learning/20240627_predictions.json'>predictions</a>. with +50% probabilities for:

```
ALLK: 0.98
BOXL: 1.0 <--
ECDA: 1.0 <--
FCEL: 0.53
FRSX: 1.0 <--
HOVR: 0.69
IMUX: 1.0 <--
ORGN: 1.0 <--
QLGN: 1.0 <--
```
From these, QLGN is not tradeable (see https://finance.yahoo.com/news/qualigen-therapeutics-inc-received-nasdaq-200000056.html?guccounter=1). We issue a market order (where we can, alternatively limit order).
so going to pick the 1.0 stocks, 6 of them: BOXL, ECDA, FRSX, IMUX, ORGN.
Once the order is filled, we calculate the sell price (4% increase), and issue a limit order for sell.

### Making the trade
not filled:
- BOXL

Fill prices, sell prices:
- ECDA: 1.28, 1.3312, can only set 1.33 for 3.9% or 1.34 for 4.7% profit. have set to the latter
- FRSX: not filled
- IMUX: 1.09, 1.336, can only set 1.14 for 4.6$ profit
- ORGN: 1.06, 1.1024, can only set 1.11

Invested $10 in each symbol.

<img src='/media/study-machine-learning/trade1.png'></img>


Here are the plots on 20240627 and 20240628:

<center>
<img src='/media/study-machine-learning/20240627_ECDA.png' style='width:49%'></img>
<img src='/media/study-machine-learning/20240628_ECDA.png' style='width:49%'></img>
</center>

ECDA was trading at mostly zero volume throughout day1. Why on day2 was it able to go up by 3.9%?

<center>
<img src='/media/study-machine-learning/20240627_FRSX.png' style='width:49%'></img>
<img src='/media/study-machine-learning/20240628_FRSX.png' style='width:49%'></img>
</center>
FRSX was barely trading on both days.

<center>
<img src='/media/study-machine-learning/20240627_IMUX.png' style='width:49%'></img>
<img src='/media/study-machine-learning/20240628_IMUX.png' style='width:49%'></img>
</center>

Nothing to mention on IMUX, looks normal.

<center>
<img src='/media/study-machine-learning/20240627_ORGN.png' style='width:49%'></img>
<img src='/media/study-machine-learning/20240628_ORGN.png' style='width:49%'></img>
</center>

For ORGN, on day1, very small price movements. Huge loss on day2.


## Learnings
Things to adjust or study further.

Trading:
- consider: do not use market order, only limit order
  - how to estimate the limit order buy price? Previous close
- do not trade on the phone, use the web ui
- what about bid/ask spread?

Model:
- It doesn't work well at all. Filter out stocks which don't trade much before making predictions
- Look also at previous week chart, not just previous day

Other:
- [gpt questions](./gpt-questions)
