---
title: "Fetching NASDAQ stock symbols (python)"
date: 2022-11-07
categories: 03 other
---

How to fetch NASDAQ stocks.

{{<toc>}}

## Program
We can fetch NASDAQ stock symbols via the url:

- ftp://ftp.nasdaqtrader.com/symboldirectory/nasdaqlisted.txt


{{<highlight python>}}
import json
import sys
import urllib.request

url = 'ftp://ftp.nasdaqtrader.com/symboldirectory/nasdaqlisted.txt'

def is_symbol_valid(symbol_data):
  # Exclude various/invalid companies, e.g. those whose names end with
  # '- Warrants'
  # Return true if symbol is valid, false if not
  if symbol_data['name'].endswith((
    '- Rights',
    '- Subunit',
    '- Subunits',
    '- Trust Preferred Securities',
    '- Unit',
    '- Units',
    '- Warrant',
    '- Warrants',
    )) \
    or '.' in symbol_data['symbol']:
    return False
  return True

def guess_type(stock_data):
  name = stock_data['name']
  stock_data['type'] = 'UNKNOWN'
  if name.endswith(' ETF'):
    # ETF
    stock_data['type'] = 'ETF'
  elif name.lower().endswith(' - common shares') \
    or name.lower().endswith(' - common stock') \
    or name.lower().endswith(' - ordinary share') \
    or name.lower().endswith(' - ordinary shares')  \
    or name.lower().endswith(' - class a common stock') \
    or name.lower().endswith(' - class a common shares') \
    or name.lower().endswith(' - class a common share') \
    or name.lower().endswith(' - class a ordinary shares') \
    or name.lower().endswith(' - class a ordinary share') \
    or name.lower().endswith(' - class a ordinary shre') \
    or name.lower().endswith(' - class a shares') \
    or name.lower().endswith(' -  ordinary shares, class a common stock'):
    # common stock (includes class A)
    stock_data['type'] = 'common stock'
    stock_data['name'] = name.split(' - ')[0]
  elif name.endswith(' - American Depositary Shares') \
    or name.endswith(' - American Depository Shares'):
    # American depositary shares
    stock_data['type'] = 'American depositary shares'
    stock_data['name'] = name.split(' - ')[0]
  elif name.endswith(' Fund'):
    stock_data['type'] = 'fund'
  return stock_data

def main():
  with urllib.request.urlopen(url) as response:
    symbols = []
    for line in response.read().decode('utf-8').splitlines()[1:-1]:
      fields = line.split('|')
      symbol = {
        'symbol': fields[0],
        'name': fields[1],
      }
      if not is_symbol_valid(symbol):
        continue
      if fields[3] != 'N':
        # Skip test stocks
        continue
      symbol = guess_type(symbol)
      symbols.append(symbol)
    print(json.dumps(symbols))

if __name__ == '__main__':
  sys.exit(main())
{{</highlight>}}

running it gives

```
[
  {
    "symbol": "AACG",
    "name": "ATA Creativity Global - ...",
    "type": "UNKNOWN"
  },
  {
    "symbol": "AACI",
    "name": "Armada Acquisition Corp. I",
    "type": "common stock"
  },
  {
    "symbol": "AADI",
    "name": "Aadi Bioscience, Inc.",
    "type": "common stock"
  },
  {
    "symbol": "AADR",
    "name": "AdvisorShares Dorsey Wright ADR ETF",
    "type": "ETF"
[...]
```
grouping by type
```
$ python3 fetch_nasdaq_symbols.py | jq -r '.[].type' | sort | uniq -c
    168 American depositary shares
   3481 common stock
    441 ETF
     59 fund
    417 UNKNOWN
```

## Instrument types: What are rights, units, warrants?
- common stock (ordinary share): Also, what is class A?
- american depositary shares
- rights?
- units?
- subunits?
- trust preferred securities?
- warrants?


## Reference
- ftp://ftp.nasdaqtrader.com/symboldirectory/nasdaqlisted.txt
- https://medined.github.io/investing/fetching-stock-symbols/

