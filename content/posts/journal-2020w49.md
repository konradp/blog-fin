---
title: "2020 w49: Initial stats"
subtitle: 
date: 2020-11-30
categories: 08 journal
---

{{<toc>}}

## Objectives
The objective this week is to watch the market each day (penny stocks < $3) in order to record some of the basic stats.

For each day of the week, find:

- {{<v>}} **Price distribution**  
  How many symbols less than $3 per share are there each day? How many for $2/share, $1 per share?
- {{<v>}} **Industry/sector distribution**  
  What industries are these?
- {{<x>}} **Winners (4% gain on day)**  
  Which symbols presented 4%-on-the-day opportunities (or more?).  
  Summarise by count, industry, etc., e.g. 'all 4% gainers were in this/that sector'
- {{<x>}} **Random trade**  
  Just buy any symbol for $1, record it here, with some thoughts, e.g. did it get filled, what was the fee, etc.
- {{<v>}} **Phone alarm**  
  Create a phone alarm/alert for when NASDAQ opens/closes

## Conclusions
I didn't get to do everything I wished, so going to focus next week on the **Random trade** (one trade per day) and also on finishing some of the data gathering tools.  
I still got some market insights, e.g. going to focus on **lt/3**, and on the **software industry (33 symbols**).


## Results

### Price distribution

#### Count

We see there are 285 symbols at price less-than or equal-to $2/share. Watching 285 symbols might be possible.

price | D2 | D3
--- | --- | ---
lt/10 | 1178 | ?
lt/5  | 717  | ?
lt/3  | 453  | ?
lt/2  | 285  | ?
lt/1  | 102  | ?

where:

- D2: 20201201 Tuesday

#### Distribution

Distribution of prices. We see that most symbols have price < $100 per share, with a large amount below $20/share.

![](/media/trade-journal-2020W49/count_d2_200.png)
![](/media/trade-journal-2020W49/count_d2_100.png)
![](/media/trade-journal-2020W49/count_d2_20.png)
![](/media/trade-journal-2020W49/count_d2_10.png)
![](/media/trade-journal-2020W49/count_d2_5.png)
![](/media/trade-journal-2020W49/count_d2_2.png)

### Industry/sector distribution
By industry, for lt/3

![](/media/trade-journal-2020W49/d4_industries.png)

### Winners (4% gain on day)
??

### Random trade
??

### Phone alarm
??



## Data and notes
Dump here the data as the week progresses.

### Price distribution
Raw counts
```
$ curl -s "http://192.168.0.100/lt/2" | jq '.[]' | wc -l
285
```

Charts
```
cd /opt/fintools-ib/data/quotes
jq -r '.c' *.json >> /tmp/prices
```
Use snippets/python/plot/plot_histogram.py with different sizes

### Industry/sector distribution
For lt/3.  
Get with:
```
$ ./bin/get_ltcategories.py
# copy json, paste into 'json' file, then extract data, sort, categorise
$ jq -r '.[].industry' json | sort | uniq -c | sort -h
      1 Auto Manufacturers
      1 Banks
      1 Engineering&Construction
      1 Hand/Machine Tools
      1 Home Builders
      1 Leisure Time
      1 Lodging
      1 null
      1 Office Furnishings
      1 Savings&Loans
      1 Textiles
      1 Toys/Games/Hobbies
      2 Aerospace/Defense
      2 Auto Parts&Equipment
      2 Electrical Compo&Equip
      3 Agriculture
      3 Apparel
      3 Building Materials
      3 Coal
      3 Energy-Alternate Sources
      3 Machinery-Diversified
      3 Metal Fabricate/Hardware
      3 Pipelines
      3 Semiconductors
      4 Distribution/Wholesale
      4 Environmental Control
      4 Insurance
      4 Media
      4 Mining
      4 REITS
      5 Beverages
      5 Chemicals
      5 Food
      5 Home Furnishings
      5 Investment Companies
      6 Advertising
      6 Computers
      6 Entertainment
      6 Miscellaneous Manufactur
      6 Oil&Gas Services
      8 Healthcare-Services
      9 Electronics
      9 Oil&Gas
      9 Retail
     10 Transportation
     12 Commercial Services
     12 Diversified Finan Serv
     12 Telecommunications
     13 Holding Companies-Divers
     28 Healthcare-Products
     28 Internet
     33 Software
     58 Pharmaceuticals
     92 Biotechnology
```
To get the pie, take the same json file, and use snippets/python/plot/piechart.py
