---
title: "2020 w53: discretionary trade each day"
subtitle: 
date: 2020-12-30
categories: 08 journal
---

<style>
.red {
  border: 2px solid red;
}
.discard {
  border: 5px solid blue;
}
</style>

# 20201230 Wednesday

## Report
Focus on volume.

{{<table sortable>}}
SYMB   |     P |    V1  |   V2  |      V3 |     V4 |     V5 |    vm |      VM
------ |  ----: |  ----:  | ----:  | -------: | ------: | ------: | -----: | -------:
IDEX   |  2.09 |   390  |  390  | 1007.77 | 704.50 | 704.50 | 33.00 | 7195.00
GNUS   |  1.38 |   390  |  390  |  560.81 | 221.00 | 221.00 |  6.00 | 6129.00
XELA   |  0.40 |   390  |  370  |  144.45 |  50.00 |  56.00 |  1.00 | 2403.00
VISL   |  1.67 |   390  |  378  |  107.09 |  44.50 |  46.50 |  1.00 | 1501.00
QTT    |  1.90 |   390  |  315  |   80.87 |  19.00 |  32.00 |  1.00 | 1188.00
BOXL   |  1.52 |   390  |  360  |   78.73 |  29.50 |  36.50 |  1.00 | 1288.00
PHUN   |  1.08 |   390  |  357  |   75.82 |  29.00 |  35.00 |  1.00 | 1313.00
INPX   |  1.04 |   390  |  358  |   56.67 |  12.00 |  16.00 |  1.00 |  914.00
MYSZ   |  1.50 |   390  |  201  |   44.35 |   1.00 |  29.00 |  1.00 | 1575.00
**VERB** |  1.85 |   390  |  280  |   42.82 |  10.00 |  19.50 |  1.00 | 1986.00
BRQS   |  1.04 |   390  |  251  |   39.68 |   5.00 |  21.00 |  1.00 | 1544.00
**LKCO**   |  0.71 |   390  |  284  |   36.51 |   8.00 |  17.50 |  1.00 |  905.00
**SEAC**   |  0.79 |   390  |  309  |   28.77 |   3.00 |   7.00 |  1.00 |  507.00
CNET   |  1.25 |   390  |  227  |   28.02 |   3.00 |  14.00 |  1.00 |  595.00
BHAT   |  0.88 |   390  |  165  |   14.97 |   0.00 |  12.00 |  1.00 |  341.00
PIXY   |  2.60 |   390  |  316  |   12.27 |   2.00 |   3.00 |  1.00 |  354.00
CYRN   |  1.08 |   390  |  192  |   10.41 |   0.00 |   8.50 |  1.00 |  390.00
BOSC   |  2.87 |   380  |   98  |    8.73 |   0.00 |   9.50 |  1.00 |  508.00
MKD    |  0.85 |   390  |  127  |    6.69 |   0.00 |  10.00 |  1.00 |  206.00
ANY    |  1.45 |   390  |  117  |    5.37 |   0.00 |   5.00 |  1.00 |  443.00
GVP    |  1.36 |   390  |  170  |    3.81 |   0.00 |   1.00 |  1.00 |  157.00
**BSQR**   |  1.39 |   390  |   80  |    3.75 |   0.00 |   7.00 |  1.00 |  112.00
EVOL   |  2.03 |   390  |  110  |    3.68 |   0.00 |   5.50 |  1.00 |   91.00
MNDO   |  2.59 |   390  |   44  |    3.20 |   0.00 |   4.00 |  1.00 |  250.00
STRM   |  1.63 |   390  |   27  |    2.14 |   0.00 |   8.00 |  1.00 |  162.00
PBTS   |  2.20 |   390  |   42  |    0.94 |   0.00 |   5.50 |  1.00 |   45.00
GSUM   |  1.63 |   390  |   27  |    0.94 |   0.00 |  10.00 |  1.00 |  105.00
LYL    |  2.27 |   390  |   37  |    0.79 |   0.00 |   5.00 |  1.00 |   42.00
{{</table>}}

```
Legend:
- SYMB: symbol
- P: price
- V1: vol_len
- V2: vol_non_zero_len
- V3: vol_avg
- V4: vol_median
- V5: vol_median_non_zero
- vm: vol_min
- VM: vol_max
```


## Trade
We choose IDEX, as high volume, highly traded, prices below.  
Buy price is 2% less than close price. Sell price is 4% higher than buy price.

SYMB | cp   | bp   | sp
---  | ---  | ---  | ---
IDEX | 2.09 | 2.04 | 2.12

SYMB | cp   | f?  | xsp  | f?
---  | ---  | --- | ---  | ---
IDEX | 2.09 | YES | 2.12 | ?

```
Legend:
- cp: close price
- bp: buy price
- sp: sell price
- xbp: actual buy price
- xsp: actual sell price
- f?: order filled?
```

```
Calculations:
- PERC: ((sellprice-buyprice)/buyprice)*100
- bp: cp-(0.02*cp) = 0.98*cp
- sp: bp+(0.04*bp) = 1.04*b
```

### Buy

<img width="30%" src="/media/trade-journal-2020W53/03wed_IDEX_b1.png"></img>
<img width="30%" src="/media/trade-journal-2020W53/03wed_IDEX_b2.png"></img>
### Second buy
<img width="30%" src="/media/trade-journal-2020W53/03wed_IDEX_b3.png"></img>
<img width="30%" src="/media/trade-journal-2020W53/03wed_IDEX_b4.png"></img>
### Sell
<img width="30%" src="/media/trade-journal-2020W53/03wed_IDEX_s1.png"></img>


## Notes
![](/media/trade-journal-2020W53/03wed_IDEX_chart.png)

### Did
Bought for 2.04 at the start of the way. Was hoping for sell at 2.12. Price went down, and I bought again (?!).
In the end sold it at a loss before market close.

### Should have done
- Not trade this symbol on this day
- Settle for less than 4% gain
- Not buy again
- Sell at break-even price

### Conclusion
This IDEX symbol was no good.  
It had high volume, but no volatility. The price was moving in increments 0.01, which is 0.5% for price of $2. There was also a clear downtrend. For some reason, I bought 13 shares again mid-day, anticipating a bounceback.  
We need more volatile stocks, don't just look at high volume.


# 20201231 Thursday
Again, run the volume report (out of interest).
```
# ./report_industry_volume.py Software
```
giving

SYMB   |    P |   V1 |   V2 |     V3 |     V4 |     V5 |    vm |      VM
------ | ---- | ---- | ---- | ------ | ------ | ------ | ----- | -------
IDEX   | 2.09 |  390 |  390 | 752.05 | 478.50 | 478.50 | 32.00 | 6377.00
PHUN   | 1.08 |  390 |  378 | 493.54 | 117.00 | 129.00 |  1.00 | 6198.00
GNUS   | 1.38 |  390 |  390 | 374.70 | 107.00 | 107.00 |  1.00 | 9808.00
XELA   | 0.42 |  390 |  386 | 191.02 |  86.00 |  87.50 |  2.00 | 4271.00
**SEAC** |   1.04 |  390 |  359 | 138.18 |  46.50 |  56.00 |  1.00 | 1789.00
BOXL   | 1.52 |  390 |  352 | 122.95 |  36.00 |  45.00 |  1.00 | 3986.00
CNET   | 1.25 |  390 |  311 |  72.84 |  15.00 |  27.00 |  1.00 | 3183.00
QTT    | 1.59 |  390 |  339 |  67.04 |  14.00 |  23.00 |  1.00 | 1634.00
PIXY   | 2.60 |  390 |  245 |  43.81 |   3.00 |  11.00 |  1.00 | 1285.00
**LKCO**  |  0.71 |  390 |  315 |  43.26 |  13.00 |  27.00 |  1.00 |  492.00
MYSZ   | 1.50 |  390 |  295 |  38.95 |   7.00 |  16.00 |  1.00 |  897.00
INPX   | 1.04 |  390 |  324 |  37.91 |  11.00 |  17.00 |  1.00 | 1115.00
VISL   | 1.35 |  390 |  336 |  37.46 |  15.00 |  20.50 |  1.00 |  397.00
XNET   | 2.88 |  390 |  308 |  32.63 |  12.00 |  20.00 |  1.00 |  594.00
BHAT   | 0.88 |  390 |  229 |  30.24 |   1.00 |  11.00 |  1.00 | 1701.00
PBTS   | 2.20 |  368 |  160 |  28.65 |   0.00 |  11.00 |  1.00 | 1125.00
**VERB**  |  1.61 |  390 |  264 |  22.36 |   4.00 |  10.00 |  1.00 |  653.00
**BSQR**  |  1.39 |  390 |  173 |  20.65 |   0.00 |  10.00 |  1.00 |  669.00
MKD    | 0.85 |  390 |  144 |  14.47 |   0.00 |  11.00 |  1.00 |  330.00
BRQS   | 1.04 |  390 |  195 |  13.74 |   0.50 |  10.00 |  1.00 |  210.00
BOSC   | 2.87 |  390 |  224 |  12.10 |   1.00 |   8.00 |  1.00 |  281.00
CYRN   | 1.08 |  390 |  152 |   9.02 |   0.00 |   6.00 |  1.00 |  236.00
EVOL   | 2.03 |  390 |  202 |   8.33 |   1.00 |   5.00 |  1.00 |  196.00
ANY    | 1.45 |  390 |  134 |   5.83 |   0.00 |   4.00 |  1.00 |  152.00
GVP    | 1.36 |  390 |   51 |   3.98 |   0.00 |   2.00 |  1.00 |  963.00
STRM   | 1.61 |  390 |   44 |   3.59 |   0.00 |  14.00 |  1.00 |  162.00
SSNT   | 2.97 |  390 |  112 |   2.54 |   0.00 |   5.00 |  1.00 |   61.00
GSUM   | 1.63 |  390 |   33 |   1.77 |   0.00 |   8.00 |  1.00 |  136.00
MNDO   | 2.59 |  390 |   32 |   1.02 |   0.00 |   4.00 |  1.00 |  100.00
LYL    | 2.27 |  390 |   23 |   0.36 |   0.00 |   4.00 |  1.00 |   20.00

```
Legend:
- SYMB: symbol
- P: price
- V1: vol_len
- V2: vol_non_zero_len
- V3: vol_avg
- V4: vol_median
- V5: vol_median_non_zero
- vm: vol_min
- VM: vol_max
```

But now also run new report, focus on prices: low, high, open, close, perc difference.
```
# ./report_industry_price.py Software
Get cheaper than
Got 30 symbols
```

SYMB   |    P |    l |    h |   lh_perc |    o |    c |   oc_perc
------ | ---- | ---- | ---- | --------- | ---- | ---- | ---------
**SEAC** | 1.04 | 1.05 | 1.44 |   37.14 | 1.05 | 1.39 |     32.38
PHUN     | 1.08 | 1.10 | 1.50 |   36.36 | 1.17 | 1.35 |     15.44
PBTS     | 2.20 | 2.18 | 2.97 |   36.03 | 2.18 | 2.54 |     16.36
**BSQR** | 1.39 | 1.38 | 1.74 |   26.09 | 1.38 | 1.53 |     11.23
MYSZ     | 1.50 | 1.43 | 1.73 |   20.98 | 1.50 | 1.48 |     -1.67
BOXL     | 1.52 | 1.44 | 1.68 |   16.32 | 1.45 | 1.60 |     10.34
EVOL     | 2.03 | 1.82 | 2.10 |   15.38 | 2.00 | 1.83 |     -8.45
PIXY     | 2.60 | 2.47 | 2.82 |   14.17 | 2.57 | 2.55 |     -0.78
**LKCO** | 0.71 | 0.71 | 0.80 |   12.68 | 0.72 | 0.75 |      4.17
CNET     | 1.25 | 1.23 | 1.37 |   11.38 | 1.25 | 1.33 |      6.36
ANY      | 1.45 | 1.41 | 1.57 |   11.35 | 1.43 | 1.51 |      5.59
**VERB** | 1.61 | 1.61 | 1.79 |   11.19 | 1.63 | 1.72 |      5.52
CYRN     | 1.08 | 1.02 | 1.12 |    9.79 | 1.10 | 1.05 |     -4.55
XELA     | 0.42 | 0.42 | 0.46 |    8.24 | 0.46 | 0.43 |     -5.26
IDEX     | 2.09 | 1.95 | 2.11 |    8.21 | 2.05 | 1.97 |     -3.90
QTT      | 1.59 | 1.56 | 1.67 |    7.05 | 1.57 | 1.61 |      2.87
MKD      | 0.85 | 0.85 | 0.91 |    7.05 | 0.86 | 0.86 |      0.00
LYL      | 2.27 | 2.21 | 2.35 |    6.33 | 2.21 | 2.27 |      2.71
GVP      | 1.36 | 1.31 | 1.39 |    6.11 | 1.37 | 1.37 |      0.00
STRM     | 1.61 | 1.54 | 1.63 |    5.89 | 1.59 | 1.56 |     -1.89
GNUS     | 1.38 | 1.37 | 1.45 |    5.84 | 1.40 | 1.38 |     -1.43
BHAT     | 0.88 | 0.86 | 0.91 |    5.81 | 0.86 | 0.89 |      3.49
SSNT     | 2.97 | 2.98 | 3.15 |    5.70 | 3.00 | 2.99 |     -0.33
VISL     | 1.35 | 1.32 | 1.39 |    5.30 | 1.37 | 1.35 |     -1.54
BOSC     | 2.87 | 2.25 | 2.35 |    4.44 | 2.35 | 2.27 |     -3.62
XNET     | 2.88 | 2.90 | 3.00 |    3.27 | 2.98 | 2.98 |     -0.17
INPX     | 1.04 | 1.01 | 1.04 |    2.97 | 1.03 | 1.02 |     -0.49
BRQS     | 1.04 | 1.01 | 1.03 |    1.98 | 1.02 | 1.02 |      0.00
GSUM     | 1.63 | 1.61 | 1.64 |    1.86 | 1.61 | 1.64 |      1.55
MNDO     | 2.59 | 2.56 | 2.60 |    1.42 | 2.60 | 2.60 |      0.00

```
Legend:
- SYMB: symbol
- P: price
- l: low
- h: high
- lh_perc: lo to hi percentage difference
- o: open
- c: close
- oc_perc: open to close percentage change


Symbols
BSQR SEAC CYRN PIXY LYL PBTS PHUN VERB QTT MNDO IDEX BOSC MYSZ STRM INPX SSNT ANY GNUS BRQS XNET EVOL MKD VISL GVP GSUM BHAT LKCO CNET BOXL XELA
```

Get charts
```
$ SYMBOLS="BSQR SEAC CYRN PIXY LYL PBTS PHUN VERB QTT MNDO IDEX BOSC MYSZ STRM INPX SSNT ANY GNUS BRQS XNET EVOL MKD VISL GVP GSUM BHAT LKCO CNET BOXL XELA"
$ for I in $SYMBOLS; do echo $I; ./get_day_perc.py $I; done
$ for I in $SYMBOLS; do mv $I.png 04thu_$I.png; done
$ mv *.png /home/konrad2/Desktop/gitlab/blog/content/media/trade-journal-2020W53/
# Get copy/paste for blog
$ echo SYMBOL \| CHART; echo --- \| ---; for I in $SYMBOLS; do echo $I\|"<img width=\"30%\" src=\"/media/trade-journal-2020W53/04thu_$I.png\"></img>"; done
```

### Keep
BSQR SEAC CYRN PIXY PBTS PHUN VERB QTT MYSZ ANY GNUS EVOL MKD VISL BHAT LKCO CNET BOXL XELA<br>
<img width="20%" src="/media/trade-journal-2020W53/04thu_BSQR.png" class="red"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_SEAC.png" class="red"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_CYRN.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_PIXY.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_PBTS.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_PHUN.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_VERB.png" class="red"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_QTT.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_MYSZ.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_ANY.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_GNUS.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_EVOL.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_MKD.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_VISL.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_BHAT.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_LKCO.png" class="red"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_CNET.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_BOXL.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_XELA.png"></img>

### Discards from table
lh_perc < 4%

XNET INPX BRQS GSUM MNDO<br>
<img width="20%" src="/media/trade-journal-2020W53/04thu_XNET.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_INPX.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_BRQS.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_GSUM.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_MNDO.png"></img>

# Discards from looking at charts
HFT maybe, tickers with too few data points or small price moves

STRM SSNT GVP LYL IDEX BOSC<br>
<img width="20%" src="/media/trade-journal-2020W53/04thu_STRM.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_SSNT.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_GVP.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_LYL.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_IDEX.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_BOSC.png"></img>

This still leaves 19 symbols undiscarded. That's too many.  
Use an algorithm to spot 4% opportunities, to filter out further.

# Random trade
XNET
<center>
<img width="30%" src="/media/trade-journal-2020W53/04thu_XNET_b1.png"></img><br>
</center>

# New idea: best pair perc gain
Now, we can do something like this.
```
$ ./get_day_perc.py IDEX
{
  "l": {
    "price": 2.02,
    "t": 1609338600000.0,
    "d": "2020-12-30 14:30:00"
  },
  "h": {
    "price": 2.11,
    "t": 1609338960000.0,
    "d": "2020-12-30 14:36:00"
  },
  "perc": 4.455445544554449
}
```
showing that IDEX would have seen a 4.46% gain is bought/sold at these two prices.  
The get_day_perc.py shows the best perc increase on the day.

Combining this into a report:
```
$ ./report_symbols_best_gain.py "$SYMBOLS"
```

giving

{{<table sortable>}}
SYMB     |   tbuy                  |   tsell                 |   pbuy |   psell |   perc
------   |   -------------------   |   -------------------   | ------ | ------- | ------
**SEAC** | **2020-12-30 14:30:00** | **2020-12-30 20:51:00** |   1.05 |    1.44 |  37.14
PHUN     |   2020-12-30 14:31:00   |   2020-12-30 20:19:00   |   1.10 |    1.50 |  36.36
PBTS     |   2020-12-30 14:52:00   |   2020-12-30 17:00:00   |   2.18 |    2.97 |  36.03
**BSQR** | **2020-12-30 14:30:00** |   2020-12-30 19:06:00   |   1.38 |    1.74 |  26.09
MYSZ     |   2020-12-30 14:33:00   |   2020-12-30 16:06:00   |   1.43 |    1.73 |  20.98
BOXL     |   2020-12-30 14:30:00   |   2020-12-30 19:34:00   |   1.44 |    1.68 |  16.32
**LKCO** |   2020-12-30 14:31:00   |   2020-12-30 17:46:00   |   0.71 |    0.80 |  12.68
PIXY     |   2020-12-30 14:31:00   |   2020-12-30 15:27:00   |   2.52 |    2.82 |  11.90
CNET     |   2020-12-30 14:45:00   |   2020-12-30 17:10:00   |   1.23 |    1.37 |  11.38
ANY      |   2020-12-30 14:55:00   |   2020-12-30 18:28:00   |   1.41 |    1.57 |  11.35
**VERB** |   2020-12-30 14:31:00   |   2020-12-30 15:27:00   |   1.61 |    1.79 |  11.19
EVOL     |   2020-12-30 14:34:00   |   2020-12-30 16:01:00   |   1.94 |    2.10 |   8.25
XELA     |   2020-12-30 14:53:00   |   2020-12-30 19:24:00   |   0.42 |    0.46 |   7.41
QTT      |   2020-12-30 14:33:00   |   2020-12-30 15:19:00   |   1.56 |    1.67 |   7.05
MKD      |   2020-12-30 14:33:00   |   2020-12-30 16:21:00   |   0.85 |    0.91 |   7.05
LYL      |   2020-12-30 14:30:00   |   2020-12-30 20:56:00   |   2.21 |    2.35 |   6.33
GVP      |   2020-12-30 14:41:00   |   2020-12-30 14:41:00   |   1.31 |    1.39 |   6.11
BHAT     |   2020-12-30 14:30:00   |   2020-12-30 19:20:00   |   0.86 |    0.91 |   5.81
SSNT     |   2020-12-30 14:30:00   |   2020-12-30 15:50:00   |   3.00 |    3.15 |   5.00
CYRN     |   2020-12-30 15:47:00   |   2020-12-30 18:13:00   |   1.03 |    1.08 |   4.85
GNUS     |   2020-12-30 14:35:00   |   2020-12-30 15:24:00   |   1.39 |    1.45 |   4.66
VISL     |   2020-12-30 15:43:00   |   2020-12-30 18:40:00   |   1.32 |    1.38 |   4.55
IDEX     |   2020-12-30 14:30:00   |   2020-12-30 14:36:00   |   2.02 |    2.11 |   4.46
BOSC     |   2020-12-30 14:48:00   |   2020-12-30 15:27:00   |   2.27 |    2.35 |   3.52
XNET     |   2020-12-30 15:00:00   |   2020-12-30 19:31:00   |   2.90 |    3.00 |   3.27
INPX     |   2020-12-30 15:14:00   |   2020-12-30 19:48:00   |   1.01 |    1.04 |   2.97
STRM     |   2020-12-30 16:14:00   |   2020-12-30 19:16:00   |   1.54 |    1.58 |   2.58
BRQS     |   2020-12-30 14:30:00   |   2020-12-30 14:32:00   |   1.01 |    1.03 |   1.98
GSUM     |   2020-12-30 14:30:00   |   2020-12-30 15:49:00   |   1.61 |    1.64 |   1.86
MNDO     |   2020-12-30 15:04:00   |   2020-12-30 18:16:00   |   2.56 |    2.60 |   1.42
{{</table>}}

```
Legend:
- SYMB: symbol
- tbuy: buy time
- tsell: sell time
- pbuy: buy price
- psell: sell price
```

The 7 symbols less than %4 gain (BOSC XNET INPX STRM BRQS GSUM MNDO) were already filtered out using the previous method, leaving 23 symbols, which is still more than 19.

But, consider the charts, ordered as above, by perc (discards from previous method highlighted)


<img width="20%" src="/media/trade-journal-2020W53/04thu_SEAC.png" class="red"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_PHUN.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_PBTS.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_BSQR.png" class="red"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_MYSZ.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_BOXL.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_LKCO.png" class="red"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_PIXY.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_CNET.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_ANY.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_VERB.png" class="red"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_EVOL.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_XELA.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_QTT.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_MKD.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_LYL.png" class="discard"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_GVP.png" class="discard"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_BHAT.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_SSNT.png" class="discard"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_CYRN.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_GNUS.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_VISL.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_IDEX.png" class="discard"></img>

Next step is to focus on the symbols at the top of the table, and also consider the perc gain over the course of several days. Do symbols reccur? Can we guess a good buy price?


# Report over few days

## Downloader
Remember, IB API allows max. 1,000 data points in market data endpoint (https://interactivebrokers.com/api/doc.html#tag/Market-Data/paths/~1iserver~1marketdata~1history/get).  
As there are 390 minutes (6hr30min in a day) in a trading days, giving 1950 minutes in five days.  
But, using a 2 minute bar width, we get 390/2 = 195 bars per day, giving 975 data points over 5 days, which is within limit.  

Run downloader.
```
# for I in $SYMBOLS; do echo $I; ./down_symbol2day_5days.py $I > /opt/fintools-ib/data/day_5days/$I.json; done
```

Run report
```
$ ./report_symbols_best_gain_5days.py "$SYMBOLS"
```

giving

<center>
<body onload='Load()'>
</body>

<script src="/media/trade-journal-2020W53/js.js"></script>
<style>
  .inline {
    display: inline-block;
    vertical-align: middle;
  }
</style>
<div>
  Percentage:
  <input type="range" class="inline" min="0" max="50" value="10" oninput="UpdateTable(this.value)">
  <div id="label" class="inline">0</div>
</div>
</center>

<div id="table">
{{<table sortable>}}
SYMB     |   2020-12-24 |   2020-12-28 |   2020-12-29 |   2020-12-30 |   2020-12-31
------   | -----------: | -----------: | -----------: | -----------: | -----------:
ANY      |         5.33 |         7.80 |         5.00 |        11.35 |         5.24
BHAT     |         2.53 |         9.82 |         5.87 |         5.81 |         7.22
BOSC     |         0.38 |         6.17 |        18.22 |         3.52 |         2.01
BOXL     |         3.07 |         2.44 |         5.28 |        16.32 |         3.86
BRQS     |         5.00 |         1.98 |         6.12 |         1.98 |         2.00
**BSQR** |        10.08 |         7.01 |         2.96 |        26.09 |         7.59
CNET     |         2.40 |         5.51 |         4.17 |        11.38 |        13.28
CYRN     |         2.74 |         5.62 |        10.00 |         4.85 |         5.83
EVOL     |        15.83 |         8.00 |         5.39 |         8.25 |        10.00
GNUS     |         3.18 |         4.83 |         4.82 |         4.66 |         6.11
GSUM     |         4.32 |         1.85 |         3.12 |         1.86 |         1.86
GVP      |         6.30 |         9.35 |         4.99 |         6.11 |         5.30
IDEX     |         7.58 |         7.88 |         9.66 |         4.46 |         8.33
INPX     |         2.46 |         1.98 |         4.00 |         2.97 |         1.98
**LKCO** |        21.18 |        15.89 |         5.06 |        12.68 |        10.00
LYL      |         4.80 |         7.39 |         5.38 |         6.33 |         4.42
MKD      |         3.49 |         2.78 |         2.89 |         7.05 |         2.38
MNDO     |         0.78 |         0.58 |         6.58 |         1.42 |         0.39
MYSZ     |         4.17 |         4.00 |        24.35 |        20.98 |         7.46
PBTS     |         2.31 |         7.87 |         6.05 |        36.03 |         7.96
PHUN     |         5.00 |         9.91 |         4.85 |        36.36 |        10.74
PIXY     |         5.39 |        15.94 |         7.20 |        11.90 |         2.46
QTT      |         3.12 |         9.74 |         7.64 |         7.05 |         5.00
**SEAC**   |         3.45 |        16.87 |        11.68 |        37.14 |        13.67
SSNT   |         1.31 |         4.35 |         3.70 |         5.00 |         3.15
STRM   |         2.48 |         1.23 |         1.26 |         2.58 |         1.94
**VERB**   |         5.79 |         7.27 |         6.45 |        11.19 |         3.03
VISL   |         6.54 |        15.54 |         4.57 |         4.55 |         3.91
XELA   |         4.96 |         4.02 |        10.39 |         7.41 |         5.98
XNET   |         1.67 |        10.10 |         4.87 |         3.27 |         4.17
{{</table>}}
</div>

```
Legend:
- SYMB: symbol
- d1: best perc increase on day 1
- d2, d3, d4, d5: like above
```

## Thoughts
From looking at charts:

- SEAC: buy on previous close. Increase within first 30min
- PHUN: single day. But many patterns here. Cup on start, cup on exit, falling highs before end
- PBTS: single huge trade mid-day. few data on tradingview
- BSQR: single huge trade end-day. few data on tradingview
- MYSZ: hold until mid-day or couple of hours. Ok to buy at prev close
- BHAT: 

### Like SEAC
percentage of green candles in first 30 mins on the day
```
./report_symbols_day_start_increase.py "$SYMBOLS"
```
gives

{{<table sortable>}}
SYMB     |   2020-12-24 |   2020-12-28 |   2020-12-29 |   2020-12-30 |   2020-12-31 |   avg |   avg-1
------   | -----------: | -----------: | -----------: | -----------: | -----------: | ----: | ------:
ANY      |        43.75 |        56.25 |        37.50 |        37.50 |        50.00 | 45.00 |   43.75
BHAT     |        31.25 |        18.75 |        43.75 |        50.00 |        31.25 | 35.00 |   35.94
BOSC     |         0.00 |        50.00 |        43.75 |        31.25 |        56.25 | 36.25 |   31.25
BOXL     |        43.75 |        37.50 |        37.50 |        68.75 |        43.75 | 46.25 |   46.88
BRQS     |        43.75 |        31.25 |        25.00 |        50.00 |        43.75 | 38.75 |   37.50
**BSQR** |        62.50 |        37.50 |        37.50 |        68.75 |        25.00 | 46.25 |   51.56
CNET     |        25.00 |        68.75 |        18.75 |        43.75 |        68.75 | 45.00 |   39.06
CYRN     |        37.50 |        18.75 |        37.50 |        18.75 |        81.25 | 38.75 |   28.12
EVOL     |        43.75 |        25.00 |        31.25 |        18.75 |         6.25 | 25.00 |   29.69
GNUS     |        81.25 |        31.25 |        31.25 |        75.00 |        37.50 | 51.25 |   54.69
GSUM     |        87.50 |        37.50 |        93.75 |        68.75 |        50.00 | 67.50 |   71.88
GVP      |        81.25 |        62.50 |        68.75 |        31.25 |         0.00 | 48.75 |   60.94
IDEX     |        56.25 |        56.25 |        31.25 |        37.50 |        37.50 | 43.75 |   45.31
INPX     |        37.50 |        25.00 |        56.25 |        43.75 |        56.25 | 43.75 |   40.62
**LKCO** |        56.25 |        75.00 |        37.50 |        68.75 |        31.25 | 53.75 |   59.38
LYL      |        25.00 |        25.00 |        37.50 |        68.75 |         0.00 | 31.25 |   39.06
MKD      |        31.25 |        43.75 |        43.75 |        31.25 |        56.25 | 41.25 |   37.50
MNDO     |        62.50 |        37.50 |        75.00 |         0.00 |        56.25 | 46.25 |   43.75
MYSZ     |        25.00 |        68.75 |        25.00 |        62.50 |        25.00 | 41.25 |   45.31
PBTS     |        68.75 |         6.25 |        12.50 |         0.00 |        43.75 | 26.25 |   21.88
PHUN     |        50.00 |        56.25 |        18.75 |        31.25 |        31.25 | 37.50 |   39.06
PIXY     |        18.75 |        18.75 |        31.25 |        43.75 |        50.00 | 32.50 |   28.12
QTT      |        37.50 |        56.25 |        43.75 |        50.00 |        68.75 | 51.25 |   46.88
**SEAC** |        75.00 |        50.00 |        93.75 |        81.25 |        56.25 | 71.25 |   75.00
SSNT     |        18.75 |        50.00 |        56.25 |        43.75 |        18.75 | 37.50 |   42.19
STRM     |         0.00 |        12.50 |         0.00 |        31.25 |         0.00 |  8.75 |   10.94
**VERB** |        43.75 |        56.25 |        43.75 |        87.50 |        43.75 | 55.00 |   57.81
VISL     |        31.25 |         6.25 |        12.50 |        50.00 |        43.75 | 28.75 |   25.00
XELA     |        43.75 |        31.25 |        37.50 |        37.50 |        25.00 | 35.00 |   37.50
XNET     |        37.50 |        56.25 |        31.25 |        43.75 |        18.75 | 37.50 |   42.19
{{</table>}}

```
Legend:
- SYMB: symbol
- d1: best perc increase on day 1
- d2, d3, d4, d5: like above
```

Look also at avg-1, the GVP had 0% green in the last day. That's also one which was excluded with few data points.

Taking the top, by average-1 (>50%) (Thu 31st charts):
SEAC GSUM GVP LKCO VERB GNUS BSQR  

but excluding GVP GNUS GSUM:  
SEAC LKCO VERB BSQR  
<img width="20%" src="/media/trade-journal-2020W53/04thu_SEAC.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_LKCO.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_VERB.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_BSQR.png"></img>

Taking the top by average (>50%) (Thu 31st):  
SEAC GSUM VERB LKCO QTT GNUS

<img width="20%" src="/media/trade-journal-2020W53/04thu_SEAC.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_GSUM.png" class="discard"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_VERB.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_LKCO.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_QTT.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_GNUS.png" class="discard"></img>

but GSUM GNUS already excluded, so remain (Thu 31st):  
SEAC LKCO VERB QTT

<img width="20%" src="/media/trade-journal-2020W53/04thu_SEAC.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_LKCO.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_VERB.png"></img>
<img width="20%" src="/media/trade-journal-2020W53/04thu_QTT.png"></img>

# Conclusions
Automate the exclusions first and use last 5 days data everywhere.  
Exclusions:
- use volume, average non-zero, min/max maybe too
