---
title: "BlackRock/Vanguard movements"
subtitle: 
date: 2024-12-23
categories: 09 wip
---
WIP  
{{<toc>}}

## Notes/TODO
- Vanguard: https://www.sec.gov/cgi-bin/browse-edgar?CIK=0000102909&action=getcompany
- BlackRock: https://www.sec.gov/cgi-bin/browse-edgar?CIK=0001364742&action=getcompany
- looking for SC 13G/A and 'SCHEDULE 13G/A' 2024-02-13 for LIN
- other filings:
  - SC 13G/A
  - SC 13G
  - SCHEDULE 13G/A
  - 4
  - 144

## Example: SC 13G/A Vanguard own FERG.L on 2024-09-10
BlackRock owned some FERG.L on 2024-09-10 (a small amount of 74594 stocks, 0.0% of FERG.L). As it's a SC 13G/A filing (Amendment), it's not clear whether this is a buy or sell.

However, previous SC 13G/A filing on 2024-02-02, which is for event on 2023-12-31 shows:
```
Sole voting power: 11974293 (6.5%)
```
compared to 2024-09-10, which is a filing for 2024-08-31:
```
Sole voting power: 74594
```
so that's a sale of 11899699 shares. So, although the stock was red on that day, why did it go up so much over the subsequent days? Were the big price reductions in the previous days due to Vanguard selling? Because rule 13d-1(b) has:
```
Filing Deadline: Within 45 days after the end of the calendar year in which the 5% threshold is crossed, unless ownership exceeds 10% (then a more immediate filing is required).
```
So they could have been selling anytime in 2024? And they could have been buying anytime in 2023 until reached 6.5% on 2023-12-31.

Significant gains since 2024-09-01:
<center>
<img style="width:60%" src="/media/study-blackrock-vanguard/ferg_gains.png"></img>
</center>

Highlighted dates:
- `2024-09-11 - 2024-09-19: 8.92% gain`
- `2024-10-08 - 2024-10-18: 6.44% gain`
- `2024-11-05 - 2024-11-06: 5.44% gain`
- `2024-11-18 - 2024-11-25: 7.70% gain`

FERG.L has no subsequent filings with SEC as it's a LSE-traded company so this concludes this example, as we focus only on NASDAQ companies.

## Example: SC 13G/A Vanguard own NI (Nisource Inc) on 2024-09-10
SC 13G/A, 10.5% of the shares. This is /A so Amendment, so it's unclear if this is a buy or a sell.

<center>
<img style="width:60%" src="/media/study-blackrock-vanguard/ni_blk.png"></img>
</center>

## Other examples:
- Example: Vanguard buy iSHARES on 2024-09-10 1%, but iSHARES has multiple tickers for different trusts (over a thousand different ETFs), so skipping this example as can't see the combined price
