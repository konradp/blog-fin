---
title: "Swing trading plan"
subtitle: 
date: 2025-02-03
categories: 08 journal
math:
  enable: true
---
Table of contents:
{{<toc>}}

## Intro
This is for the Goal 2 for year 2025, from my [blog: Year goals plan](/posts/journal-year-plan/) where the goal is stated as:

<div style='background:lightgray'>

**Design and trade a swing-trading strategy**
  - doesn't have to be good or working, but well-defined
  - as part of this, skill up in indicators and signals, e.g. bollinger, mean-reversion, RSI, MACD, VWAP, VIX etc

</div>


Plan:
- {{<w>}} New blog page: indicators
- {{<x>}} a few strategies backtested, e.g. RSI and my own volume
- {{<x>}} a few strategies backtested, with parameter fuzzing (document what the 'fuzzing' is, and find the proper term for this (is it Monte Carlo simulation?)
- {{<x>}} refined: how to hold, hold over SEC earnings?
- {{<x>}} refined: how long to hold for?
- {{<x>}} stops? 1% drawdown, 4% increase?
- {{<x>}} my own volume: how to scale prices based on volume traded
  - {{<x>}} highlight for increased volume <- daily volume above last X days average volume?
- {{<x>}} candlestick patterns


Expected outcome at the end of the month (February):
- {{<x>}} A scanner which highlights which stocks to buy - doesn't have to result in live trades, but a daily signal for stocks to watch
- {{<x>}} a running paper trading strategy: pretend buys and sells, e.g. a week


## TBP system: Trend Balance Point system
- from JWW, section V
- "three to five trades a week"
- MF: momentum factor: difference between close today and the close two days ago
  - $C_0 - C_{-2}$, where $C_0$ is close today, and $C_{-2}$ is close two days ago

The method is:
- go long on the close today if:
  - $MF_{today} > min(MF_{-1}, MF_{-2})$
- go short on the close today if:
  - $MF_{today} < max(MF_{-1}, MF_{-2})$
- what do these mean?:
  - take profit at the target, do not reverse
  - exit market at the STOP, do not reverse
  - when out of the market (either at the target or at the stop), reenter on the close under the first or second procedure as applicable
- TBP (Trend Balance Point):
  - if long: the point the price must close **below** in order to reverse to short
  - if short: the point the price must close **above** in order to reverse to long
  - calculated by:
    - TBP for tomorrow if long:
      - pick lower of the two previous MF, and add it to yesterday's close
    - TBP for tomorrow if short:
      - pick higher of the two previous MF, and add it to yesterday's close
- TODO: unclear how to pick long or short the first time we enter the market

### Code from tradingview.com
https://www.tradingview.com/script/w12N5Pq3-Trend-Balance-Point-System-by-Welles-Wilder/


{{<highlight java>}}
// This source code is subject to the terms of the Mozilla Public License 2.0 at https://mozilla.org/MPL/2.0/
// © 2020 X-Trader.net

//@version=3
strategy("Trend Balance Point System by Welles Wilder", overlay=true, default_qty_type = strategy.percent_of_equity, default_qty_value = 100, initial_capital = 10000)

MomPer = input(2, "Momentum Period")

isLong = strategy.position_size > 0
isShort = strategy.position_size < 0

longTrigger = mom(close, MomPer)[1] > mom(close, MomPer)[2] and mom(close, MomPer)[1] > mom(close, MomPer)[3]
shortTrigger = mom(close, MomPer)[1] < mom(close, MomPer)[2] and mom(close, MomPer)[1] < mom(close, MomPer)[3]

longEntry = (not isLong) and longTrigger 
shortEntry = (not isShort) and shortTrigger

longStop = valuewhen(longEntry, ((high[1]+low[1]+close[1])/3 - (high[1]-low[1])), 0)
longTP = valuewhen(longEntry, (2*(high[1]+low[1]+close[1])/3 - low[1]), 0)
shortStop = valuewhen(shortEntry, ((high[1]+low[1]+close[1])/3 + (high[1]-low[1])), 0)
shortTP = valuewhen(shortEntry, (2*(high[1]+low[1]+close[1])/3 - high[1]), 0)

strategy.entry(id = "Long", long = true, when = (longEntry and not isShort))
strategy.exit("Exit Long", "Long", profit = longTP, loss = longStop,  when = isLong) 

strategy.entry(id = "Short", long = false, when = (shortEntry and not isLong))
strategy.exit("Exit Short", "Short", profit = shortTP, loss = shortStop,  when = isShort) 
{{</highlight>}}

### Code from prorealcode.com
https://www.prorealcode.com/prorealtime-indicators/trend-balance-point-tbp/

{{<highlight java>}}
periodo=2
i=1
f=i+periodo

MFieri=dclose(i)-dclose(f)
MFaltroieri=dclose(i+1)-dclose(f+1)
massimo=MAX(MFieri,MFaltroieri)
minimo=MIN(MFieri,MFaltroieri)
//Calcolo TBP
TBPiflong=dclose(periodo)+minimo
TBPifshort=dclose(periodo)+massimo

//Calcolo dello Stop loss
atr1=dhigh(1)-dlow(1)
atr2=dhigh(1)-dclose(2)
atr3=dlow(1)-dclose(2)

a=MAX(atr1,atr2)
b=max(a,atr3)

SLiflong=((dclose(1)+DLow(1)+dhigh(1))/3)-b
SLifshort=((dclose(1)+DLow(1)+dhigh(1))/3)+b

////Calcolo del Target Price
TPiflong=(2*((dclose(1)+DLow(1)+dhigh(1))/3))-dlow(1)
TPifshort=(2*((dclose(1)+DLow(1)+dhigh(1))/3))-dhigh(1)

TPL=round(100*TPiflong)/100
TPS=round(100*TPifshort)/100
SLL=round(100*SLiflong)/100
SLS=round(100*SLifshort)/100
once long=0
once short=0
if (close crosses over TBPifshort) then
  long=1
  short=0
endif
if (close crosses under TBPiflong) then
  long=0
  short=1
endif

if short=1 then
  if close crosses over TBPiflong then
    long=1
    short=0
  endif
endif


if long=1 then
  DRAWARROWUP(barindex, low*0.999) coloured (0,255,0)
endif
if long[1]=1 then
  DRAWTEXT("TP=#TPL#", barindex, high*1.005)
  DRAWTEXT("SL=#SLL#", barindex, low*0.995)
endif
if short=1 then
  DRAWARROWDOWN(barindex, high*1.001) coloured (255,0,0)
endif
if short[1]=1 then
  DRAWTEXT("SL=#SLS#", barindex, high*1.005)
  DRAWTEXT("TP=#TPS#", barindex, low*0.995)
endif
return
{{</highlight>}}

a comment:
```
I tested the strategy on the daily Mib on an excel sheet in January, on 7 days in which ADX < 20 makes 100% gain while where ADX> 20 has results lower than 50% and since the stop losses are about triple the take profits the strategy is actually in loss, I will try to continue the test in other months also considering the commissions that I have not calculated for now. I will try to do more in-depth tests but it seems like a winning strategy at first sight.`
```
- TODO: What is ADX?

a comment:
```
Alex, thanks for doing the backtest. Wilder was a little genius even if with unorthodox ideas at times. By his own definition this system has a negative risk/reward ratio (intended) and therefore you necessarily have to win much more often than you lose. He also takes for granted that this system (like the others he created including RSI and ADX) are used on instruments with a high Commodity Selection Index (CSI). Maybe in the next backtests you can also take this into account.
```
a comment:
```
Coding the CSI is possible. I use a modification of it (since I only work on stocks, I don't need many parameters) and it boils down to multiplying ADXR by ATR. I don't use a normal ATR but a balanced ATR (I attach the formula). The screener returns the stocks with the highest CSI value and then at that point it's easy.

a=100*averagetruerange[period](close)/average[period](close)
```
a comment:
```
I share with you the evolution of my study, you have made me passionate about Wilder that as an ignorant I did not know. I tried to code in excel the ADX and the Trade Balance Point as per your code and the first results are excellent I would say. Soon I will try to simplify the spreadsheet and if I can share it. I do not believe that the strategy can be automated because as conceived most of the trades are closed during the day and the tick by tick backtest does not allow a long-term analysis, in addition to the problem of not being able to enter at the closing but only at the opening. However, know that even entering at the opening the results remain good. So I continue on excel with the problem of finding the correct intraday data for the various commodities
```
TODO: Try this: enter at the open




## Notes
### Candlestick patterns
TODO: On 2024-11-19, the green candle which was below the range of the previous red candle. Is it a pattern?
<center>
  <img src='/media/journal-swing-trading-plan/20241120_CTSH.png'></img>
</center>


## Reference
- JWW: J. Welles Wilder, 1978: New Concepts In Technical Trading Systems
