---
title: 'System 3: NASDAQ 100: earnings'
subtitle: 
date: 2024-11-11
categories: 00 studies
---
{{<toc>}}
<script src="https://konradp.gitlab.io/finplotjs/js/finplot.js"></script>

## Overview
Trading company earnings.

TODO:
- Get earnings calendar
- Decide: 10-K, 10-Q, or 8-K
- price for gddy around the earnings
- get ib trading station

## Motivation: GDDY 10-Q earnings reports
10-Q reports on dates:
- 2024-02-29
- 2024-05-03
- 2024-08-02
- 2024-10-31

<center>
<img src="/media/study-system3/gddy4.png"></img>
</center>

### 2024.10.30 report
We take a closer look at the 2024.10.30 GDDY earnings report. When was it released exactly? Looking at [SEC/EDGAR filings](https://www.sec.gov/edgar/browse/?CIK=1609711&owner=exclude), and other sites, we see:
- 2024-10-30 09:30 ET: NASDAQ open
- 2024-10-30 16:00 ET: NASDAQ close
- 2024-10-30 16:05 ET: [prnewswire.com](https://www.prnewswire.com/news-releases/godaddy-reports-third-quarter-2024-financial-results-302292022.html)
- 2024-10-30 16:11 ET: SEC 8-K Current report:
  - 2.02 - Results of Operations and Financial Condition
  - 9.01 - Financial Statements and Exhibits, link to [gddy site press release](https://aboutus.godaddy.net/newsroom/news-releases/press-release-details/2024/GoDaddy-Reports-Third-Quarter-2024-Financial-Results/default.aspx) (time unclear, but it links to prnewswire.com as 'original content')
- 2024-10-31 18:19 ET: SEC 10-Q Quarterly report [Sections 13 or 15(d)]

<center>
<img src="/media/study-system3/gddy.png"></img>
</center>
with prices:

date       | o      | h      | l      | c      | v     | perc
---        | ---    | ---    | ---    | ---    | ---   | ---
2024-10-28 | 161.96 | 162.17 | 159.82 | 159.90 | 823k  | ?
2024-10-29 | 160.00 | 161.50 | 159.71 | 161.35 | 878k  | +0.91 %
**2024-10-30** | 161.82 | 162.83 | 160.96 | 161.60 | 1.42m | +0.15 %
**2024-10-31** | 164.30 | 171.93 | 163.68 | 166.80 | 2.68m | **+3.22 %**
2024-11-01 | 167.79 | 167.98 | 162.21 | 163.36 | 1.55m | -2.06 %
2024-11-04 | 163.35 | 165.71 | 161.43 | 165.51 | 1.30m | +1.32 %
2024-11-05 | 165.50 | 168.05 | 165.25 | 167.63 | 945k  | +1.28 %
2024-11-06 | 171.00 | 175.98 | 170.00 | 175.81 | 1.78m | +4.88 %
2024-11-07 | 176.52 | 177.77 | 175.04 | 177.31 | 1.21m | +0.85 %

Note: perc is the percent change between previous close and current close

2024-08-02 10-Q
<center>
<img src="/media/study-system3/gddy2.png"></img>
</center>


## Getting earnings calendar
questions:
- future earning calendars: where does the data come from? Is it reliable?
- am I better off getting already published earnings reports from SEC/Edgar?

ref: [blog: koyfin/top-earning-calendar-platforms](https://www.koyfin.com/blog/top-earnings-calendar-platforms/)

- SEC/Edgar
- other portals?
  - yahoo
  - koyfin
  - tradingview
  - investing.com
  - ycharts
  - finchat.io
  - zacks


## SEC/Edgar
form types:
- 10-K: annual reports
- 10-Q: quarterly reports
- 8-K: current reports that may include earnings announcements

urls:
- https://www.sec.gov/search-filings/edgar-search-assistance/accessing-edgar-data  
  request header, CIK translation, especially these two:
  - https://www.sec.gov/files/company_tickers.json
  - https://www.sec.gov/files/company_tickers_exchange.json
- https://www.sec.gov/search-filings/edgar-application-programming-interfaces  
  API endpoints
- the CIK is 10 chars long, zero-padded on the left, e.g. GDDY is CIK0001609711

There are four endpoints:
- https://data.sec.gov/submissions/CIK##########.json
- data.sec.gov/api/xbrl/companyconcept/
- https://data.sec.gov/api/xbrl/companyfacts/CIK##########.json
- data.sec.gov/api/xbrl/frames/


### submissions
e.g. for AAPL: https://data.sec.gov/submissions/CIK0001609711.json  
See `.filings.recent`:

```
HEADER                EXAMPLE
acceptanceDateTime    2024-12-18T18:06:03.000Z
accessionNumber       0001609711-24-000207
act                   33
core_type             4, XBRL, 144, SC 13G/A, SC 13D/A, CORRESP, ARS, DEFA14A, LETTER
fileNumber            001-36904
filingDate            2024-12-18
filmNumber            241552673
form                  4, 8-K, 144, 8-K/A, SC 13G/A, UPLOAD, CORRESP
isInlineXBRL          0, 1
isXBRL                0, 1
items                 1.01,2.03,9.01
primaryDocDescription FORM 4, 8-K, JANUS CLOSEOUT, AMENDMENT NO. 5 TO The SCHEDULE 13D
primaryDocument       xslF345X05/wk-form4_1734563157.xml
reportDate            2024-12-16
size                  4632
```

### companyconcept
Can pick the individual items from companyfacts below.

e.g. for AAPL: https://data.sec.gov/api/xbrl/companyconcept/CIK0001609711/us-gaap/ProfitLoss.json

```
$ jq -r '.units.USD[] |  [.filed, .val ] | @tsv' ProfitLoss.json | sort
[...]
2024-05-03	401500000
2024-05-03	47400000
2024-08-02	130500000
2024-08-02	146300000
2024-08-02	401500000
2024-08-02	47400000
2024-08-02	547800000
2024-08-02	83100000
2024-10-31	131000000
2024-10-31	146300000
2024-10-31	190500000
2024-10-31	261500000
2024-10-31	401500000
2024-10-31	47400000
2024-10-31	738300000
2024-10-31	83100000
```
but understand:
```
{
  "start": "2024-07-01",
  "end": "2024-09-30",
  "val": 190500000,
  "accn": "0001609711-24-000167",
  "fy": 2024,
  "fp": "Q3",
  "form": "10-Q",
  "filed": "2024-10-31",
  "frame": "CY2024Q3"
}
```



### companyfacts
e.g. for AAPL: https://data.sec.gov/api/xbrl/companyfacts/CIK0001609711.json  
See `.facts`:
```
facts:
  dei:
    EntityCommonStockSharesOutstanding
    EntityPublicFloat
  us-gaap:
    AccountsPayableCurrent
    AccountsReceivableNetCurrent
    AccrualForTaxesOtherThanIncomeTaxesCurrent
    Assets
    AssetsCurrent
    Revenues
    ...    
```
very useful data in `us-gaap`.

Descriptions of all company facts: <a href="/media/study-system3/company_facts.txt">company_facts.txt</a>


### frames
TODO


## Study
### How often are earnings published?
Quaterly, but not on the same day, see the chart below for 10-Q filing dates for each NASDAQ 100 constituent for 2024, plotted against the overall NASDAQ 100 index price.

<center>
<img src="/media/study-system3/earnings1.png"></img>
</center>


## System
As per my article `data sources and facts`, we can get NASDAQ 100 constituents through https://api.nasdaq.com/api/quote/list-type/nasdaq100

### Other notes
- 2024-11-20
  - CSGP +5.34%
  - TTD +4.61%
  - WBD +3.11%
- 2024-11-19
  - SMCI +31.80%
  - APP +6.68%
  - DDOG +5.03%
  - NVDA +4.77%
  - ARM +3.65% 
  - TEAM +3.37%
  - NFLX +3.03%
