---
title: "NASDAQ price structure"
subtitle: 
date: 2024-09-30
categories: 00 studies
---
Table of contents:
{{<toc>}}


## Overview
Goal: To be able to understand the price and market cap structure of NASDAQ. Also, the sectors/industries distribution.

**Note:** Unless specified otherwise, the data is from 2025.01.07.

TODO and fixes remaining:
- the 'price' chart title here should be 'market cap'<br>
- Lorentz curve and Gini coefficient for price distribution<br>
- followup: For a specific ticker, where is it on the market cap or price boxplot for the entire NASDAQ?<br>
- What are the empty sectors?
- NASDAQ 100 Historical price (maybe also against other indices, against NASDAQ composite etc)<br>
- Do also 'Global Market', 'Capital Market', 'ADR'.<br>
- DO NASDAQ/GS Market Cap

Main TODO remaining:
- any useful insights or followups from this article?
- how do the findings inform trading decisions?
- can I get entry points or any buy signals from this?
- can I get overall market sentiment from this?
- overall market direction from this?


## NASDAQ (composite)
### Overview
The https://www.nasdaq.com/market-activity/stocks/screener provides a CSV export. Without any filters, it returns 6968 stocks, with fields:  
- `Symbol,Name,Last Sale,Net Change,% Change,Market Cap,Country,IPO Year,Volume,Sector,Industry`

Count of stocks with filters applied:
- NASDAQ: 3926 stocks, with tiers:
  - Global Select: 1523 stocks
  - Global Market: 778 stocks
  - Capital Market: 1626 stocks
  - ADR: 197 stocks

Regarding the tiers:
- the sum of the Global Select, Global Market, and Capital Market counts gives 3927
    - the extra stock is the BSIIU (Black Spade Acquisition II Co Unit) which is in Global Market, but is somehow missing from overall NASDAQ?
- the Global Select, Global Market, Capital Market, do not have any stocks in common
- each stock from ADR is featured in one of the other three tiers, with counts:
  - Global Select: 84, e.g. NICE, LOT
  - Global Market: 50, e.g. BNR, IVA
  - Capital Market: 63, e.g. KZIA, NAMI

First, we will study the full NASDAQ, with the individual tiers further below.


### Historical price
Here we look at [wiki: Nasdaq Composite](https://en.wikipedia.org/wiki/Nasdaq_Composite) (ticker ^IXIC) which includes almost all stocks from NASDAQ. As per wiki, the price is cap-weighted. See the price:
- [NASDAQ composite historical price](./ndq-price-history) (TODO: Finish the range slider/selector)

or the chart from wiki:
<center>
  <img src="/media/study-nasdaq-price-structure/nasdaq_composite_price.webp"></img>
</center>

Or other charts, also from wiki: [chart1](/media/study-nasdaq-price-structure/nasdaq_composite_price.svg), [chart2](/media/study-nasdaq-price-structure/nasdaq_composite_price2.svg).

TODO: maybe also against other indices<br>


### Price
Interactive charts:
- [ndq-price-200](./ndq-price)
- [ndq-marketcap](./ndq-marketcap)

Out of 3926 stocks, 93.73% are within the (0,100] price range.

price | count | note
--- | --: | --:
**0-100** | **3681** | **93.76%**
100-200   | 146      | 3.72%
200-300   | 46       | 1.17%
300-400   | 22       | 0.56%
400-500   | 12       | 0.31%

with the remaining high-price stocks: ROP 506.455, ISRG 532.845, AXON 562.29, DJCO 566.375, INTU 616.81, META 621.24, MPWR 632.64, ARGX 653.585, KLAC 694.76, REGN 731.12, ASML 764.09, NFLX 880.76, COST 920.525, EQIX 942.94, ORLY 1204.95, COKE 1305.7, MELI 1791.285, FCNCA 2135.28, BKNG 4808.095.

For stocks in the range (0, 100] with bin size=10, we see that there also, most of the stocks are in the leftmost price range (0,10].

histogram (another version [here](./ndq-price) )
<center>
  <div id="ndq-price-charts">
    <img chartId="1" src="/media/study-nasdaq-price-structure/ndq/price_histogram1.png" style="display:none"></img>
    <img chartId="10" src="/media/study-nasdaq-price-structure/ndq/price_histogram10.png" style="display:none"></img>
    <img chartId="100" src="/media/study-nasdaq-price-structure/ndq/price_histogram100.png" style="display:none"></img>
    <img chartId="500" src="/media/study-nasdaq-price-structure/ndq/price_histogram500.png" style="display:none"></img>
    <img chartId="1000" src="/media/study-nasdaq-price-structure/ndq/price_histogram1000.png" style="display:none"></img>
    <img chartId="5000" src="/media/study-nasdaq-price-structure/ndq/price_histogram5000.png" style="display:block"></img>
  </div>
  <script src="/media/study-nasdaq-price-structure/js/ndq-price-histogram-slider.js"></script>
  <div class="slider-container">
    <div class="value-display">value: <span id="display-value">5000</span></div>
    <input id="range-slider" type="range" min="0" max="5" step="1" value="5" style="width:70%">
  </div>
</center>

boxplot:
<center>
  <div id="ndq-price-boxplots">
    <img class="wide" chartId="1" src="/media/study-nasdaq-price-structure/ndq/price_boxplot1.png" style="display:none"></img>
    <img class="wide" chartId="10" src="/media/study-nasdaq-price-structure/ndq/price_boxplot10.png" style="display:none"></img>
    <img class="wide" chartId="100" src="/media/study-nasdaq-price-structure/ndq/price_boxplot100.png" style="display:none"></img>
    <img class="wide" chartId="500" src="/media/study-nasdaq-price-structure/ndq/price_boxplot500.png" style="display:none"></img>
    <img class="wide" chartId="5000" src="/media/study-nasdaq-price-structure/ndq/price_boxplot5000.png" style="display:block"></img>
  </div>
  <script src="/media/study-nasdaq-price-structure/js/ndq-price-boxplot-slider.js"></script>
  <div class="slider-container">
    <div class="value-display">value: <span id="boxplot-value">5000</span></div>
    <input id="boxplot-slider" type="range" min="0" max="4" step="1" value="4" style="width:70%">
  </div>
</center>
<hr>

On 2024-06-21, these hold:
- 1727 stocks
- average price: 55.73 $ per stock
- highest price: BKNG: 3989.1
- lowest price: SLNA: 0.067
- count of stocks above average: 372
- count of stocks below average: 1355

min: <input type="number" id="price-min" value="50"></input><br>
max: <input type="number" id="price-max" value="55"></input><br>
<button id="price-submit">Submit</button><br>
count: <span id="price-count"></span><br>
<button id="show-hide">show/hide</button>
<div id="price-stocks-div" style="display:none">
  <code><pre id="price-stocks"></pre></code>
</div>
ref: [prices-20240621.json](/media/study-nasdaq-price-structure/prices-20240621.json)
<script src="/media/study-nasdaq-price-structure/price.js"></script>
<hr>


### Market cap
First of all, there are 280 stocks with 0.00 market cap, [here](/media/study-nasdaq-price-structure/nasdaq_screener_1736274256746_nasdaq_zero_market_cap.txt). We exclude these in the charts below.  
The max market cap is $3,662,110,438,210

histogram:
<center>
  <div id="ndq-mc-histograms">
    <img chartId="e05" src="/media/study-nasdaq-price-structure/ndq/mc_histogram_e05.png" style="display:none"></img>
    <img chartId="e06" src="/media/study-nasdaq-price-structure/ndq/mc_histogram_e06.png" style="display:none"></img>
    <img chartId="e07" src="/media/study-nasdaq-price-structure/ndq/mc_histogram_e07.png" style="display:none"></img>
    <img chartId="e08" src="/media/study-nasdaq-price-structure/ndq/mc_histogram_e08.png" style="display:none"></img>
    <img chartId="e09" src="/media/study-nasdaq-price-structure/ndq/mc_histogram_e09.png" style="display:none"></img>
    <img chartId="e10" src="/media/study-nasdaq-price-structure/ndq/mc_histogram_e10.png" style="display:none"></img>
    <img chartId="max" src="/media/study-nasdaq-price-structure/ndq/mc_histogramMax.png" style="display:block"></img>
  </div>
  <script src="/media/study-nasdaq-price-structure/js/ndq-mc-histogram-slider.js"></script>
  <div class="slider-container">
    <div class="value-mc">value: <span id="display-mc">max</span></div>
    <input id="range-mc" type="range" min="0" max="6" step="1" value="6" style="width:70%">
  </div>
</center>

boxplot:
<center>
  <div id="ndq-mc-boxplots">
    <img class="wide" chartId="e04" src="/media/study-nasdaq-price-structure/ndq/mc_boxplot_e04.png" style="display:none"></img>
    <img class="wide" chartId="e05" src="/media/study-nasdaq-price-structure/ndq/mc_boxplot_e05.png" style="display:none"></img>
    <img class="wide" chartId="e06" src="/media/study-nasdaq-price-structure/ndq/mc_boxplot_e06.png" style="display:none"></img>
    <img class="wide" chartId="e07" src="/media/study-nasdaq-price-structure/ndq/mc_boxplot_e07.png" style="display:none"></img>
    <img class="wide" chartId="e08" src="/media/study-nasdaq-price-structure/ndq/mc_boxplot_e08.png" style="display:none"></img>
    <img class="wide" chartId="e09" src="/media/study-nasdaq-price-structure/ndq/mc_boxplot_e09.png" style="display:none"></img>
    <img class="wide" chartId="e10" src="/media/study-nasdaq-price-structure/ndq/mc_boxplot_e10.png" style="display:none"></img>
    <img class="wide" chartId="e11" src="/media/study-nasdaq-price-structure/ndq/mc_boxplot_e11.png" style="display:none"></img>
    <img class="wide" chartId="e12" src="/media/study-nasdaq-price-structure/ndq/mc_boxplot_e12.png" style="display:none"></img>
    <img class="wide" chartId="max" src="/media/study-nasdaq-price-structure/ndq/mc_boxplot_max.png" style="display:block"></img>
  </div>
  <script src="/media/study-nasdaq-price-structure/js/ndq-mc-boxplot-slider.js"></script>
  <div class="slider-container">
    <div class="value-display">value: <span id="mc-boxplot-value">5000</span></div>
    <input id="mc-boxplot-slider" type="range" min="0" max="9" step="1" value="9" style="width:70%">
  </div>
</center>


### Industry
Many industries
```
$ cat industry.txt | sort | uniq -c | sort -h
      1 Accident &Health Insurance
      1 Aluminum
      1 Books
      1 Diversified Electronic Products
[...]
    140 Industrial Machinery/Components
    143 Blank Checks
    153 
    181 Biotechnology: Biological Products (No Diagnostic Substances)
    198 Computer Software: Prepackaged Software
    235 Major Banks
    539 Biotechnology: Pharmaceutical Preparations
```

### Sector
There are 13 sectors, with stock counts (including EMPTY). Most stocks are in Health Care (26.21%), followed by Finance (19.54%), Consumer Discretionary (17.01%), and Technology (15.23%). Also, another version of the chart [here](./ndq-pie).

<center><img class="wide" src="/media/study-nasdaq-price-structure/ndq/sector.png"></img></center>

CAT | COUNT | PERC
--- | --: | --:
Health Care | 1029 | 26.21
Finance | 767 | 19.54
Consumer Discretionary | 668 | 17.01
Technology | 598 | 15.23
Industrials | 281 | 7.16
EMPTY | 152 | 3.87
Real Estate | 91 | 2.32
Consumer Staples | 91 | 2.32
Telecommunications | 67 | 1.71
Energy | 58 | 1.48
Utilities | 50 | 1.27
Miscellaneous | 46 | 1.17
Basic Materials | 28 | 0.71

Looking closer at the symbols which have EMPTY Sector (the list of 152 tickers [here](/media/study-nasdaq-price-structure/nasdaq_screener_1736274256746_nasdaq_empty_sector.txt)), it appears that many of them also have 0.00 marketcap. Checking one of the examples, TBCH (Turtle Beach Corporation), the company appears completely fine, and regenerating the data now shows the Sector as 'Telecommunications'. This appears to be a problem with the CSV export from nasdaq.com/market-activity/stocks/screener. Retrieving the same CSV on 2024-01-21 shows 160 such tickers. Some are empty no matter how many times I try, e.g. ALF (Centurion Acquisition Corp. Class A Ordinary Shares), and I notice that ALFUW (Centurion Acquisition Corp. Warrant) does have a sector of 'Finance'. Interestingly, ALF does not have much data in Yahoo Finance either.


## NASDAQ/Global Select
### Price
Out of the 1523 Global Select stocks, 1297 of them (85.16%) are in the 0-100 price range, followed by 138 (9.06%) are in the 100-200 price range.

RANGE      | COUNT      | PERC      
--:        | --:        | --:       
**0-100**  | **1297**   | **85.16%**
100-200    | 138        | 9.06%
200-300    | 37         | 2.43%
300-400    | 21         | 1.38%
400-500    | 12         | 0.79%
500-600    | 3          | 0.20%
600-700    | 5          | 0.33%
700-800    | 2          | 0.13%
800-900    | 1          | 0.07%
900-1000   | 2          | 0.13%

histogram:
<center>
  <div id="ndq-gs-price-histograms">
    <img chartId="1" src="/media/study-nasdaq-price-structure/ndq-gs/price_histogram1.png" style="display:none"></img>
    <img chartId="10" src="/media/study-nasdaq-price-structure/ndq-gs/price_histogram10.png" style="display:none"></img>
    <img chartId="100" src="/media/study-nasdaq-price-structure/ndq-gs/price_histogram100.png" style="display:none"></img>
    <img chartId="500" src="/media/study-nasdaq-price-structure/ndq-gs/price_histogram500.png" style="display:none"></img>
    <img chartId="1000" src="/media/study-nasdaq-price-structure/ndq-gs/price_histogram1000.png" style="display:none"></img>
    <img chartId="max" src="/media/study-nasdaq-price-structure/ndq-gs/price_histogramMax.png" style="display:block"></img>
  </div>
  <script src="/media/study-nasdaq-price-structure/js/ndq-gs-price-histogram-slider.js"></script>
  <div class="slider-container">
    <div class="value-mc">value: <span id="display-mc">max</span></div>
    <input id="ndq-gs-price-histogram-slider" type="range" min="0" max="5" step="1" value="5" style="width:70%">
  </div>
</center>

boxplot:
<center>
  <div id="price-gs-boxplots">
    <img class="wide" chartId="1" src="/media/study-nasdaq-price-structure/ndq-gs/price_boxplot1.png" style="display:none"></img>
    <img class="wide" chartId="10" src="/media/study-nasdaq-price-structure/ndq-gs/price_boxplot10.png" style="display:none"></img>
    <img class="wide" chartId="100" src="/media/study-nasdaq-price-structure/ndq-gs/price_boxplot100.png" style="display:none"></img>
    <img class="wide" chartId="500" src="/media/study-nasdaq-price-structure/ndq-gs/price_boxplot500.png" style="display:none"></img>
    <img class="wide" chartId="5000" src="/media/study-nasdaq-price-structure/ndq-gs/price_boxplot5000.png" style="display:block"></img>
  </div>
  <script src="/media/study-nasdaq-price-structure/js/ndq-gs-price-boxplot-slider.js"></script>
  <div class="slider-container">
    <div class="value-display">value: <span id="price-gs-boxplot-value">5000</span></div>
    <input id="price-gs-boxplot-slider" type="range" min="0" max="4" step="1" value="4" style="width:70%">
  </div>
</center>


### Market cap
TODO

### Sector
There are 13 sectors in NASDAQ Global Select (including EMPTY). Most of them in Finance (22.32%), then Health Care (20.42%), and so on.

<center><img class="wide" src="/media/study-nasdaq-price-structure/ndq-gs/sector.png"></img></center>

CAT | COUNT | PERC
--- | --: | --:
Finance | 340 | 22.32
Health Care | 311 | 20.42
Technology | 279 | 18.32
Consumer Discretionary | 277 | 18.19
Industrials | 123 | 8.08
Real Estate | 45 | 2.95
Telecommunications | 35 | 2.30
Consumer Staples | 34 | 2.23
Energy | 26 | 1.71
Utilities | 25 | 1.64
EMPTY | 14 | 0.92
Miscellaneous | 9 | 0.59
Basic Materials | 5 | 0.33


## NASDAQ 100
These are the companies listed here: https://api.nasdaq.com/api/quote/list-type/nasdaq100<br>
The NASDAQ 100 appears to be a subset of NASDAQ/Global Select, except for TTD (The Trade Desk, Inc.), which is in NASDAQ/Global Market.


### Price

RANGE      | COUNT      | PERC      
--:        | --:        | --:       
0-100      | 31         | 30.69%
100-200    | 27         | 26.73%
200-300    | 16         | 15.84%
300-400    | 8          | 7.92%
400-500    | 6          | 5.94%
500-600    | 3          | 2.97%
600-700    | 3          | 2.97%
700-800    | 2          | 1.98%
800-900    | 1          | 0.99%
900-1000   | 1          | 0.99%
1200-1300  | 1          | 0.99%
1700-1800  | 1          | 0.99%
4800-4900  | 1          | 0.99%

histogram:
<center>
  <div id="ndq100-price-histograms">
    <img chartId="100" src="/media/study-nasdaq-price-structure/ndq100/price_histogram100.png" style="display:none"></img>
    <img chartId="500" src="/media/study-nasdaq-price-structure/ndq100/price_histogram500.png" style="display:none"></img>
    <img chartId="1000" src="/media/study-nasdaq-price-structure/ndq100/price_histogram1000.png" style="display:none"></img>
    <img chartId="1000_2" src="/media/study-nasdaq-price-structure/ndq100/price_histogram1000_2.png" style="display:block"></img>
  </div>
  <script src="/media/study-nasdaq-price-structure/js/ndq100-price-histogram-slider.js"></script>
  <div class="slider-container">
    <div class="value-mc">max: <span id="ndq100-price-histogram-value">1000</span></div>
    <input id="ndq100-price-histogram-slider" type="range" min="0" max="3" step="1" value="3" style="width:70%">
  </div>
</center>


### Market cap
histogram:
<center>
  <div id="ndq100-mc-histograms">
    <img chartId="e10" src="/media/study-nasdaq-price-structure/ndq100/mc_histogram_e10.png" style="display:none"></img>
    <img chartId="e11" src="/media/study-nasdaq-price-structure/ndq100/mc_histogram_e11.png" style="display:none"></img>
    <img chartId="e12" src="/media/study-nasdaq-price-structure/ndq100/mc_histogram_e12.png" style="display:block"></img>
  </div>
  <script src="/media/study-nasdaq-price-structure/js/ndq100-mc-histogram-slider.js"></script>
  <div class="slider-container">
    <div class="value-mc">max: <span id="ndq100-mc-histogram-value">1000</span></div>
    <input id="ndq100-mc-histogram-slider" type="range" min="0" max="2" step="1" value="2" style="width:70%">
  </div>
</center>

boxplot:
<center>
  <div id="ndq100-mc-boxplots">
    <img class="wide" chartId="e10" src="/media/study-nasdaq-price-structure/ndq100/mc_boxplot_e10.png" style="display:none"></img>
    <img class="wide" chartId="e11" src="/media/study-nasdaq-price-structure/ndq100/mc_boxplot_e11.png" style="display:none"></img>
    <img class="wide" chartId="max" src="/media/study-nasdaq-price-structure/ndq100/mc_boxplot_max.png" style="display:block"></img>
  </div>
  <script src="/media/study-nasdaq-price-structure/js/ndq100-mc-boxplot-slider.js"></script>
  <div class="slider-container">
    <div class="value-display">value: <span id="ndq100-mc-boxplot-value">5000</span></div>
    <input id="ndq100-mc-boxplot-slider" type="range" min="0" max="2" step="1" value="2" style="width:70%">
  </div>
</center>


### Sector
In NASDAQ 100, there are 9 sectors. Out of 101 tickers, 45.54% are Technology, 20.79% are Consumer Discretionary, 10% are Healthcare, and so on. The four sectors that are missing in NASDAQ 100 are EMPTY, Basic Materials, Miscellaneous, Real Estate.

<center><img class="wide" src="/media/study-nasdaq-price-structure/ndq100/sector.png"></img></center>

CAT | COUNT | PERC
--- | --: | --:
Technology | 46 | 45.54
Consumer Discretionary | 21 | 20.79
Health Care | 10 | 9.90
Industrials | 7 | 6.93
Consumer Staples | 6 | 5.94
Telecommunications | 5 | 4.95
Utilities | 4 | 3.96
Finance | 1 | 0.99
Energy | 1 | 0.99


## Comparison, conclusion
### Price

<center>
<img class="wide" src="/media/study-nasdaq-price-structure/bplot_price.png"></img>

with stats:

NAME       | COUNT      | MIN        | Q1         | MEDIAN     | Q3         | MAX       
---        | --:        | --:        | --:        | --:        | --:        | --:       
NASDAQ 100 | 101        | 10.46      | 88.61      | 179.13     | 324.65     | 4808.10   
NASDAQ GS  | 1523       | 0.01       | 9.28       | 23.77      | 58.76      | 4802.10   
NASDAQ     | 3926       | 0.00       | 1.76       | 7.64       | 23.99      | 4808.10   

</center>


### Market cap
<center>
<img class="wide" src="/media/study-nasdaq-price-structure/bplot_mc.png"></img>
with stats:

NAME       | COUNT      | MIN        | Q1         | MEDIAN     | Q3         | MAX       
---        | --:        | --:        | --:        | --:        | --:        | --:       
NASDAQ     | 3926       | 0       | 18508029 | 185079552 | 1245612050 | 3662110438210
NASDAQ GS  | 1523       | 0       | 439670271 | 1538142308 | 5701190476 | 3663092966705
NASDAQ 100 | 101        | 18357001914 | 43108691869 | 78586906308 | 150046867582 | 3662110438210

</center>


## Reference
- from [nasdaq.com/market-activity/stocks/screener](https://www.nasdaq.com/market-activity/stocks/screener) obtained on 20250107:
  - [src1_ndq_all.json](/TODO), [original](/media/study-nasdaq-price-structure/nasdaq_screener_1736274256746_nasdaq.csv)
  - [src2_ndq_gs.json](/TODO), [original](/media/study-nasdaq-price-structure/nasdaq_screener_1736274394579_nasdaq_global_select.csv)
  - [src3_ndq_gm.json](/TODO), [original](/media/study-nasdaq-price-structure/nasdaq_screener_1736274429748_nasdaq_global_market.csv)
  - [src4_ndq_cm.json](/TODO), [original](/media/study-nasdaq-price-structure/nasdaq_screener_1736274450764_nasdaq_capital_market.csv)
  - [src5_ndq_adr.json](/TODO), [original](/media/study-nasdaq-price-structure/nasdaq_screener_1736274476733_nasdaq_adr.csv)
- script: `./fintools-ib/fintools_ib/bin/insights/prices_get.py`
- script: `./fintools-ib/fintools_ib/bin/insights/prices_analyse.py`


### Data methods
The CSV files from nasdaq.com stock screener contain fields:
- `Symbol,Name,Last Sale,Net Change,% Change,Market Cap,Country,IPO Year,Volume,Sector,Industry`

They are converted to json with the script: TODO

See snippets/python/plot/*.py

```
# Convert to a list
./csv_extract_field.py nasdaq_screener_1736274256746_nasdaq.csv "Last Sale" > ndq_price.txt
# Get a plot
~/code/snippets/python/plot/plot_histogram.py ./ndq_price.txt 10
```
