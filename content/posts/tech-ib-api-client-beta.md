---
title: "IB: Client API beta"
subtitle: Notes on the new API
date: 2019-08-25
categories: 02 tech
---

Source: https://interactivebrokers.github.io/cpwebapi/  
API: https://ibkrcampus.com/ibkr-api-page/cpapi-v1/#md-snapshot

{{<toc>}}

<!--more-->

## Prerequisites
### Get account and run gateway

Get an account at [Interactive Brokers](https://interactivebrokers.co.uk/). Once approved, you can apply for beta access by raising a support ticket in the Client Area. You will receive a .zip file containing the IB gateway for the beta API.  
Or, get it here: https://download2.interactivebrokers.com/portal/clientportal.beta.gw.zip

Unzip, install prerequisites and run.
```
sudo apt update
sudo apt-get install default-jre
bin/run.sh root/conf.yaml
```
Auth in browser via  
https://localhost:5000/

### API notes
The below HTTP header is required on HTTP queries. You need to add it if making calls via NodeJS `https` module, but is automatically present in `curl` calls.  
```
Accept: */*
```

API base url:
```
https://localhost:5000/v1
```

## Keepalive
Check auth status (call this once a minute)
```
curl -sk https://localhost:5000/v1/portal/iserver/auth/status | jq
```
Example
```
$ curl -sk https://localhost:5000/v1/portal/iserver/auth/status | jq
{
  "authenticated": true,
  "competing": false,
  "connected": true,
  "message": "",
  "fail": ""
}
```
Keepalive (call this once a minute)
```
curl -sk https://localhost:5000/v1/portal/sso/validate | jq
```

## Get info
Get account info
```
curl -sk https://localhost:5000/v1/portal/portfolio/accounts \
  | jq '.'
```
Example (redacted)
```
$ curl -sk https://localhost:5000/v1/portal/portfolio/accounts \
>   | jq '.'
[
  {
    "id": "U0000000",
    "accountId": "U0000000",
    "accountVan": "U0000000",
    "accountTitle": "FirstName LastName",
    "displayName": "FirstName LastName",
    ...
  }
]
```

## What is conid
Tickers (e.g. 'AAPL') have a 'conid' number. First, get the conid for a given ticker, e.g.
```
curl -sk -d '{ "symbol": "AMZN" }' \
  https://localhost:5000/v1/portal/iserver/secdef/search
```
Example
```
$ curl -sk -d '{ "symbol": "AMZN" }'   https://localhost:5000/v1/portal/iserver/secdef/search | jq '.' | head
[
  {
    "conid": 3691937,
    "companyHeader": "AMAZON.COM INC - NASDAQ",
    "companyName": "AMAZON.COM INC",
    "symbol": "AMZN",
    "description": "NASDAQ",
    "fop": null,
    "opt": "20190830; ...",
    "war": "20190723;...",
```
In the above, conid is 369137.

Get conid by name, show only stock tickers.
```
curl -skG \
  --data-urlencode "symbol=AAPL" \
  --data-urlencode "name=false" \
  --data-urlencode "secType=STK" \
  https://localhost:5000/v1/portal/iserver/secdef/search \
  | jq '.'
```

## Get data
Example conid (AMZN NASDAQ): 3691937

Get last week data for AMZN  
```
curl -skG \
  --data-urlencode "conid=3691937" \
  --data-urlencode "period=1w" \
  https://localhost:5000/v1/portal/iserver/marketdata/history\
  | jq '.' | less
```
Example (redacted)
```
$ curl -skG \
  --data-urlencode "conid=3691937" \
  --data-urlencode "period=1w" \
  https://localhost:5000/v1/portal/iserver/marketdata/history \
  | jq '.'
{
  "start": "20190819-00:00:00",
  "mdAvailability": "D",
  "barLength": 123,
  "delay": 123,
  "high": "123123/12312/123",
  "low": "123123/1231/1231",
  "symbol": "AMZN",
  "text": "AMAZON.COM INC",
  "tickNum": null,
  "timePeriod": "1w",
  "data": [
    {
      "o": 000.00,
      "c": ...,
      "h": ...,
      "l": ...,
      "v": ...,
      "t": ...
    },
    ...
  ],
  "points": 123,
  "travelTime": 7
}
```

## Market data snapshot fields
The endpoint: https://localhost:5000/v1/portal/iserver/marketdata/snapshot  
has undocumented fields. These are as below.

Note: If market is closed, some of the fields will keep updating, e.g. percent change will not be the percent update at the last close, but the pecent change (???? since last close, or since last open ????)

```
31 After hours price
55 Symbol
58 
70
71 low
72
73
74
75
76
77
78
82 price change (after hours)
83 price change percent (after hours)
84
85
86
87
88
6004
6008
6070 STK
6072
6073
6119 server_id
6457 (&serviceID1=203&serviceID2=775&serviceID3=108&serviceID4=1042&serviceID5=109&serviceID6=97)
6509 (DPB)
7051 Company name
7094
7219 Symbol
7220
7221 Exchange
7280 Sector/Industry (Biotechnology)
7281 Sector/Industry (Medical-Biomedical/Gene)
7282 (3.03M)
7284 (332.177%)
7285
7286
7287
7288
7289 (9.791M)
7290
7291 (-0.47)
7292
7293 52 week high
7294 52 week low
7295
7296 Previous close (most recent close if market closed)
7633
server_id
conid": 0,
_updated": 0
```
Actually, get these here instead:  
https://gdcdyn.interactivebrokers.com/portal.proxy/v1/portal/swagger/swagger?format=json  
i.e.
```
server_id:
conid:
_updated:
31: Last Price
55: Symbol
58: Text
70: High
71: Low
72: Position
73: Market Value
74: Average Price
75: Unrealized PnL,
76: ?
77: ?
78: ?
82: Change Price
83: Change Percent
84: Bid Price
85: Ask Size
86: Ask Price
87: Volume
88: Bid Size
6004: Exchange
6008: Conid
6070: Security Type
6072: Months
6073: Regular Expiry
6119: ?
6457: Underlying Conid. Use /trsrv/secdef to get more information about the security
6509: Market Data Availability.
      The field may contain two chars.
      The first char is the primary code:
        R = Realtime,
        D = Delayed,
        Z = Frozen
        Y = Frozen Delayed.
      The second char is the secondary code:
        P = Snapshot Available,
        p = Consolidated.
7051: ?
7094: Conid + Exchange
7219: Contract Description
7220: Contract Description
7221: Listing Exchange
7280: Industry
7281: Category
7282: Average Daily Volume
7633: Implied volatility of the option
7284: Historic Volume (30d)
7285: Put/Call Ratio
7286: Dividend Amount
7287: Dividend Yield %
7288: Ex-date of the dividend
7289: Market Cap
7290: P/E
7291: EPS
7292: Cost Basis
7293: 52 Week High
7294: 52 Week Low
7295: Open Price
7296: Close Price
```



After hours, to get the prices at market close, get something like:
```
https://localhost:5000/v1/portal/iserver/marketdata/history?conid=242137345&period=1d&bar=1d
```
giving the the 0.27 price at the market close (`data.c` field)
```
{
    "start": "20191008-12:00:00",
    "mdAvailability": "Z",
    "barLength": 86400,
    "delay": 900,
    "high": "2950/474/0",
    "low": "2700/474/0",
    "symbol": "DRIO",
    "text": "DARIOHEALTH CORP",
    "tickNum": null,
    "timePeriod": "1d",
    "data": [
        {
            "o": 0.29,
            "c": 0.27,
            "h": 0.295,
            "l": 0.27,
            "v": 474,
            "t": 1570536000000
        }
    ],
    "points": 0,
    "travelTime": 7
}
```


## Market scanners
This is about the market scanners documented [here](https://www.interactivebrokers.com/api/doc.html#tag/Scanner). There are two scanners:

- Scanner Run: /iserver/scanner/run
- Run Scanner (beta): /hmds/scanner

Resources

- https://github.com/benofben/interactive-brokers-api/tree/master/JavaClient/src/com/ib/controller
- https://github.com/benofben/interactive-brokers-api/blob/master/JavaClient/src/com/ib/controller/Types.java
- https://groups.io/g/twsapi/topic/82452188

### Beta scanner
url: **/hmds/scanner**

with params:
- instrument: STK
- scanCode: [TOP_PERC_GAIN](https://github.com/benofben/interactive-brokers-api/blob/master/JavaClient/src/com/ib/controller/ScanCode.java)
- secType: STK
- filters: [
  {
    "code": "priceBelow",
    "value": 1,
  },
- locations: STK.US.MAJOR


<!--Reference-->
