---
title: 'Price study: Price change of 46 stocks over a year'
subtitle: 
date: 2023-12-06
categories: 00 studies
---
<script src='/media/price-study/ex1_graph.js'></script>
<script src='/media/price-study/ex1.js'></script>
<script src='/media/price-study/ex1_data.js'></script>
<style>
.wrapper {
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: center;
}
.graph {
  /*width: 100%;*/
}
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  font-size: 0.8em;
}
/*table tr:hover td {
  border: 2px solid black;
}*/
.empty {
  background-color: none;
}
.highlight {
  /*background-color: black;*/
  /*color: white;*/
  border: 2px solid black;
}
.select {
  background-color: silver;
}
</style>

Pick symbols at price at or under $1 on 2023-12-06. There are 46 of them.  
**Question:** How did their price change over last year?

<center>

<div class='wrapper' style='width:100%'>
  <div class='graph'>
    <canvas id='ex1' style='border:1px solid;width:90%'></canvas><br>
  </div>
  <div class='table'>
    <table id='table'>
      <tr><td>ABC</td><td>0.5</td></tr>
    </table>
  </div>
</div>
  date: <span id='date-label'>20231206</span><br>
  <input id='date-range' type='range' style='width: 80%'></input><br>
  <button id='btn-sort'>sort</button>
  <button id='btn-reset'>reset</button>
</center>

## How to generate
Generated with fintools-ib, through:
- `bin/insights/fetch_365_price_less_than_1.py`
- `bin/insights/fetch_365_price_less_than_1_sector.py`

## Notes
This data was originally generated on 20231206. Since then (2023-12-09), the symbol CENN has been delisted.

<center><img src='/media/price-study/grafana1.png' style='width:80%'></img></center>

