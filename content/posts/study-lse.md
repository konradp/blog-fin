---
title: "London Stock Exchange"
subtitle: 
date: 2019-12-22
categories: 00 studies
---

# Overview
Looking at:

- General info
- List of companies/tickers
  - with prices
  - with market cap
  - with sector

# General info
https://en.wikipedia.org/wiki/London_Stock_Exchange

# List of companies/tickers

Where to get a list of companies which trade on London Stock Exchange (LSE)?

Use the Stock Screener on LSE webpage:  
https://londonstockexchange.com  
i.e.  
https://londonstockexchange.com/exchange/companies-and-advisors/companies/stock-screener.html
```
submitFormForCSV();
```

Numbers:

- 2312 securities traded in total
- 883 on AIM market
- 114 on AIM, below £1
- 82 on AIM, below £1, 'Country of incorporation=Great Britain'

Number of companies traded on LSE by sector (accurate as of 2019-12-22).  
**Note:** Used the default filter query on the 'Stock Screener', and so includes companies whose 'Country Of Incorporation' is other than Great Britain, and so for example includes companies incorporated in USA, Guernsey, and others.  
**Note:** Similarly, there is no discrimination regarding the 'Market', e.g. companies are shown which trade on the 'Main Market', 'AIM', 'Main Market -SFS', and others.  
**Question:** E.g. Babcock Intl operates in multiple sectors. Are these duplicates included here?  
**TODO:** Sector by country.

- What is: 'Market=AIM'?
- What if Market is empty, e.g. some Hyundai stocks in 'Automobiles and Parts'?

{{<table sortable>}}
| Sector | Number |
|:------|------:|
| Aerospace and Defense                          | 17 |
| Alternative Energy                             | 15 |
| Automobiles and Parts                          | 10 |
| Banks                                          | 49 |
| Beverages                                      | 14 |
| Chemicals                                      | 21 |
| Closed End Investments                         |  5 |
| Construction and Materials                     | 49 |
| Consumer Services                              |  8 |
| Electricity                                    | 20 |
| Electronic and Electrical Equipment            | 43 |
| Finance and Credit Services                    | 32 |
| Food Producers                                 | 37 |
| Gas, Water and Multi-utilities                 | 13 |
| General Industrials                            | 25 |
| Health Care Providers                          | 17 |
| Household Goods and Home Construction          | 28 |
| Industrial Engineering                         | 16 |
| Industrial Materials                           | 14 |
| Industrial Metals and Mining                   |108 |
| Industrial Support Services                    | 99 |
| Industrial Transportation                      | 35 |
| Investment Banking and Brokerage Services      |116 |
| Leisure Goods                                  | 18 |
| Life Insurance                                 | 12 |
| Media                                          | 60 |
| Medical Equipment and Services                 | 24 |
| Mortgage Real Estate Investment Trusts         |  0 |
| Non-life Insurance                             | 16 |
| Oil, Gas and Coal                              |149 |
| Open End and Miscellaneous Investment Vehicles | 39 |
| Personal Care, Drug and Grocery Stores         | 27 |
| Personal Goods                                 |  7 |
| Pharmaceuticals and Biotechnology              | 71 |
| Precious Metals and Mining                     | 56 |
| Real Estate Investment Trusts                  | 50 |
| Real Estate Investment and Services            | 62 |
| Retailers                                      | 53 |
| Software and Computer Services                 |117 |
| Technology Hardware and Equipment              | 33 |
| Telecommunications Equipment                   | 14 |
| Telecommunications Service Providers           | 28 |
| Tobacco                                        |  2 |
| Travel and Leisure                             | 77 |
| Waste and Disposal Services                    |  3 |
{{</table>}}

Things to look at:
- last day close vs prev. day close <- perc increase/decrease

# AIM

Recall, numbers:

- 2312 securities traded in total on LSE
- 883 on AIM market
- 114 on AIM, below £1
- 82 on AIM, below £1, 'Country of incorporation=Great Britain'

Example trade of an AIM stock via Degiro.

ticker: TRX
order: 2020-01-03 06:36
fill : 2020-01-03 13:24
type : market
value: -0.01
local value; GBX -1.00
exchange rate: 100
fee: 1.75
total: 1.76
price at order: (OHLC)
2020-01-02 16:25-16.29: 0.977
2020-01-03 08:10-08:14: 0.953
price at fill: (OHLC)
2020-01-03 12:55-12:59: 0.970
2020-01-03 13:35-13:39: 0.945
