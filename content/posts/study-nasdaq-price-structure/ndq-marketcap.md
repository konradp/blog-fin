---
title: "NASDAQ price structure: market cap"
---
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="/media/study-nasdaq-price-structure/js/ndq-marketcap.js"></script>


back to the [main post](../)

Market cap in 100 000s

<div>
<canvas id="chart"></canvas>
</div>
