---
title: "NASDAQ price structure: price"
---
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="/media/study-nasdaq-price-structure/js/ndq-price.js"></script>


back to the [main post](../)

<div>
<canvas id="chart"></canvas>
</div>
