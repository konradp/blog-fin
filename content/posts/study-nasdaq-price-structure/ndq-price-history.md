---
title: "NASDAQ price structure: price"
---
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/15.6.1/nouislider.min.css">
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/15.6.1/nouislider.min.js"></script>
<script src="/media/study-nasdaq-price-structure/js/ndq-price-history.js"></script>


back to the [main post](../)

<div>
<canvas id="chart"></canvas>
</div>


<div id="slider"></div>


source: [yahoo](https://query2.finance.yahoo.com/v8/finance/chart/^IXIC?period1=315525600&period2=1737127425&interval=1d), reworked with [extract_closing.py](/media/study-nasdaq-price-structure/ndq-price-history/extract_closing.py) script to extract dates and closing price, i.e. the file [here](/media/study-nasdaq-price-structure/ndq-price-history/IXIC_cleaned.json).
