---
title: "System 1: Trading Friday losers on Monday"
date: 2018-08-20
categories: 00 studies
---
We study symbols with low price and negative day change, and their subsequent performance.
<!--more-->

## Intro and motivation

In this study we focus on cheap NASDAQ stocks/symbols, in particular those priced at-, or under, 10$/share. Furthermore, we only consider the stocks whose day negative change is at least -10% after the close of the day. We will refer to these stocks as **losers**.  
**Note**: stocks at $5/share, or less, are commonly known as **penny stocks**.

We capture a list of *losers* after the close of day 0 (Friday 17 Aug 2018) and then measure their subsequent day change after day 1 close (Monday 20 Aug 2018).
Summarising the data in a table provides an initial look at our data.

<center>

id | symbol | day 0 change (%) | day 1 change (%)
--- | --- | ---: | ---:
1 | SLNOW | -67.65 | 90.91
2 | ONSIW | -54.75 | 99.00
3 | CETXW | -42.51 | 0.00
4 | NUROW | -39.70 | -27.24
5 | BNTCW | -34.75 | 0.00
6 | AMRHW | -28.05 | 0.00
7 | JASNW | -26.47 | 36.00
8 | CHEKW | -19.90 | 74.66
9 | BOXL  | -18.78 | -6.78
... | ... | ... | ...
36 | CJJD | -10 | 1.71
Total |  |  -680.76 | 295.34 

**Table 1:** Percent change on day 0 and 1 of day 0 *losers*
</center>

The change on day 0 can be as negative as -67.65%. On day 1, change varies between positive and negative numbers in no immediately clear pattern, e.g. NUROW with -27.24% value, compared to CHEKW with 74.66% value.

The 'Total' figure in the Table.1 motivates the further study; for suppose we have invested $100 per stock. Then, the day 0 saw 36 stocks satisfying our definition of *losers*, with a total day change of -680.76. In other words, having invested $100 into each stock on on that day have resulted in a 680.76 loss. However, the total change on day 1 is +295.34, i.e. an 8.20% gain on the initial $3,600 (36 stocks at $100/share). If we are able to buy just before the close of Friday day 0; the theoretical Monday day 1 gain of 8.20% justifies further investigation.


## Day 0 percent change distribution
The change range on day 0 ranges between -67.65% and 45.08%. For information about the distribution of this change, we can represent the data on a histogram.

<center>
<img src="/media/2018-08-20/fig1.png"/>
**Fig.1**: Day 0 percent change distribution of all symbols, grouped by share price.
</center>


It is also worth considering a detailed view by narrowing the graph range to [-20,20].

<center>
<img src="/media/2018-08-20/fig2.png"/>
**Fig 2**: Day 0 percent change distribution of all symbols, grouped by share price (detail).
</center>

The distribution of the change (fig1, fig2) show that most stocks cluster in the middle of the graph, around 0% day change. The shape of the graph resembles that of normal distribution. Otherwise, grouping by price offers no additional insight.

The grouping by price range becomes more insightful when considering solely the **losers**, i.e. by 'zooming in' on left-hand-side of the histogram, and comparing against all the other stocks.

<center>
<img src="/media/2018-08-20/fig3.png"/>
**Fig 3**: Symbols with percent change less than -10% on day 0, grouped by price per share
</center>

The negative change graph is mostly dominated by the cheap stocks. There were 40 stocks on day 0 with decrease of 10% or more: our already mentioned 36 cheap stocks, and additional four stocks at price higher than $10/share.

Although the graph of the overall distribution (fig1, fig2) is not too insightful, a closer look at the negative change stocks (< -10%) suggests that the Friday *losers* group is dominated by the cheap stocks.

## Day 1 percent change distribution
The day percent change on day 1 ranges between -X% and Y%, and can also be displayed on a histogram, and compared with day 0.

<center>
<img src="/media/2018-08-20/fig4.png"/>
**Fig.4**: Day 1 percent change distribution of all symbols, grouped by share price (detail).
</center>


The distribution is mostly unchanged between day 0 and day 1, hence the similarity between fig. 2 and fig. 4 histograms. However, representing the same data on a boxplot highlights the differences in the extrema between the two days (minimum, maximum).

<center>

day | min (%) | mean (%) | max (%)
--- | ---: | ---: | ---:
day 0 | -67.65 | 0.11 | 45.08 
day 1 | -44.32 | 0.33 | 99.00

**Table 2:** Percent change of all symbols on day 0 and day 1
</center>

<center>
<img src="/media/2018-08-20/fig5.png"/>
**Fig.5**: Change percent of all symbols on day 0 and day 1.
</center>

The lowest bound (i.e. the most negative change) on day 1 is higher than on day 0. Respectively, the upper bound on day 1 is also higher than on day 0. This suggests that day 1 was overall more 'positive' than day 0.

<center>
<img src="/media/2018-08-20/fig6.png"/>
**Fig.6**: Change percent of all symbols on day 0 and day 1 (detail).
</center>

The fig. 6, which is a detail of fig. 5, shows no dramatic change between the two days, and so we conclude that there was no major move between the two days, except for the extreme values having shifted in the positive direction.

## Losers percent change distribution
Only our symbols (losers)

Although there no dramatic differences between the percent change distribution of day 0 compared to day 1, 

<center>
<img src="/media/2018-08-20/fig7.png"/>
**Fig 7**: Percent day change distribution of losers on day 1 compared to day 0.
</center>

The fig.7 shows that day 1 behaviour is visibly more 'positive' with 11 stocks (30.6%) below zero, and 25 stocks (69.4%) at- or above zero, out of total 36 loser stocks.

A comparison of fig.5 with fig.7 suggests that three out of four stocks with change greater than +50% on day 1 classify as day 0 losers. This is indeed the case, as summarised in Table 3 below.

<center>

Day 1 winner | Day 1 chg percent | Day-0-price | Is day 0 loser
--- | ---: | ---: | ---:
ONSIW | 99 | 0.02 | yes
SLNOW | 90.91 | 0.11 | yes
CHEKW | 74.66 | 0.08 | yes
SSC | 53.3 | 1.97 | -
JASNW | 36 | 0.01 | yes
GRIN | 34.61 | 8.9 | -
GLG | 30.51 | 0.59 | -
ADXSW | 30 | 0.05 | yes
ALRN | 30 | 2.00 | -
BCACR | 22.5 | 0.40 | -

**Table 3:** Day 1 top ten winners against day 0 losers.
</center>

We also see that half of the top ten day 1 belong to the group of day 0 losers; and all ten winner symbols satisfy our definition of *cheap* (less than $10/share).

## Conclusion
Is simply betting on Friday losers a profitable Monday strategy? At a glance, the **Total** row of Table 1 suggests so, and is further backed by the percent change distribution of losers on day 0 and day 1 (Fig. 7). We are not able to extend this statement beyond the dates of 17th-20th August 2018 as further questions have not yet been addressed.

Firstly, we did not account for the broker fees, nor was this strategy back-tested against historic resuls. It could very well be that pattern observed in Fig. 7 does not occur often enough to constitute an edge.

Secondly, the data for certain stocks appears to be missing, or numbers are in places equal to zero, which raise suspicion as to the accuracy of the data gathered. No attempt was made to validate our API data source against other sources, e.g. the historical NASDAQ prices.

Most importantly, there is no certainty that the closing prices on which we base our calculations are attainable in practice. In other words, we do not know if the orders placed would have been 'filled' at the expected prices. No investigation was made into which type of orders should be used with our strategy, e.g. market order or limit order. To address these questions, a practical exercise would be required.


Our strategy can be extended and studied further in various ways. For instance, we have only considered only stocks with price less than $10/share. Other price groups or ranges could be considered, e.g. the penny stocks at $5/share, only the extremely cheap stocks of less than $1/share, or the stocks with price bounded by these two numbers.
The strategy can be investigated against other days of the week, e.g. by investigating the performance of Monday losers on Tuesday.
Also, the ways of reducing the number of the day 1 negative stocks could also be investigated in a follow-up study, possibly by looking at the price and volume charts of the stocks involved.

## Appendix: Data
To reproduce the graphs and tables for this article, download the below files to a single directory.

- [R script: main.r](/media/2018-08-20/main.r)
- [Day 0 data: day0.csv](/media/2018-08-20/day0.csv)
- [Day 1 data: day1.csv](/media/2018-08-20/day0.csv)

Then, run the script from within an R shell.
```
$ R
> source("main.r", print.eval=TRUE)
```

In that same dir, obtain the graph data.
```
curl -o chart.json \
"https://api.iextrading.com/1.0/stock/market/batch?symbols=SLNOW,ONSIW,CETXW,NUROW,BNTCW,AMRHW,JASNW,CHEKW,BOXL,IAMXW,GLACW,ADXSW,DFBHW,CTXRW,DLPNW,USEG,RSLS,SNOAW,ARLZ,ADMP,ATISW,AMRS,EASTW,DOTAW,ATACR,ALPN,SNOA,PTIE,ADOM,GRNQ,JTPY,CYCC,RMNI,JMU,CLRBZ,CJJD&types=chart&range=1y"

jq . chart.json > chart2.json
mv chart2.json chart.json
```

## Appendix: Full table

id | symbol | day 0 change (%) | day 1 change (%)
--- | --- | --- | ---
1 | SLNOW | -67.65 | 90.91
2 | ONSIW | -54.75 | 99
3 | CETXW | -42.51 | 0
4 | NUROW | -39.7 | -27.24
5 | BNTCW | -34.75 | 0
6 | AMRHW | -28.05 | 0
7 | JASNW | -26.47 | 36
8 | CHEKW | -19.9 | 74.66
9 | BOXL | -18.78 | -6.78
10 | IAMXW | -17.41 | 12.5
11 | GLACW | -17.39 | -10.47
12 | ADXSW | -16.67 | 30
13 | DFBHW | -16.67 | 0
14 | CTXRW | -16 | 0
15 | DLPNW | -15.97 | 0
16 | USEG | -15.69 | 0
17 | RSLS | -15.19 | 11.94
18 | SNOAW | -14.29 | 0
19 | ARLZ | -13.33 | -19.58
20 | ADMP | -13.27 | -4.08
21 | ATISW | -12.64 | 0
22 | AMRS | -12.6 | 3.51
23 | EASTW | -12.1 | -8.57
24 | DOTAW | -11.36 | 0
25 | ATACR | -11.2 | -2.7
26 | ALPN | -11.08 | -1.73
27 | SNOA | -11.05 | 5.65
28 | PTIE | -11.01 | -1.81
29 | ADOM | -10.8 | 5.01
30 | GRNQ | -10.66 | 10.1
31 | JTPY | -10.53 | 2.94
32 | CYCC | -10.49 | 3.45
33 | RMNI | -10.41 | -6.64
34 | JMU | -10.38 | -2.44
35 | CLRBZ | -10.01 | 0
36 | CJJD | -10 | 1.71
Total |  |  -680.76 | 295.34 

