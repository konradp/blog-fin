---
title: 'System 2: NASDAQ 100 MA crossovers'
subtitle: 
date: 2024-10-26
categories: 00 studies
---
{{<toc>}}
<script src="https://konradp.gitlab.io/finplotjs/js/finplot.js"></script>
<script src="/media/study-system2/charts.js"></script>
<link rel="stylesheet" href="/media/study-system2/style.css">

## Overview
Here we study moving average crossovers.

## 2024.04.08
Data file: <a href="/media/study-system2/20240408.json">20240408.json</a>
shows stocks which increased 3% or more:

date     | winnerCount | stocks
---      | ---         | ---
20240408 | 1           | ARM 3.09
20240409 | 0           |
20240410 | 0           |
20240411 | 3           | AVGO 3.03<br>AAPL 3.09<br>GFS 3.00
20240412 | 0           |

So we will focus on the two days:
- 20240408 MON
- 20240411 THU

NASDAQ 100 index prices, daily, YTD
<center>
  <img src="/media/study-system2/nasdaq100.png" width="80%"></img>
</center>

### 20240408: ARM
3 months daily chart, from 2024-01-02, to 2024-04-08.
Plotting MA crossovers for different period lengths. There are 20 bullish crossovers, and 58 bearish MA crossovers. A bullish cross-over is when the short period MA crosses above the long period MA.

<center><div class="charts" symbol="arm"></div></center>


### 20240411: AVGO, AAPL, GFS
AVGO
<center><div class="charts" symbol="avgo"></div></center>

AAPL
<center><div class="charts" symbol="aapl"></div></center>

GFS
<center><div class="charts" symbol="gfs"></div></center>
