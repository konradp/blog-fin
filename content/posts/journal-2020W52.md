---
title: "2020 w52: discretionary trade each day"
subtitle: 
date: 2020-12-23
categories: 08 journal
---

# Objectives

Use previous week findings

Use lt/3 (453 symbols), focus on software industry (33 symbols).
```
./get_lt_for_industry.py Software
```

# Notes
REPORT  
Download day data for software stocks (28 symbols).
Note: Run after market close, before market open.
```
cd cron
./download-day-industry.py Software
```
Run report
```
cd bin
./report_industry.py Software
```
and get table
```
Legend:
- SYMB: symbol
- P: price
- V1: vol_len
- V2: vol_non_zero_len
- V3: vol_avg
- V4: vol_median
- V5: vol_median_non_zero
- vm: vol_min
- VM: vol_max

SYMB       P    V1    V2       V3      V4      V5     vm        VM
------  ----  ----  ----  -------  ------  ------  -----  --------
IDEX    2.14   390   390  1367.94  656.00  656.00  44.00  10578.00
VISL    1.67   390   390   513.01  240.00  240.00   4.00   6071.00
GNUS    1.57   390   390   489.51  191.00  191.00   3.00   4448.00
PHUN    1.01   390   383   164.79   83.00   83.00   1.00   2170.00
XELA    0.40   390   367   143.35   49.00   54.00   1.00   3220.00
BRQS    1.07   390   289   114.81   15.00   31.00   1.00   2280.00
QTT     1.90   390   372    87.67   29.50   33.00   1.00   2113.00
VERB    1.85   390   360    76.88   30.00   36.50   1.00   1004.00
BOXL    1.75   390   353    59.31   18.00   22.00   1.00   1550.00
LKCO    0.58   390   306    51.17    7.00   14.00   1.00   2031.00
INPX    1.02   390   345    36.31   11.00   14.00   1.00   1190.00
CYRN    1.09   390   229    26.81    3.00   14.00   1.00   1073.00
CNET    1.28   390   239    24.18    3.00   13.00   1.00    641.00
MYSZ    1.28   390   236    23.14    2.00   12.00   1.00    438.00
MKD     0.86   390   136    11.12    0.00   10.00   1.00    444.00
EVOL    2.23   390   100    10.25    0.00   12.00   1.00    621.00
BHAT    0.84   390   157     9.08    0.00    9.00   1.00    332.00
SEAC    0.79   390   339     7.34    1.00    2.00   1.00    274.00
PIXY    2.36   390   190     5.34    0.00    4.00   1.00    105.00
GVP     1.40   390   114     4.91    0.00    3.00   1.00    145.00
LYL     2.35   390    69     2.91    0.00    5.00   1.00    169.00
ANY     1.61   390    85     2.57    0.00    6.00   1.00    101.00
GSUM    1.62   390    20     1.01    0.00    5.00   1.00    163.00
STRM    1.63   390    24     0.87    0.00    7.50   1.00     74.00
MNDO    2.59   390    42     0.76    0.00    5.00   1.00     48.00
PBTS    2.20   370    19     0.55    0.00    3.00   1.00     92.00
BSQR    1.30   364    26     0.48    0.00    2.50   1.00     39.00
BOSC    2.68   390    31     0.20    0.00    2.00   1.00     15.00
```
showing 3 stocks to watch (see V5 column), i.e.

- GNUS
- IDEX
- VISL

aim:
- buy price 2% below close price
- 4% increase

Legend:

- cp: close price
- bp: buy price
- sp: sell price
- xbp: actual buy price
- xsp: actual sell price
- f?: order filled?

SYMB | cp   | bp     | sp
---  | ---  | ---    | ---
GNUS | 1.57 | 1.5386 | 1.600144
IDEX | 2.14 | 2.1172 | 2.201888
VISL | 1.67 | 1.6366 | 1.702064

SYMB | cp   | xbp  | f?  | xsp  | f?
---  | ---  | ---  | --- | ---  | ---
GNUS | 1.57 | 1.53 | YES | 1.60 | ?
IDEX | 2.14 | 2.11 | XXX |  XXX | ?
VISL | 1.67 | 1.63 | YES | 1.70 | ?

IDEX fail to buy
another order for
buy:  2.18
sell: 2.27

Calculations:

- PERC: ((sellprice-buyprice)/buyprice)*100
- bp: cp-(0.02*cp) = 0.98*cp
- sp: bp+(0.04*bp) = 1.04*b

# Buy
![](/media/trade-journal-2020W52/IDEXxbp1.png)
![](/media/trade-journal-2020W52/GNUSxbp1.png)
![](/media/trade-journal-2020W52/VISLxbp1.png)


# Conclusions
??

# Notes

e.g. get min price for PHUN
```
/opt/fintools-ib/data/day# jq '.[].l' PHUN.json | sort | uniq | sort -n | head -n1
1.05
```
or in a loop
```
for I in GNUS IDEX PHUN XELA; do echo $I; jq '.[].h' $I.json | sort | uniq | sort -n | head -n1; done
```
