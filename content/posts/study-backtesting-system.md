---
title: "Backtesting system"
subtitle: 
date: 2025-01-24
categories: 09 wip
---
Table of contents:
{{<toc>}}

TODO:
- implement some basic indicators
- run the strategies for various combinations of parameters




## Gathering data
The data is gathered with the below script.
```
./backtest/data_download.py
```
it generates 67 MB for data from 1990-01-01 to 2024-12-31. The data comes from yahoo as per [blog: Data sources and facts](/posts/tech-data) and it gives the prices for **current** NASDAQ 100 constituents.

**Note:** As the historical data gathered is for the **current** NASDAQ 100 constituents, the strategies developed from this data will inherently suffer from a **survivor bias**.

## System structure
The two main classes are **Runner** and **Strategy**.

The **Runner** class is used in **run.py** like this to load the **Strategy** called **strategy1**:
```
from lib.runner import Runner
runner = Runner()
runner.load_strategy('strategy1')
runner.run()
```

The **strategy1** is defined in **strategy1.py** as a class named **Strategy1**, and it inherits from **Strategy** parent class:
```
class Strategy1(Strategy):
  def __init__(self):
    super().__init__()
```

Each strategy needs an **is_buy()** method, i.e.:
```
  def is_buy(self, symbol, date):
```
It returns true or false on whether the symbol satisfies the strategy's condition for a buy on a given day or not.<br>
**TODO:** This should be **get_buy()** to get the actual strategy pick.

The **Runner** runs the strategy for each day, and 'buys' the symbols on given days.

TODO: What about sell?
- In **Runner**, a var called **holding** which will have symbols and their current holding duration in days
- The runner gets the buy candidates with **is_buy()**
- Or should it be within the Strategy1 implementation? e.g. if Strategy1 picks symbols at random from candidates, or the first one
- **is_sell()** method?
- want to accomodate: buy and sell on the same day, also swing
- The **holding** var should be passed to the **Strategy** instance, and evaluated within the strategy implementation.
- The strategy should also have some settings/instructions for the **Runner**, e.g. how much to risk, how long to hold for, etc
- TODO: Make the **is_buy()** insted of about getting candidates, make it about getting the actual pick for the day


## Indicators
The backtesting system includes indicators in `backtest/lib/indicator/` dir, e.g. `moving_average.py`:

TODO
