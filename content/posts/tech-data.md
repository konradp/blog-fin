---
title: Data sources and facts
date: 2023-11-25
categories: 02 tech
---

Financial data, e.g. where to download free end-of-day stock prices for NASDAQ.

{{<toc>}}

TODO:
- https://stooq.com/q/d/?s=%5endx <- NDQ100 prices

## Stock list
- etoro: https://api-portal.etoro.com/  
- etoro: https://www.etoro.com/discover/markets/stocks/exchange/nasdaq  
  875 on NASDAQ
- etoro: able to get from IB: 825 or 823
- etoro: 52 stocks <= $1
- API: https://github.com/evgenyzorin/Financials/blob/main/nasdaq-api-v3.0.2.ipynb
- https://wolfpaulus.com/pandas-streamlit-cicd


## yahoo
notes:
- 60 days for 5m charts
- 8 days for 1m charts

urls:
- ratings: https://query1.finance.yahoo.com/v2/ratings/top/AAPL?exclude_noncurrent=true&lang=en-US&region=US&crumb=9AuU.hkwAgH
- finance/chart: https://query1.finance.yahoo.com/v8/finance/chart/AAPL?period1=1643673600&period2=1645315200&interval=&events=div
- finance/chart: 5min chart: https://query2.finance.yahoo.com/v8/finance/chart/AAPL?period1=1727980800&period2=1728975600&interval=5m&includePrePost=true&events=div%7Csplit%7Cearn&useYfid=true&lang=en-US&region=US  
  note: only a range of 5 days or so available at a time
- finance/quote: https://query1.finance.yahoo.com/v7/finance/quote?&symbols=AAPL&fields=currency,fromCurrency,toCurrency,exchangeTimezoneName,exchangeTimezoneShortName,gmtOffSetMilliseconds,regularMarketChange,regularMarketChangePercent,regularMarketPrice,regularMarketTime,preMarketTime,postMarketTime,extendedMarketTime&crumb=9AuU.hkwAgH&formatted=false&region=US&lang=en-US
- finance/spark: https://query1.finance.yahoo.com/v7/finance/spark?includePrePost=false&includeTimestamps=false&indicators=close&interval=5m&range=1d&symbols=ES%3DF%2CYM%3DF%2CNQ%3DF%2CRTY%3DF&lang=en-US&region=US
- ws/fundamentals-timeseries: https://query2.finance.yahoo.com/ws/fundamentals-timeseries/v1/finance/timeseries/AAPL?lang=en-US&region=US&padTimeSeries=true&type=earningsReleaseEvents%2CanalystRatings%2CeconomicEvents&period1=1735865940&period2=1736240400
- ws/insights: https://query1.finance.yahoo.com/ws/insights/v3/finance/insights?disableRelatedReports=true&formatted=true&getAllResearchReports=true&reportsCount=4&ssl=true&symbols=AAPL&lang=en-US&region=US  
  has sector
- ws/screeners/calendar-events: : https://query1.finance.yahoo.com/ws/screeners/v1/finance/calendar-events?countPerDay=25&economicEventsHighImportanceOnly=true&economicEventsRegionFilter=&endDate=1736175600000&modules=economicEvents&startDate=1736089200000&lang=en-US&region=US
- ws/screeners/calendar-events: https://query1.finance.yahoo.com/ws/screeners/v1/finance/calendar-events?countPerDay=100&economicEventsHighImportanceOnly=true&economicEventsRegionFilter=&endDate=1737385200000&modules=economicEvents&startDate=1735484400000&lang=en-US&region=US

## wikidata
- sparql: https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service
You can use SPARQL to query data from wikidata
- https://stackoverflow.com/questions/48568094/wikidatasparql-lookup-a-company-based-on-its-ticker-symbol
- query.wikidata.org
- https://stackoverflow.com/questions/54536076/wikidatasparql-get-tickers-of-all-companies-listed-on-stock-exchanges
- https://en.wikipedia.org/wiki/Wikidata
- https://www.wikidata.org/wiki/Wikidata:Tools/For_programmers
- python:
  - https://doc.wikimedia.org/pywikibot/stable/
  - sparqlwrapper
    - https://people.wikimedia.org/~bearloga/notes/wdqs-python.html
    - https://sparqlwrapper.readthedocs.io/en/stable/
    - example: https://stackoverflow.com/questions/56728845/why-the-results-are-diffrent-in-sparqlwrapper-and-wikidata-query-editor-in-sparq


## nasdaq.com
- https://www.nasdaq.com/market-activity/stocks/screener  
  csv export, has all the Industry/Sector data,  
  6969 stocks (includes NASDAQ, NYSE, AMEX)  
  fields:  
  `Symbol,Name,Last Sale,Net Change,% Change,Market Cap,Country,IPO Year,Volume,Sector,Industry`
- ftp://ftp.nasdaqtrader.com/symboldirectory/nasdaqlisted.txt  
  [blog-fin: Fetching NASDAQ stock symbols python](/posts/other-fetch-nasdaq-stocks/)  
  3481 common stock, only tickers and names, no price
- https://api.nasdaq.com/api/quote/AAPL/historical?assetclass=stocks&fromdate=2018-11-25&limit=9999&todate=2023-11-25&random=15
- https://api.nasdaq.com/api/quote/AAPL/summary?assetclass=stocks  
  has industry and sector
- https://api.nasdaq.com/api/quote/AAPL/chart?assetclass=stocks&timeframe=1d
- https://api.nasdaq.com/api/quote/AAPL/dividends?assetclass=stocks
- https://api.nasdaq.com/api/screener/stocks
- https://api.nasdaq.com/api/screener/stocks?sector=Technology
- https://api.nasdaq.com/api/screener/stocks?tableonly=true&limit=25&offset=0&download=true
- https://api.nasdaq.com/api/screener/stocks?tableonly=false&limit=25&recommendation=strong_buy
- https://api.nasdaq.com/api/screener/stocks?tableonly=false&limit=25&offset=25&recommendation=strong_buy
- NASDAQ 100 constituents: https://api.nasdaq.com/api/quote/list-type/nasdaq100

## Old notes
- https://www.eoddata.com/download.aspx: This is now paid, no longer free
