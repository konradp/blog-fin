---
title: 'Gain perc: percent required to break even'
subtitle: 
date: 2023-06-16
categories: 03 other
math:
  enable: true
---
<script src='/media/other-gain-perc/chart.js'></script>
{{<toc>}}

## Formula
We have

$$
\begin{equation*}
  y = { x \over 1 - {x \over 100} }
\end{equation*}
$$

where:
- $x$: percent decrease
- $y$: required perc increase to return back to original amount

## Derivation

First, decrease $a$ by $x$ percent to get $b$. Then, increase $b$ by $y$ percent, to get back to $a$. Find $y$.

$$
\begin{align} a - {xa \over 100} &= b \\\\ 
  b + {yb \over 100} &= a \end{align}
$$

Substitute (1) into (2), and solve for $y$ to get:

$$
\begin{align*} &a - {xa \over 100} + { y(a - {ax \over 100}) \over 100 }         &= a \\\\ 
  \Rightarrow \quad &1 - {x \over 100} + {y(1 - {x \over 100}) \over 100} &= 1 \\\\ 
  \Rightarrow \quad &x = y(1 - {x \over 100}) \\\\ 
  \Rightarrow \quad &y = {x \over 1 - {x \over 100}} \end{align*}
$$

which looks like this:
<center>
<img src='/media/other-gain-perc/graph.png'></img>
</center>

## Interpretation
If we decrease by 50% ($x$), we clearly need to raise by 100% ($y$) to break even. But the relationship is not linear.

<center>
<div id='chart-main'>
  <canvas id='chart' style='border:1px solid;'></canvas>
  <div id='controls'>
    <div id='x'>
    x:
    <input id='range' type='range' min='0' max='50' value='20'></input>
    <input id='xInput' type='number' min='0' max='50' value='20'></input>
    </div>
    y: <span id='y'>25.00%</span>
  </div>
</div>
</center>
