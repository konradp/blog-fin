---
title: "NASDAQ 100: add/remove effects on price"
subtitle: 
date: 2024-12-18
categories: 09 wip
---
WIP  
Does adding/removing stocks from NASDAQ 100 index affect stock price?
{{<toc>}}

## Notes/TODO
- [wiki: changes](https://en.wikipedia.org/wiki/Nasdaq-100#Changes_in_2024)
- check urls like https://ir.nasdaq.com/node/108351/pdf where the number increases. Consider polling automatically: https://ir.nasdaq.com/node
- another case study, or two
- study: nasdaq100 constituent rules: better to base signals on when stocks become eligible for addition/removal?

## Conclusions
- NASDAQ 100 adds/removes do not have much effect on price
- better to base signals on when stocks become eligible for addition/removal?
- sometimes 8-K will contain earnings instead of 10-Q. These can move the price
- 10-Q can move the price, it can gap down

## Case study: 2024.03.18 Linde addition
ticker: LIN

timeline:
- `2024-03-08 08:47 UTC`: [news: ir.nasdaq.com](https://ir.nasdaq.com/news-releases/news-release-details/linde-plc-join-nasdaq-100-indexr-beginning-march-18-2024)
- `2024-03-08 20:00 ET `: [news: globenewswire.com](https://www.globenewswire.com/news-release/2024/03/09/2843370/6948/en/Linde-plc-to-Join-the-Nasdaq-100-Index-Beginning-March-18-2024.html)
- `2024-03-18 ??:?? ???`: added to NASDAQ 100

The chart below shows daily price of LIN from 2024-01-01. There is a 13.19% bullish run until 2024-03-08. The two vertical dotted lines are for 2024-03-08 and 2024-03-18.
<center>
<img style="width:40%" src="/media/study-ndq100-add-remove/lin.png"></img>
</center>

Price vs. earnings for year 2024 (10-Q quarterly report SEC filings) show the effect of the earnings release: mostly causes the stock to go down, observe the gaps.
<center>
<img style="width:40%" src="/media/study-ndq100-add-remove/lin_earnings_2024.png"></img>
</center>

What is this bump on 2024-02-05?
<center>
<img style="width:40%" src="/media/study-ndq100-add-remove/lin_price_2024_bump.png"></img>
</center>



And what caused the bullish run starting in 2024-02-01, culminating in 2024-03-18? Looking at SEC filings:

- **2023-12-06 8-K**
  - 365-day revolving credit agreement with Bank of America for $1,500,000,000
- **2024-02-06 8-K**
  - press release for quarterly earnings <- shouldn't this be 10-Q instead?
- 2024-02-13 SC 13G/A
  - BlackRock owns 7.2% stake, event at 2023-12-31
- 2024-02-13 SC 13G/A
  - Vanguard owns 9.13% stake, event at 2023-12-29
- 2024-02-14 8-K
  - issued stock on Luxembourg Stock Exchange: EUR 2,238 million? EUR 700 million worth of notes maturing at 3% in 2028, and other notes
- 2024-02-28 8-K
  - new audit/sustainability director

So that dump on 2024-02-05 could have been due to earnings release on 2024-02-06. Would have been a good time to buy, at least 3.73% increase the next day.

2023 price
<center>
<img style="width:40%" src="/media/study-ndq100-add-remove/lin_price_2023.png"></img>
</center>

What is this anomaly bump on 2023-12-12?
<center>
<img style="width:40%" src="/media/study-ndq100-add-remove/lin_price_2023_bump.png"></img>
</center>

Similar on 2023-01-05:
<center>
<img style="width:40%" src="/media/study-ndq100-add-remove/lin_price_2023_bump2.png"></img>
</center>


TODO: Review the below


The 2024-02-06 is when it gapped up. The 2024-02-15 is when it started trending up until 2024-03-14 with a 10.56% increase between these two dates. The reason for the bullish run is the earnings release on 2024-02-06 (concealed in an 8-K release), and the Bank of America loan. Then, BlackRock and Vanguard acquired significant stakes

## Conclusions
- Follow BlackRock and Vanguard
