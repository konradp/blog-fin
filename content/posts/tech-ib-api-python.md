---
title: "IB: native Python API (TWS API)"
subtitle: Notes on the Python API for Interactive Brokers
date: 2019-07-27
categories: 02 tech
---

Shortened version of [another blog article](https://qoppac.blogspot.com/2017/03/interactive-brokers-native-python-api.html)  
**Note:** This is the TWS API: [API manual](https://interactivebrokers.github.io/tws-api/introduction.html#gsc.tab=0)

<!--more-->

# Get account

Get an account at [Interactive Brokers](https://interactivebrokers.co.uk/). No need to wait for the account approval, it will give an username which will allow paper trading (although scanners won't work)

# Get IB gateway
Search online for "interactivebrokers ib gateway download" or get it from https://www.interactivebrokers.com/en/index.php?f=16457

**Note:** View the page full screen, otherwise it will think you are on a mobile device, and will not show the download button.

# Configure gateway
- Launch gateway
- Click Configure -> settings -> API -> Settings

Use settings as below.
```
Read only API: false
Allow connections from localhost only: true
Trusted IPs: 127.0.0.1
```

# Get API
Get API at http://interactivebrokers.github.io/

Install Python libs.
```
/IBJts/source/pythonclient $ python3 setup.py install
```
See docs at https://interactivebrokers.github.io/tws-api/introduction.html

# Run program
https://gist.github.com/robcarver17/7b4a8e2f1fbfd70b4aaea5d205cb35eb

```
wget https://gist.githubusercontent.com/robcarver17/7b4a8e2f1fbfd70b4aaea5d205cb35eb/raw/a9b3d61f5660ae76aca7c4eec14024356f6ed129/IBAPIpythonexample1.py
python3 IBAPIpythonexample1.py
```

# About program
- client: Send requests
- wrapper: Process response



<!--Reference-->
