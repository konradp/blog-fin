---
title: "2020 w50: discretionary trade each day"
subtitle: 
date: 2020-12-06
categories: 08 journal
---

# Objectives
The objective this week is to:

- perform a trade each day

For each day of the week:

- buy and sell within a day
- record motivation and findings

**Note:** Use previous week findings

Use lt/3 (453 symbols), focus on software industry (33 symbols).
```
./get_lt_for_industry.py Software
```
<hr>

REPORT  
Download day data for software stocks (e.g. 29 of them).  
Note: Run once per day, after market close.
```
cd cron
./download-day-industry.py Software
```

Run report
```
cd bin
./report_industry.py Software
```

and get table
```
Legend:
- SYMB: symbol
- P: price
- V1: vol_len
- V2: vol_non_zero_len
- V3: vol_avg
- V4: vol_median
- V5: vol_median_non_zero
- vm: vol_min
- VM: vol_max

SYMB       P    V1    V2       V3      V4      V5     vm        VM
------  ----  ----  ----  -------  ------  ------  -----  --------
BSQR    1.33   390    47     0.98    0.00    5.00   1.00     69.00
SEAC    0.79   390   162     6.92    0.00    3.00   1.00    737.00
CYRN    1.00   390   141   215.59    0.00   70.00   1.00   6592.00
PIXY    2.43   390   165     4.99    0.00    3.00   1.00    176.00
LYL     2.77   390    66     2.77    0.00    5.00   1.00    140.00
PBTS    2.18   390    28     0.90    0.00    8.00   1.00     40.00
PHUN    0.97   390   390   214.77  100.50  100.50   1.00   3299.00
VERB    1.53   390   354    66.38   26.00   30.00   1.00   1767.00
QTT     2.50   390   374    80.55   20.00   21.50   1.00   3821.00
MNDO    2.60   390    52     1.24    0.00    4.00   1.00     57.00
IDEX    1.86   390   390  1048.13  622.00  622.00  22.00   6694.00
BOSC    2.63   348     8     0.32    0.00   12.00   1.00     29.00
MYSZ    1.13   390   130    23.89    0.00   30.50   1.00    697.00
STRM    1.62   390    33     0.51    0.00    2.00   1.00     71.00
INPX    1.03   390   376   103.07   27.00   29.00   1.00   2231.00
ANY     1.49   390    95     2.91    0.00    4.00   1.00    133.00
GNUS    1.69   390   390   557.67  265.50  265.50  10.00   4410.00
BRQS    1.00   390   304    97.92   16.50   32.00   1.00   1870.00
EVOL    2.21   390   152     5.56    0.00    5.50   1.00    104.00
MKD     0.94   390   121    11.71    0.00   11.00   1.00    498.00
VISL    1.21   390   357   122.57   30.00   36.00   1.00   1769.00
GVP     1.31   390    50     1.55    0.00    4.00   1.00    113.00
GSUM    1.65   390    45     1.36    0.00    5.00   1.00     81.00
BHAT    0.91   390   201     7.98    1.00    5.00   1.00    404.00
LKCO    0.57   390   257    23.24    2.00    5.00   1.00   1092.00
NCTY    2.94   390   341    29.71   10.00   14.00   1.00    296.00
CNET    1.36   390   253    43.35    6.00   26.00   1.00    908.00
BOXL    1.70   390   335    72.56   11.00   16.00   1.00   2629.00
XELA    0.36   390   390  2352.90  834.00  834.00  33.00  27095.00
```
showing 4 stocks to watch (see V5 column), i.e.

- GNUS
- IDEX
- PHUN
- XELA

On the next day (2020.12.21, Monday), after market close, we see:

![](/media/trade-journal-2020W50/20201221_GNUS.jpg)
![](/media/trade-journal-2020W50/20201221_IDEX.jpg)
![](/media/trade-journal-2020W50/20201221_PHUN.jpg)
![](/media/trade-journal-2020W50/20201221_XELA.jpg)

This means that GNUS was our best bet, possibly IDEX too. Recall, we are aiming for 4% profit.

Visually inspecting these for best profit:

stock | buytime | selltime | buyprice | sellprice | perc
---   | ---     | ---      | ---      | ---       | ---:
GNUS  | 10:10   | 11:46    | 1.5700   | 1.6400    |  4.46
IDEX  | 10:24   | 14:26    | 1.9100   | 2.0900    |  9.42
PHUN  | 09:30   | 12:12    | 0.9651   | 1.1199    | 16.00
XELA  | 10:54   | 14:52    | 0.4102   | 0.4400    |  7.26

How to calculate perc:

- ((sellprice-buyprice)/buyprice)*100

Interestingly, and perhaps contrary to intuition, all stocks had a best-case-scenario trade of +4% profit on the next day, subject to correct buy/sell time.

Now, looking at previous day price, pevious day min/max pice, and 5 day trends of each stock, can we obtain any further info?

e.g. get min price for PHUN
```
/opt/fintools-ib/data/day# jq '.[].l' PHUN.json | sort | uniq | sort -n | head -n1
1.05
```
or in a loop
```
for I in GNUS IDEX PHUN XELA; do echo $I; jq '.[].h' $I.json | sort | uniq | sort -n | head -n1; done
```

giving

stock | minprice | maxprice | buyprice | sellprice | prevclose
---   | ---:     | ---      | ---      | ---       | ---
GNUS  | 1.55     | 1.5700   | 1.5700   | 1.6400    | 1.5500
IDEX  | 1.66     | 1.6800   | 1.9100   | 2.0900    | 1.6700
PHUN  | 1.05     | 1.0600   | 0.9100   | 1.1199    | 1.0500
XELA  | 0.44     | 0.4407   | 0.4102   | 0.4400    | 0.4402

I see nothing obvious here:

- GNUS: buyprice the same as maxprice of prev. day
- IDEX: buyprice way higher than maxprice of prev. day
- PHUN: buyprice way lower than minprice of prev. day
- XELA: buyprice way lower than minprice of prev. day

So we look at a 5 day trend for more info.  
But noting significant there.  

TODO:

- Revisit
- Look at close prices
- Look at averages/means over previous days

# Conclusions
??

# Notes
TODO:

- get symbols from given industry, price less than
- look at charts (include here)
- decide entry strategy (price, order type) <- which gets filled? start with market order on first day
- exit strategy: aim for percentage? 4%?


Completed:

- `(x)` get symbols from given industry, price less than
- `(x)` look at charts (include here)
- `(x)` decide entry strategy (price, order type) <- which gets filled? start with market order on first day
- `(x)` exit strategy: aim for percentage? 4%?
