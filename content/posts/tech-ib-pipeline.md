---
title: "IB: GitLab CI/CD for cpwebapi python SDK"
date: 2023-03-06
categories: 02 tech
---

In my [earlier blog post](https://konradp.gitlab.io/blog/post/fin-interactive-brokers-web-api-client-swagger/), I used **swagger-codegen** to build a python SDK for [Interactive Brokers Client Portal Web API](https://www.interactivebrokers.com/api/doc.html). Now, I automate these steps with a GitLab pipeline, which additionally uploads the SDK to PyPI.

repo: https://gitlab.com/konradp/ib_web_api  
PyPI package: https://pypi.org/project/ib-web-api/

**TABLE OF CONTENTS**
{{<toc>}}

## Plan
I expect to have a few pipeline stages:

- prep:
    - Docker img: python
    - fix bugs in swagger.json (spec)
    - skip further stages if no change to spec
- generate_sdk:
    - Docker mg: java
    - **swagger-codegen**: build client SDK
- test:
    - Docker img: python
    - python: **import ib_web_api** to check for errors
- pkg:
    - use 'wheel' and 'twine' to generate a **.whl** python pkg
    - test the pkg somehow? in another stage?
- deploy:
    - upload pkg to test PyPI
    - upload pkg to PyPI
    - git commit the new spec
    - git commit the new SDK source


## Fix the OpenAPI spec
As documented in [my other blog post](http://konradp.gitlab.io/blog/post/fin-ib-web-api-client-swagger/), there are a few fixes that need to be applied to the [OpenAPI spec](https://www.interactivebrokers.com/api/doc.json) provided by Interactive Brokers. Without these, the generated SDK breaks with various syntax errors. I wrote a [fix_json.py](https://gitlab.com/konradp/ib_web_api/-/blob/main/fix_json.py) script which does it in the 'prep' stage of the pipeline.

In short, there are few broken 'enums' in the spec, at these paths:

- **definitions/order-data/properties/execType**
- **definitions/order-data/properties/orderStatus**
- **definitions/order-data/properties/orderType**
- **definitions/order-data/properties/secType**
- **definitions/order-data/properties/side**
- **definitions/order-data/properties/tif**

and one duplicate REST API param (company_name vs companyName):

- **definitions/contract/properties/companyName**



## PyPI
Reading the [python packaging ref](https://packaging.python.org/en/latest/tutorials/packaging-projects/), for PyPI we need a **.whl** and a **tar.gz** files. Now, **swagger-codegen** generates the code (to be uploaded to git), but not the **.whl** or **.tar.gz** files.

But **swagger-codegen** generates a **setup.py** file, and we can use it with **wheel** and **twine** to generate the dist packages. I've done this already in [another project](https://gitlab.com/konradp/pyeh), and it looks like this:
```
python3 setup.py sdist bdist_wheel
python3 -m twine check dist/*
python3 -m twine upload dist/*
```


## Committing from within a pipeline
One thing which I haven't done before in GitLab CI/CD, is committing to my repo from within a pipeline. It turned out to be quite straightforward, and I took a hint from a [StackOverflow article](https://stackoverflow.com/questions/51716044/how-do-i-push-to-a-repo-from-within-a-gitlab-ci-pipeline).

You can see my solution [here](https://gitlab.com/konradp/ib_web_api/-/blob/main/.gitlab-ci.yml#L164), and it uses:

- SSH key: GitLab repo deploy key
- priv key added as a GitLab CICD file variable
- **git remove origin** and **git add origin**
- **git add**, **git commit**, **git push**

## Outcome
repo: https://gitlab.com/konradp/ib_web_api  
PyPI package: https://pypi.org/project/ib-web-api/
