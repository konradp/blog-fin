---
title: "Indicators"
date: 2025-02-03
categories: 00 studies
math:
  enable: true
---
Table of contents:
{{<toc>}}


TODO
- {{<x>}} definitions and links for the indicators:
  - {{<v>}} mean-reversion
    - {{<v>}} RSI, MACD, VWAP
    - {{<x>}} stochastic oscillator
    - {{<x>}} bollinger
  - {{<x>}} VIX
- {{<x>}} any other indicators I should be aware of? Ask chatgpt/deepseek
- {{<x>}} fibonacci?


## VIX: Volatility Index
- ^VIX symbol on yahoo
- aka. CBOE Volatility Index: Chicago Board Options Exchange Volatility Index
- based on S&P 500 index options
- VXNSM (or ^VXN on yahoo): NASDAQ-100 Volatility Index


## Momentum indicators / mean reversion
- not good in trending markets
- mean reversion and momentum are not the same: momentum is like the acceleration in acceleration vs speed (ref: JWW section V)
  - JWW calculates MF (Momentum Factor) by comparing today to two days ago closing price. It has like a two-day sliding window
  - TODO: How does this show momentum?
- what is reversal accoding to JWW? ("the system is not a true reversal system because profits are taken at a target")


### Bollinger
TODO


### RSI: Relative Strength Index
- ref: https://www.investopedia.com/terms/r/rsi.asp
- type: momentum
- type: mean-reversion
- trend-following indicator
- plot below chart as a line chart
- introduced by J. Welles Wilder in 1978 book New Concepts In Technical Trading Systems
  - Note: Also introduced ATR bands there
- 0 - 100
- range: +70: overbought, -30: oversold
- default period: 14 days

$$
\begin{equation*}
  RSI_1 = 100 - \bigg[ \frac{100}{1 + avg(gain) \over avg(loss)} \bigg]
\end{equation*}
$$


### MACD: Moving Average Convergence Divergence
- ref: https://www.investopedia.com/investing/momentum-and-relative-strength-index/
- subtract 26-period EMA from 12-period EMA
- type: momentum
- type: mean reversion


#### EMA: Exponential Moving Average
- ref: https://www.investopedia.com/terms/e/ema.asp
- ref: https://en.wikipedia.org/wiki/Exponential_smoothing
- more weight to recent days

$$
\begin{align*}
  E_0 &= \bigg( V_0 * \frac{S}{1+D} \bigg) \\\
      &+ E_{-1} * \bigg( 1 - \frac{S}{1+D} \bigg)
\end{align*}
$$

where:
- $E_0$: EMA today
- $E_{-1}$: EMA yesterday
- $S$: smoothing = 2
- $D$: days
- $V$: value today

better, from wiki:

$$
\begin{align*}
  s_0 &= x_0 \\\
  s_t &= \alpha x_t + (1-\alpha) s_{t-1}  \text{,} \qquad t>0 \\\
      &= s_{t-1} + \alpha ( x_t - s_{t-1} )
\end{align*}
$$

where
- $0 < \alpha < 1$


### VWAP: Volume-Weighted Average Price
- used for day trading
- price*volume / total shares traded
- (cumulative typical price * volume) / cumulative volume
  - where typical price = (high+low+close)/3
- (shares bought * share price) / total volume
- ref: https://www.investopedia.com/terms/v/vwap.asp

$$
\begin{equation*}
  V = \frac{\sum_j P_j \cdot Q_j}{\sum_j Q_j}
\end{equation*}
$$

where:
- $V$: VWAP value
- $P_j$: price of trade $j$
- $Q_j$: quantity of trade $j$
- $j$: each individual trade


## Notes
- JWW: J. Welles Wilder book: downloaded from pdfdrive.to or pdfcoffee.com or any other russian site
  - Section II: Parabolic system
  - Section III: Volatility: ATR
  - Section IV: Directional Movement
  - Section V, p53:  Momentum, TBP (Trend Balance Point System)
  - Section VI: RSI
  - Section VII: Reaction Trend
  - Section VIII: Swing
  - Section IX: Commodity Selection
  - Section X: Capital Management
- TODO: Check https://www.investopedia.com/ask/answers/121014/how-do-i-create-trading-strategy-bollinger-bands-and-relative-strength-indicator-rsi.asp
- TODO: Check https://www.investopedia.com/investing/momentum-and-relative-strength-index/
