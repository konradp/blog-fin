---
title: 'Compound interest calculator: advanced'
subtitle: 
date: 2024-10-23
categories: 03 other
---
<script src='/media/other-compound-interest-calculator-advanced/app.js'></script>
<head>
<link rel="stylesheet" href="/media/other-compound-interest-calculator-advanced/style.css">
</head>
<body onload='main();'></body>

<center>

setting | value
:-- | :--
investment $:       | <input id="investment" type="number" value="100"/>
take profit %:      | <input id="take-profit" type="number" value="4"/>
stop loss % (risk): | <input id="stop-loss" type="number" value="1"/>
number of days:     | <input id="days" type="number" value="100"/>
success rate %:     | <input id="success-rate" type="number" value="50"/>

<button id="run">calculate</button>

<hr>
final balance: <span id='final-balance'>?</span>

<table>
  <thead>
    <th>day</th>
    <th>balanceIn</th>
    <th>invest</th>
    <th>perc</th>
    <th>soldPrice</th>
    <th>won</th>
    <th>balanceOut</th>
  </thead>
  <tbody id="table">
  </tbody>
</table>

</center>
