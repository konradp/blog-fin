---
title: "Sunny Harris's SunnyBands"
date: 2024-02-07
categories: 00 studies
---
**Contents**
{{<toc>}}

Here I try to find out how the SunnyBands work, as used by Sunny J. Harris, so that I can create my own version for my own trading.

TODO:
- {{<x>}} compare against Bollinger bands

TODO, when more time:
- {{<x>}} listen to Sunny's appearance on chat with traders
- {{<x>}} in [^a] ref1 [^1](ref1) and [^b] and [^ref1], outer bands are mentioned. Are the inner bands used for anything? Do they come from ATR? Do they come from the fact that the DMA has two lines?
- {{<x>}} see Sunny's books, ref: https://moneymentor.com/consulting.html
- {{<x>}} watch: [youtube: Live Demo 20231024 Sunny Harris w SunnyBands and DMA](https://www.youtube.com/watch?v=k2vTvCgKo1I)
- {{<x>}} watch: [youtube: Jim Simons Trading Secrets 1.1 MARKOV Process](https://www.youtube.com/watch?v=gA0egjZcRB0&pp=ygUSc3VubnkgaGFycmlzIGJhbmRz)


## MA: Moving average
- [wiki: Moving average](https://en.wikipedia.org/wiki/Moving_average)
- It's common to look at two moving averages with different period, and to take signals from when they cross: [wiki: Moving average crossover](https://en.wikipedia.org/wiki/Moving_average_crossover)
- make it to max. 200 day length


## Bollinger bands
TODO

## DMA: Dynamic Moving Average
DMA usually means a Displaced Moving Average, which is a moving average (MA) but shifted left or right (no change to the period). As I understand, the Displaced Moving Average is **not** the DMA which is used in SunnyBands.

Dynamic Moving Average:
- it has two lines, not one: see 'Moving average crossover' above
  - ref: [investopedia](https://www.investopedia.com/terms/d/displacedmovingaverage.asp)


## ATR: Average True Range
- what are ATR bands?
  - average true range
  - "The ATR time period default is 21 days, with multiples set at a default of 3 x ATR. The normal range is 2, for very short-term, to 5 for long-term trades. Multiples below 3 are prone to whipsaws."
  - "The bands are calculated by adding/subtracting a multiple of Average True Range to the daily closing price."
  - "Average True Range is typically a 14 day exponential moving average* of True Range."
  - "A number of popular indicators, including Relative Strength Index (RSI), Average True Range (ATR) and Directional Movement were developed by J. Welles Wilder Jr. and introduced in his 1978 book: New Concepts in Technical Trading Systems."

TR: definition: The greatest of the following:
- currentHigh - currentLow
- abs(currentHigh - prevClose)
- abs(currentLow - prevClose)

ATR: a moving average (usually 14 days) of the true ranges


## SunnyBands
- extension of Sunny's DMA
- DMA: purple and gold
- additional two lines: constructed from ATR bands on either side of the DMA
- First, try to reproduce the chart from [ref1]. Get prices from: [yahoo](https://finance.yahoo.com/quote/ES%3DF/history?period1=1599782400&period2=1618099200&interval=1d&filter=history&frequency=1d&includeAdjustedClose=true)
- TODO: ATR bands only have two bands, not four?

Example from Sunny's website:<br>
<center>
  <img src="/media/study-sunny-harris-bands/chart1_sunny.jpg"></img><br>
  source: [ref1], cropped by me<br>
  <a href="https://moneymentor.com/images/Indicators/sjh_SunnyBands.jpg">link to original image</a>
</center>


## My attempt
My attempt:<br>
<b>Note:</b>Try MA period 15 and 13, or 16 and 14
<script src="https://konradp.gitlab.io/finplotjs/js/finplot.js"></script>
<script src="/media/study-sunny-harris-bands/chart1.js"></script>
<center>
  <canvas id='canvas'>></canvas>
</center>
<b>Note:</b> The red line is a moving average which adjusts its period based on the gradient between current and previous close.

<!-- CONTROLS: MOVING AVERAGE -->
<input type='checkbox'></input>
MA 1: (purple)
<input id='range-ma1' type='range' min='1' max='70' value='16'></input>
<span id='span-ma1'>10</span>
<br>
<input type='checkbox'></input>
MA 2: (gold)
<input id='range-ma2' type='range' min='1' max='70' value='14'></input>
<span id='span-ma2'>20</span>

Notes:
- chart shows E-mini S&P 500 Futures (ticker ES, on yahoo it's ES=F)
- chart is from 2020.09.08 to 2021.03.13

## References
- ATR:
  - ref: [chartschool](https://chartschool.stockcharts.com/table-of-contents/technical-indicators-and-overlays/technical-indicators/average-true-range-atr-and-average-true-range-percent-atrp)
  - ref: [incrediblecharts.com](https://www.incrediblecharts.com/indicators/atr_average_true_range_bands.php)
  - ref: [investopedia](https://www.investopedia.com/terms/a/atr.asp)
  - ref: [stockmaniacs.net: bands](https://www.stockmaniacs.net/atr-bands/)
  - ref: [themuse.com](https://www.themuse.com/advice/average-true-range)
  - ref: [tradingtechnologies.com](https://library.tradingtechnologies.com/trade/chrt-ti-atr-bands.html)
- SunnyHarris indicators:
  - bands: https://moneymentor.com/products.html#SBands
  - dma: https://moneymentor.com/products.html#DMA
