---
title: Financial APIs
subtitle: Free APIs which provide free end of day financial data
date: 2019-07-22
categories: 02 tech
---

We look at APIs which provide free end-of-day stock prices.

{{<toc>}}

## APIs to check
Notes

platform              | Notes
:--                   | :--
finnhub.io            | 60/minute, so could work
ETRADE                | auth complicated
IB                    | Is the auth still annoying?
Degiro                |
Alpaca                |
Robinhood             |
OptionChain           |
Xignite               |
eoddata.com           | manual download
barchart
databull
eSignal
IQFeed
IRIS
MTPredictor
Primate
prophet.net
QCollector
QuotesPlus
Reuters
thinkorswim
Tradeguider
Tradestation
trading-tools.com
WealthLab Pro (fidelity)
Yahoo Finance

Notes2

platform              | limit             | can_trade | stock_data
:--                   | :--               | :--       | :---
finnhub.io            | 60/minute
ETRADE                | 
IB                    |  
Degiro                |  
Alpaca                |  
Robinhood             |  
OptionChain           |  
Xignite               |  
eodhistoricaldata.com | 20/day   | 

## Rejected APIs

platform              | reason
:--                   | :--
AlphaVantage          | 5/minute, 500/day, no bulk download
Ameritrade            | not available in the UK
eodhistoricaldata.com | 20/day calls, no bulk-download
eToro                 | no market data API
GoogleFinance         | no longer available
IEX                   | No longer free
marketstack.com       | 60/month calls, there is bulk EOD API, but 1 credit per ticker, so 100 stocks per month
polygon.io            | 5 API calls per minute
Tradier               | not available in the UK
TwelveData            | python 2.7 doesn't work
YahooFinance          | official API no longer works

## APIs in detail

### To check

#### eoddata.com
- https://www.eoddata.com/download.aspx

#### AlphaVantage
- Limit on requests: 5/minute, 500/day

#### Finnhub
- there is a python SDK: finnhub-python
- API: https://finnhub.io/docs/api
- SDK: https://github.com/Finnhub-Stock-API/finnhub-python
- 60 API calls per minute

API limit
```
raise FinnhubAPIException(response)
finnhub.exceptions.FinnhubAPIException: FinnhubAPIException(status_code: 429): API limit reached. Please try again later. Remaining Limit: 0
```

#### ETRADE
- looks good but complicated auth, and unable to get API key. TODO: Contact support for API key

#### Tradier
Not available in the UK
#### Degiro
#### IB
#### Xignite
#### Finhub
#### Alpaca


#### polygon.io
- EoD data
- 5 API calls / minute
- 2 yr historical data
- fundamentals
- bulk API:
  - snapshots: https://polygon.io/docs/stocks/get_v2_snapshot_locale_us_markets_stocks_tickers

### Rejected APIs

#### Ameritrade
- https://tda-api.readthedocs.io/en/latest/streaming.html
- https://developer.tdameritrade.com/content/streaming-data
- Not available in the UK

#### IEX
- Historically, has been very good
- Recently (2019) reworked their API slightly (called IEX Cloud now)
- Recently (2019) some data sets became paid-subscription only
- As of 2022, this is now being migrated into a new paid product: Apperate
- legacy APi might still work:
  - https://iexcloud.io/console/search

legacy
```
export API_TOKEN=
curl "https://cloud.iexapis.com/v1/stock/AMZN/quote?token=$API_TOKEN"
```


#### Twelvedata

- 800 API credits per day (8 per minute)
- 8 WS credits (WebSocket)
- historical data + cryptos
- tech indicators, fundamentals
- doesn't work on python2.7
- [python guide](https://twelvedata.com/blog/get-high-quality-financial-data-directly-into-python)
- [API](https://twelvedata.com/docs)

guide
```
pip install twelvedata[matplotlib,plotly]
```

Rejecting, because Python 2.7 doesn't work:
```
$ python app.py 
Traceback (most recent call last):
  File "app.py", line 6, in <module>
    from twelvedata import TDClient
  File "/usr/local/lib/python2.7/dist-packages/twelvedata/__init__.py", line 3, in <module>
    from .client import TDClient
  File "/usr/local/lib/python2.7/dist-packages/twelvedata/client.py", line 2, in <module>
    from .endpoints import (
  File "/usr/local/lib/python2.7/dist-packages/twelvedata/endpoints.py", line 123
    def get_symbol(symbol) -> (str, bool):
                           ^
SyntaxError: invalid syntax
```
for program
```
from twelvedata import TDClient
td = TDClient
```

### GoogleFinance
Retired:

- [google groups](https://groups.google.com/g/google-finance-apis?pli=1)
- [google blog](https://developers.googleblog.com/2012/04/changes-to-deprecation-policies-and-api.html)

#### marketstack.com
- limit 100/month will not be enough
- there is bulk EOD API, but 1 credit per ticker, so 100 stocks per month, so not going to work

<!--Reference-->
