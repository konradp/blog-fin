---
title: "WIP: Fintools ideas"
date: 2022-11-07
categories: 09 wip
---
{{<toc>}}

## Must do

Gathering data:

- {{<v>}} list of stocks from NASDAQ:
  - {{<v>}} nasdaqtrader.com: 3481 common stocks
  - {{<v>}} finnhub.io: 3512 stocks
- {{<v>}} sector/industry for all stocks
- {{<v>}} add market cap
- {{<v>}} add sharesOutstanding
- {{<v>}} quote for each stock
- check if IEX API has any quote endpoints for multiple stocks
- select most recent close price from the quotes table!

Store data:

- {{<v>}} Install mariadb and add users
- {{<v>}} Create DB
- {{<v>}} Python connector, DB object

General:

- store downloaded lists: redis? mongodb? flatfile? mysql?
- API

Dashboards:

- sector moves
  - view: percent increase per stock
- gainers
- losers

Notes (remember):

- difference between get and fetch


## Maybe
General:

- empty industry? '' and 'N/A'
- check if InteractiveBrokers still has annoying auth and logs out often
- Stock industry: finnhub.io categorises AMZN as 'Retail', but I think it should be 'Technology', or multiple tags. How does IB compare? Should we use a different source, e.g. NASDAQ directly, or Wikipedia (or Wikidata)? On IEX, it is 'Industry = Electronic Shopping and Mail-Order Houses', 'Sector = Retail Trade'. Check also Kaggle: https://www.kaggle.com/datasets/winston56/fortune-500-data-2021
- eod_import.py:
  - Why are there stocks here which are not in our DB?
  - Why are there stocks in our DB which are not in this CSV?

Tests:

- alert if data changes
- periodically test remote endpoints
- eToro API

Alerts:

- new stock is added to NASDAQ

## Data sources
- stock:
  - {{<v>}} list: nasdaqtrader.com
  - {{<v>}} type: nasdaqtrader.com
  - {{<v>}} industry: finnhub
  - {{<v>}} market cap: finnhub
  - {{<v>}} shares outstanding: finnhub
  - eod price
  - current price

## Better data sources
### SEC
Links

- sec.gov/data
- https://www.sec.gov/edgar/sec-api-documentation

#### SEC filings
four sections:

- business section
- F-pages
- risk factors
- MD&A

- 10-K: annual
- 10-Q: quarterly
- 8-K
- forms 3,4,5
- schedule 13
- form 114
- S-1 registration statement: before IPO

### Market cap
```
MC = N * P
where:
- N: shares outstanding
- P: share price
```

### Shares outstanding
According to [finding outstanding shares], the number can be found at:

- SEC Form 10-Q
- SEC Form 10-K

## Notes
To check:

- Thomson Reuters Open Calais API
- Hoover's company records
- PermID
- https://developers.refinitiv.com/

### SEC
https://www.sec.gov/dera/data/financial-statement-and-notes-data-set.html
data/financial-statement-and-notes-data-set.html

## Reference
- [finding outstanding shares](https://lexisnexis.custhelp.com/app/answers/answer_view/a_id/1094124/~/finding-outstanding-shares)

[finding outstanding shares]: https://lexisnexis.custhelp.com/app/answers/answer_view/a_id/1094124/~/finding-outstanding-shares
