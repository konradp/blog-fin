---
title: "2021 w53: Last 5 days"
date: 2021-01-03
categories: 08 journal
---

<style>
.red {
  border: 2px solid red;
}
.discard {
  border: 5px solid blue;
}
</style>

# Data overview
Again, focus on ~30 symbols from category lt 3$/share, Software industry.  
Last 5 days, i.e. dates:

- 2020-12-24 Thu
- 2020-12-28 Mon
- 2020-12-29 Tue
- 2020-12-30 Wed
- 2020-12-31 Thu

with 2 minute bar size, i.e. for trading from 14:30-21:00 UK time, or 09:30-16:00 Exchange time, giving 6hr30min, i.e. `(6*60+30)/2 = 390/2 = 195` data points per day (instead of 390 on 1 min bar size).  
This gives max. 975 data points for each symbol in total (5 days).

**Note:** 2020-12-24 Thu followed partial trading schedule 14:30-18:00 (09:30-13:00) and so has 105 points instead of 195.

So, in total there are max. `195*4 + 105 = 885` data points for each symbol.  

**Note:** Except for PBTS, with 874 data points because instead of 195, it has 184 data points on Wed 30. The first data point on Wed 30 is at 14:52, which accounts for the 22 minutes (11 points) missing.

In total, there are 26518 data points:
```
$ jq '.[].t' *.json | wc -l
26518
```

Symbols are:

- ANY
- BHAT
- BOSC
- BOXL
- BRQS
- BSQR
- CNET
- CYRN
- EVOL
- GNUS
- GSUM
- GVP
- IDEX
- INPX
- LKCO
- LYL
- MKD
- MNDO
- MYSZ
- PBTS
- PHUN
- PIXY
- QTT
- SEAC
- SSNT
- STRM
- VERB
- VISL
- XELA
- XNET

# Exclusions

Started with ~30 symbols, reduced to ??.

## Volume

Count of volume points:
```
$ jq '.[].v' * | sort | uniq -c | sort -h | tail -n 5
    485 5
    498 3
    773 2
   1265 1
   8495 0
```
i.e. there are 8495 data points with volume 0, which is 32% of the total 26518 data points.

volume    | count     | perc
---:      | ---:      | ---:
0         | 8495      | 32
1         | 1265      | 5
2         | 773       | 3
3         | 498       | 2
5         | 485       | 2
**total** | **11516** | **43**

That is a lot of zero volume points.

## Report
Download data (this is actually now for 2020-01-07)
```
./down_symbols_industry_lt3_5days.py Software
```

Get tables

total
```
./report_5day_volume.py /opt/fintools-ib/data/5days/Software/2021-01-07/data/
```

{{<table sortable>}}
SYMB | vol_avg | vol_min | vol_max | vol_median | vol_nonz_min | vol_nonz_count | vol_nonz_avg | vol_nonz_median | vol_zero_count
--- | --: | --: | --: | --: | --: | --: | --: | --: | --:
GVP | 5.01 | 0.00 | 338.00 | 0.00 | 1.00 | 212.00 | 23.05 | 10.00 | 763.00
BRQS | 89.77 | 0.00 | 2231.00 | 22.00 | 1.00 | 851.00 | 102.84 | 30.00 | 124.00
EVOL | 7.23 | 0.00 | 371.00 | 0.00 | 1.00 | 441.00 | 15.99 | 6.00 | 534.00
ANY | 68.58 | 0.00 | 3208.00 | 4.00 | 1.00 | 650.00 | 102.87 | 16.00 | 325.00
VISL | 313.38 | 0.00 | 8385.00 | 65.00 | 1.00 | 951.00 | 321.28 | 67.00 | 24.00
LKCO | 120.28 | 0.00 | 2371.00 | 44.00 | 1.00 | 899.00 | 130.45 | 52.00 | 76.00
PIXY | 36.27 | 0.00 | 3195.00 | 6.00 | 1.00 | 770.00 | 45.93 | 11.00 | 205.00
VERB | 41.24 | 0.00 | 745.00 | 10.00 | 1.00 | 811.00 | 49.58 | 16.00 | 164.00
GSUM | 4.51 | 0.00 | 452.00 | 0.00 | 1.00 | 146.00 | 30.14 | 10.00 | 829.00
SSNT | 5.02 | 0.00 | 221.00 | 0.00 | 1.00 | 351.00 | 13.95 | 6.00 | 624.00
STRM | 3.04 | 0.00 | 867.00 | 0.00 | 1.00 | 166.00 | 17.87 | 4.00 | 809.00
GNUS | 535.27 | 0.00 | 8596.00 | 300.00 | 16.00 | 974.00 | 535.82 | 300.00 | 1.00
IDEX | 2400.94 | 0.00 | 27630.00 | 1306.00 | 41.00 | 974.00 | 2403.40 | 1310.50 | 1.00
PBTS | 4.33 | 0.00 | 263.00 | 0.00 | 1.00 | 246.00 | 17.14 | 7.00 | 729.00
INPX | 208.98 | 0.00 | 3829.00 | 78.00 | 1.00 | 956.00 | 213.13 | 81.00 | 19.00
MYSZ | 44.38 | 0.00 | 1545.00 | 8.00 | 1.00 | 724.00 | 59.76 | 17.00 | 251.00
BSQR | 10.22 | 0.00 | 468.00 | 0.00 | 1.00 | 361.00 | 27.61 | 9.00 | 614.00
XELA | 391.68 | 0.00 | 4471.00 | 222.00 | 1.00 | 969.00 | 394.11 | 224.00 | 6.00
SEAC | 51.66 | 0.00 | 2641.00 | 8.00 | 1.00 | 764.00 | 65.93 | 16.00 | 211.00
CNET | 468.38 | 0.00 | 12268.00 | 51.00 | 1.00 | 893.00 | 511.39 | 68.00 | 82.00
BOSC | 5.84 | 0.00 | 360.00 | 0.00 | 1.00 | 293.00 | 19.44 | 7.00 | 682.00
BHAT | 46.13 | 0.00 | 2454.00 | 10.00 | 1.00 | 817.00 | 55.05 | 15.00 | 158.00
MKD | 43.22 | 0.00 | 1106.00 | 7.00 | 1.00 | 647.00 | 65.12 | 22.00 | 328.00
MNDO | 3.89 | 0.00 | 239.00 | 0.00 | 1.00 | 230.00 | 16.49 | 5.00 | 745.00
PHUN | 371.33 | 0.00 | 7323.00 | 168.00 | 1.00 | 973.00 | 372.10 | 169.00 | 2.00
XNET | 667.83 | 0.00 | 13446.00 | 115.00 | 1.00 | 951.00 | 684.68 | 126.00 | 24.00
BOXL | 232.98 | 0.00 | 10894.00 | 65.00 | 1.00 | 968.00 | 234.67 | 66.00 | 7.00
LYL | 1.58 | 0.00 | 135.00 | 0.00 | 1.00 | 172.00 | 8.89 | 4.00 | 795.00
CYRN | 36.85 | 0.00 | 2490.00 | 2.00 | 1.00 | 572.00 | 62.81 | 11.00 | 403.00
QTT | 244.00 | 0.00 | 4325.00 | 69.00 | 1.00 | 938.00 | 253.63 | 74.00 | 37.00
{{</table>}}


Again
```
./report_5day_volume.py /opt/fintools-ib/data/5days/Software/2021-01-09/data
```
gives

{{<table sortable>}}
SYMB | count | vol_avg | vol_max | vol_median | vol_nonz_min | vol_nonz_count | vol_nonz_avg | vol_nonz_median | vol_zero_count | vol_zero_perc
--- | --: | --: | --: | --: | --: | --: | --: | --: | --: | --:
GVP | 975.00 | 4.79 | 338.00 | 0.00 | 1.00 | 200.00 | 23.37 | 10.00 | 775.00 | 79.49
BRQS | 975.00 | 334.88 | 10427.00 | 42.00 | 1.00 | 884.00 | 369.35 | 54.00 | 91.00 | 9.33
EVOL | 975.00 | 6.35 | 371.00 | 0.00 | 1.00 | 431.00 | 14.37 | 5.00 | 544.00 | 55.79
ANY | 975.00 | 79.33 | 3208.00 | 12.00 | 1.00 | 753.00 | 102.71 | 26.00 | 222.00 | 22.77
VISL | 975.00 | 325.40 | 8385.00 | 77.00 | 1.00 | 954.00 | 332.56 | 79.00 | 21.00 | 2.15
LKCO | 975.00 | 124.53 | 2371.00 | 51.00 | 1.00 | 914.00 | 132.84 | 57.50 | 61.00 | 6.26
PIXY | 975.00 | 46.48 | 3195.00 | 8.00 | 1.00 | 781.00 | 58.03 | 13.00 | 194.00 | 19.90
VERB | 975.00 | 45.46 | 720.00 | 12.00 | 1.00 | 807.00 | 54.92 | 21.00 | 168.00 | 17.23
GSUM | 975.00 | 6.27 | 316.00 | 0.00 | 1.00 | 169.00 | 36.15 | 9.00 | 806.00 | 82.67
STRM | 975.00 | 2.95 | 867.00 | 0.00 | 1.00 | 164.00 | 17.51 | 4.00 | 811.00 | 83.18
GNUS | 975.00 | 580.03 | 8596.00 | 289.00 | 16.00 | 974.00 | 580.63 | 289.00 | 1.00 | 0.10
PBTS | 975.00 | 6.86 | 403.00 | 0.00 | 1.00 | 287.00 | 23.32 | 9.00 | 688.00 | 70.56
INPX | 975.00 | 215.54 | 3829.00 | 86.00 | 1.00 | 970.00 | 216.65 | 87.50 | 5.00 | 0.51
MYSZ | 975.00 | 46.01 | 1545.00 | 10.00 | 1.00 | 778.00 | 57.66 | 17.00 | 197.00 | 20.21
BSQR | 975.00 | 10.03 | 468.00 | 0.00 | 1.00 | 384.00 | 25.46 | 7.00 | 591.00 | 60.62
XELA | 975.00 | 427.12 | 4471.00 | 251.00 | 1.00 | 974.00 | 427.56 | 251.50 | 1.00 | 0.10
SEAC | 975.00 | 32.37 | 905.00 | 6.00 | 1.00 | 702.00 | 44.95 | 12.00 | 273.00 | 28.00
BOSC | 975.00 | 5.50 | 360.00 | 0.00 | 1.00 | 288.00 | 18.62 | 6.50 | 687.00 | 70.46
BHAT | 975.00 | 47.87 | 2454.00 | 12.00 | 1.00 | 847.00 | 55.10 | 17.00 | 128.00 | 13.13
MKD | 975.00 | 48.81 | 1106.00 | 9.00 | 1.00 | 660.00 | 72.11 | 24.00 | 315.00 | 32.31
MNDO | 975.00 | 4.13 | 239.00 | 0.00 | 1.00 | 233.00 | 17.30 | 5.00 | 742.00 | 76.10
PHUN | 975.00 | 443.82 | 9623.00 | 190.00 | 1.00 | 972.00 | 445.19 | 190.50 | 3.00 | 0.31
BOXL | 975.00 | 351.91 | 12582.00 | 97.00 | 1.00 | 972.00 | 353.00 | 98.00 | 3.00 | 0.31
LYL | 967.00 | 1.94 | 135.00 | 0.00 | 1.00 | 203.00 | 9.25 | 4.00 | 764.00 | 79.01
CYRN | 975.00 | 41.93 | 2490.00 | 2.00 | 1.00 | 595.00 | 68.72 | 17.00 | 380.00 | 38.97
QTT | 975.00 | 227.53 | 4325.00 | 56.00 | 1.00 | 919.00 | 241.40 | 63.00 | 56.00 | 5.74
{{</table>}}

Now think about which stats are good for excluding bad symbols based on volume.

Bad symbols:

- IDEX
- GNUS

stats

{{<table sortable>}}
stat            | is_good | comments
--- | --- | ---
vol_avg         | maybe   | maybe exclude extremes
vol_min         | no      | everything is 0
vol_max         | maybe   | If price is $2, then max vol 135 will mean low liquidity (can max buy/sell `135*2 = $270`). <br>then can exclude these. but think also to combine with other stats (think distribution)
vol_median      | maybe   | flags symbols which often have v=0. if median=0, excludes 10 symbols
vol_nonz_min    | maybe   | most are 1, but 2 symbols with non-zero. maybe exclude them if min > 1
vol_nonz_count  | maybe   | maybe exclude the extremes
vol_nonz_avg    | maybe   | interesting, it's not direct correspondence with vol_nonz_count, but where divert, interesting symbols. Also sort by vol_max, and spikes in vol_nonz_avg point to interesting symbols
vol_nonz_median | maybe   | maybe exclude extremes
vol_zero_count  | maybe   | maybe exclude extremes, or combine with another stat
{{</table>}}

Applying exclusion based on vol_zero_perc >= 50%, we exclude 9 symbols:

- BOSC
- BSQR
- EVOL
- GSUM
- GVP
- LYL
- MNDO
- PBTS
- STRM

leaving:

- ANY
- BHAT
- BOXL
- BRQS
- CYRN
- GNUS
- INPX
- LKCO
- MKD
- MYSZ
- PHUN
- PIXY
- QTT
- SEAC
- VERB
- VISL
- XELA

Inspect chars, ideas for price report.

Want to exclude symbols like below.

<center>

symbol | date | chart
--- | --- | ---
BOXL|2021-01-04|<img src="/media/trade-journal-2020W53-take2/BOXL_2021-01-04.png"></img>
BRQS|2021-01-06|<img src="/media/trade-journal-2020W53-take2/BRQS_2021-01-06.png"></img>
CYRN|2021-01-05|<img src="/media/trade-journal-2020W53-take2/CYRN_2021-01-05.png"></img>
GNUS|2021-01-04|<img src="/media/trade-journal-2020W53-take2/GNUS_2021-01-04.png"></img>
GNUS|2021-01-05|<img src="/media/trade-journal-2020W53-take2/GNUS_2021-01-05.png"></img>
INPX|2021-01-08|<img src="/media/trade-journal-2020W53-take2/INPX_2021-01-08.png"></img>

</center>

Look total:
- lh_perc
- std_dev
  - avg (mean)
  - variance: avg of (distance to mean squared)
  - std_dev: sqrt(variance)

Look here per day:
- lh_perc
- std_dev: standard deviation

{{<table sortable>}}
SYMB | mean | std_dev | std_dev_perc
--- | --: | --: | --:
GVP | 1.34 | 0.04 | 2.79
BRQS | 1.04 | 0.06 | 5.34
EVOL | 2.06 | 0.05 | 2.25
ANY | 1.68 | 0.20 | 12.04
VISL | 1.55 | 0.15 | 9.42
LKCO | 0.78 | 0.06 | 7.40
PIXY | 2.61 | 0.12 | 4.51
VERB | 1.70 | 0.05 | 2.94
GSUM | 1.69 | 0.03 | 1.84
STRM | 1.65 | 0.05 | 2.89
GNUS | 1.40 | 0.03 | 2.34
PBTS | 2.45 | 0.22 | 9.08
INPX | 1.09 | 0.03 | 2.81
MYSZ | 1.42 | 0.14 | 10.02
BSQR | 1.52 | 0.07 | 4.53
XELA | 0.49 | 0.02 | 4.95
SEAC | 1.31 | 0.06 | 4.58
BOSC | 2.26 | 0.01 | 0.59
BHAT | 0.92 | 0.01 | 1.57
MKD | 0.84 | 0.03 | 3.54
MNDO | 2.63 | 0.06 | 2.25
PHUN | 1.24 | 0.06 | 4.84
BOXL | 1.61 | 0.12 | 7.20
LYL | 2.45 | 0.18 | 7.44
CYRN | 1.08 | 0.06 | 5.11
QTT | 1.93 | 0.05 | 2.63
{{</table>}}


# Conclusions
Nothing special here
