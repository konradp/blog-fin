#!/bin/bash
# Run this in a blog root dir to install, change, or update hugo themes
read -r -d '' VAR <<EOF
  xmin    https://github.com/yihui/hugo-xmin
EOF


# Choice: 1) install themes, 2) switch theme
echo Current theme
grep theme config.toml

while [[ $OPT != "1" && $OPT != "2" ]]; do
  echo Options:
  echo 1\) Install themes
  echo 2\) Switch theme
  echo 3\) Update themes
  read -p '> ' OPT
done

echo $OPT

if [[ $OPT == "1" ]]; then
  # Install themes
  while read THEME REPO; do
    if [[ ! -d themes/$THEME ]]; then
      echo Install $THEME from $REPO
      git submodule add $REPO themes/$THEME --force
    fi
  done <<< $VAR
elif [[ $OPT == "2" ]]; then
  # Switch theme
  echo Not implemented
  echo Current theme
  grep theme config.toml
  echo Pick theme:
  for THEME in $(ls -1 themes/); do
    echo \ \ - $THEME
  done

  read -p '> ' THEME
  echo You picked $THEME
  sed -i "s/theme = .*/theme = \"$THEME\"/" config.toml
elif [[ $OPT == "3" ]]; then
  # Update themes
  echo Update themes
  git submodule update --remote --merge
fi
